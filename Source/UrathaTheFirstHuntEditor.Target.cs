// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class UrathaTheFirstHuntEditorTarget : TargetRules
{
	public UrathaTheFirstHuntEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

        DefaultBuildSettings = BuildSettingsVersion.V2;

        ExtraModuleNames.Add("UrathaTheFirstHunt");
	}
}
