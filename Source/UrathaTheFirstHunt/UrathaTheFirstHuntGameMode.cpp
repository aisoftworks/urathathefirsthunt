// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "UrathaTheFirstHuntGameMode.h"
#include "Character/UrathaCharacter.h"
#include "UObject/ConstructorHelpers.h"

AUrathaTheFirstHuntGameMode::AUrathaTheFirstHuntGameMode()
{
	// set default pawn class to our Blueprinted character
	/*static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Uratha/Dynamic/Core/Characters/BP_ThirdPersonCharacter"));*/
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Uratha/Dynamic/Core/Characters/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
