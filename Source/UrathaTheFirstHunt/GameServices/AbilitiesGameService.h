// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "BaseGameService.h"

/**
 * GameService class that holds Abilities descriptions and additional data, and persists them.
 */
class URATHATHEFIRSTHUNT_API AbilitiesGameService : public BaseGameService
{
public:
#pragma region Constants
	const FString ABILITY_TYPE_ACTIVE = FString("Active");
	const FString ABILITY_TYPE_CONTESTED = FString("Contested");
	const FString ABILITY_TYPE_PASSIVE = FString("Passive");
#pragma endregion

	/*Enum for Ability IDs. Might get relocated in the later development (common class for enums and such)*/
	UENUM();
	enum class EAbilities : uint8
	{
		Block = 0, //Belongs to Unarmed talent. ACTIVE
		EmbracePain = 1, //Belongs to Unarmed talent. PASSIVE
		Pressing = 2, //Belongs to Unarmed talent. PASSIVE
		Immobilize = 3, //Belongs to Unarmed talent. ACTIVE
		SnapNeck = 4, //Belongs to Unarmed talent. ACTIVE

		Spy = 5, //Belongs to SneakAndLarceny talent. CONTESTED
		Backstab = 6, //Belongs to SneakAndLarceny talent. ACTIVE
		ShadowDodge = 7, //Belongs to SneakAndLarceny talent. ACTIVE
		ShadowStep = 8, //Belongs to SneakAndLarceny talent. ACTIVE

		SeekCover = 9, //Belongs to Gunslinging talent. ACTIVE
		PowerShot = 10, //Belongs to Gunslinging talent. ACTIVE
		ConcentratedFire = 11, //Belongs to Gunslinging talent. ACTIVE
		MaelstromShot = 12, //Belongs to Gunslinging talent. ACTIVE

		UseComputers = 13, //Belongs to TechSavviness talent. CONTESTED
		CraftItemsAmmo = 14, //Belongs to TechSavviness talent. PASSIVE (TRAIT)
		UpgradeWeapons = 15, //Belongs to TechSavviness talent. PASSIVE (TRAIT)

		SixthSense = 16, //Belongs to Sense talent. ACTIVE
		ShadowSense = 17, //Belongs to Sense talent. ACTIVE
		KnowYourEnemy = 18, //Belongs to Sense talent. PASSIVE

		FirstAid = 19, //Belongs to Medicine talent. PASSIVE (TRAIT)
		Supplements = 20, //Belongs to Medicine talent. PASSIVE (TRAIT)
		ShadowAmmunition = 21, //Belongs to Medicine talent. PASSIVE (TRAIT)

		Interogator = 22, //Belongs to FrighfulPresence talent. CONTESTED
		BreakingPoint = 23, //Belongs to FrighfulPresence talent. ACTIVE
		BreakSpirit = 24, //Belongs to FrighfulPresence talent. ACTIVE (SOCIAL COMBAT SKILL)

		Persuader = 25, //Belongs to SmoothTalking talent. CONTESTED
		Negotiator = 26, //Belongs to SmoothTalking talent. ACTIVE
		FightWithMe = 27, //Belongs to SmoothTalking talent. ACTIVE (SOCIAL COMBAT SKILL)

		Seducer = 28, //Belongs to Deception talent. CONTESTED
		SweetSpot = 29, //Belongs to Deception talent. ACTIVE
		Feint = 30, //Belongs to Deception talent. ACTIVE (SOCIAL COMBAT SKILL)
	};

private:
	TMap<EAbilities, TSharedPtr< class UrathaCharacterAbility> > characterAbilities;

public:
	AbilitiesGameService();
	AbilitiesGameService(class UDataTable* abilitiesDataTable);
	~AbilitiesGameService();

	void RollCall();
	class UrathaCharacterAbility* GetAbilityByEnumID(EAbilities abilityEnum);

	void PopulateAbilitiesFromDataTable(class UDataTable *abilitiesDataTable);
};
