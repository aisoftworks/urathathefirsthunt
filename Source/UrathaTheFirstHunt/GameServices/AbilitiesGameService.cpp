// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "AbilitiesGameService.h"
#include "Character/UrathaCharacterAbility.h"
#include "Character/UrathaCharacterPassiveAbility.h"
#include "Character/UrathaCharacterActiveAbility.h"
//#include <type_traits>
#include "Utilities/DataTableStructures.h"

AbilitiesGameService::AbilitiesGameService()
{
	characterAbilities = TMap<EAbilities, TSharedPtr<UrathaCharacterAbility> >();

	/*characterAbilities.Add(EAbilities::Block, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Block), FString("Block")));
	characterAbilities.Add(EAbilities::EmbracePain, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::EmbracePain), FString("EmbracePain")));
	characterAbilities.Add(EAbilities::Pressing, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Pressing), FString("Pressing")));
	characterAbilities.Add(EAbilities::Immobilize, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Immobilize), FString("Immobilize")));
	characterAbilities.Add(EAbilities::SnapNeck, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::SnapNeck), FString("SnapNeck")));
	characterAbilities.Add(EAbilities::Spy, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Spy), FString("Spy")));
	characterAbilities.Add(EAbilities::Backstab, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Backstab), FString("Backstab")));
	characterAbilities.Add(EAbilities::ShadowDodge, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::ShadowDodge), FString("ShadowDodge")));
	characterAbilities.Add(EAbilities::ShadowStep, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::ShadowStep), FString("ShadowStep")));
	characterAbilities.Add(EAbilities::SeekCover, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::SeekCover), FString("SeekCover")));
	characterAbilities.Add(EAbilities::PowerShot, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::PowerShot), FString("PowerShot")));
	characterAbilities.Add(EAbilities::ConcentratedFire, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::ConcentratedFire), FString("ConcentratedFire")));
	characterAbilities.Add(EAbilities::MaelstromShot, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::MaelstromShot), FString("MaelstromShot")));
	characterAbilities.Add(EAbilities::UseComputers, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::UseComputers), FString("UseComputers")));
	characterAbilities.Add(EAbilities::CraftItemsAmmo, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::CraftItemsAmmo), FString("CraftItemsAmmo")));
	characterAbilities.Add(EAbilities::UpgradeWeapons, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::UpgradeWeapons), FString("UpgradeWeapons")));
	characterAbilities.Add(EAbilities::SixthSense, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::SixthSense), FString("SixthSense")));
	characterAbilities.Add(EAbilities::ShadowSense, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::ShadowSense), FString("ShadowSense")));
	characterAbilities.Add(EAbilities::KnowYourEnemy, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::KnowYourEnemy), FString("KnowYourEnemy")));
	characterAbilities.Add(EAbilities::FirstAid, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::FirstAid), FString("FirstAid")));
	characterAbilities.Add(EAbilities::Supplements, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Supplements), FString("Supplements")));
	characterAbilities.Add(EAbilities::ShadowAmmunition, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::ShadowAmmunition), FString("ShadowAmmunition")));
	characterAbilities.Add(EAbilities::Interogator, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Interogator), FString("Interogator")));
	characterAbilities.Add(EAbilities::BreakingPoint, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::BreakingPoint), FString("BreakingPoint")));
	characterAbilities.Add(EAbilities::BreakSpirit, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::BreakSpirit), FString("BreakSpirit")));
	characterAbilities.Add(EAbilities::Persuader, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Persuader), FString("Persuader")));
	characterAbilities.Add(EAbilities::Negotiator, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Negotiator), FString("Negotiator")));
	characterAbilities.Add(EAbilities::FightWithMe, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::FightWithMe), FString("FightWithMe")));
	characterAbilities.Add(EAbilities::Seducer, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Seducer), FString("Seducer")));
	characterAbilities.Add(EAbilities::SweetSpot, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::SweetSpot), FString("SweetSpot")));
	characterAbilities.Add(EAbilities::Feint, new UrathaCharacterAbility(static_cast<uint8>(EAbilities::Feint), FString("Feint")));*/
}

AbilitiesGameService::AbilitiesGameService(class UDataTable* abilitiesDataTable)
{
	UE_LOG(LogTemp, Error, TEXT("AbilitiesGameService - CONSTRUCTOR START"));
	characterAbilities = TMap<EAbilities, TSharedPtr<UrathaCharacterAbility> >();
	PopulateAbilitiesFromDataTable(abilitiesDataTable);
	UE_LOG(LogTemp, Error, TEXT("AbilitiesGameService - CONSTRUCTOR END"));
}

AbilitiesGameService::~AbilitiesGameService()
{
	UE_LOG(LogTemp, Error, TEXT("AbilitiesGameService - DESTRUCTOR START"));
	for (auto ability : characterAbilities)
	{
		UE_LOG(LogTemp, Warning, TEXT("AbilitiesGameService - %s ability references: %i"), *ability.Value->GetAbilityName(), ability.Value.GetSharedReferenceCount());
	}
	UE_LOG(LogTemp, Error, TEXT("AbilitiesGameService - DESTRUCTOR END"));
}

void AbilitiesGameService::RollCall()
{
	UE_LOG(LogTemp, Warning, TEXT("SUCCESS"))
}

UrathaCharacterAbility* AbilitiesGameService::GetAbilityByEnumID(EAbilities abilityEnum)
{
	auto ability = characterAbilities.Find(abilityEnum);
	return ability->Get();
}

void AbilitiesGameService::PopulateAbilitiesFromDataTable(class UDataTable *abilitiesDataTable)
{
	TArray<FAbilitiesData*> dataTableAbilities;
	abilitiesDataTable->GetAllRows<FAbilitiesData>(FString("Get all rows"), dataTableAbilities);

	for (FAbilitiesData* abilityRow : dataTableAbilities)
	{
		if (abilityRow->AbilityType == ABILITY_TYPE_PASSIVE)
		{
			characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MakeShared<UrathaCharacterPassiveAbility>(
				abilityRow->AbilityID,
				abilityRow->AbilityName,
				abilityRow->UnlocksAt
				));
			/*characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MoveTemp(MakeShared<UrathaCharacterPassiveAbility>(
				abilityRow->AbilityID,
				abilityRow->AbilityName,
				abilityRow->UnlocksAt
				)));*/
		}
		else if (abilityRow->AbilityType == ABILITY_TYPE_ACTIVE)
		{
			characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MakeShared<UrathaCharacterActiveAbility>(
				abilityRow->AbilityID,
				abilityRow->AbilityName,
				abilityRow->UnlocksAt
				));
			/*characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MoveTemp(MakeShared<UrathaCharacterActiveAbility>(
				abilityRow->AbilityID,
				abilityRow->AbilityName,
				abilityRow->UnlocksAt
				)));*/
		}
		else
		{
			characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MakeShared<UrathaCharacterAbility>(abilityRow->AbilityID, abilityRow->AbilityName));
			//characterAbilities.Emplace(static_cast<EAbilities>(abilityRow->AbilityID), MoveTemp(MakeShared<UrathaCharacterAbility>(abilityRow->AbilityID, abilityRow->AbilityName)));
		}
	}

	for (auto ability : characterAbilities)
	{
		UE_LOG(LogTemp, Log, TEXT("AbilitiesGameService - %s ability references: %i"), *ability.Value->GetAbilityName(), ability.Value.GetSharedReferenceCount());
	}
}
