// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class URATHATHEFIRSTHUNT_API BaseGameService
{
public:
	BaseGameService();
	virtual ~BaseGameService();
};
