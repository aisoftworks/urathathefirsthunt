// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Character/UrathaAbility.h"
#include "Character/UrathaGift.h"
#include "UrathaPlayerController.generated.h"

class UUrathaAbility;

//Enum used for differentiating between the gunslinging attack types. Used in gameplay ability related code.
UENUM(BlueprintType)
enum class EGunslingingAttackEnum : uint8
{
	GA_Normal UMETA(DisplayName = "Normal"),
	GA_Salvo UMETA(DisplayName = "Salvo Shot"),
	GA_Power UMETA(DisplayName = "Power Shot"),
	GA_Concentrated UMETA(DisplayName = "Concentrated Fire"),
	GA_Maelstrom UMETA(DisplayName = "Maelstrom Shot")
};

//Enum used for differentiating between the unarmed attack types. Used in gameplay ability related code.
UENUM(BlueprintType)
enum class EUnarmedAttackEnum : uint8
{
	GA_Punch UMETA(DisplayName = "Punch"),
	GA_Heavy UMETA(DisplayName = "Heavy Strike"),
	GA_Storm UMETA(DisplayName = "Fist Storm"),
	GA_Immobilize UMETA(DisplayName = "Immobilizing Strike"),
	GA_Kiling UMETA(DisplayName = "Kiling Blow")
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	//Property used to set the current gunslinging attack.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input Commands")
	EGunslingingAttackEnum GunslingingAttackType;

	//Property used to set the current unarmed attack.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Input Commands")
	EUnarmedAttackEnum UnarmedAttackType;
	
protected:
	//Function used for adjusting the "depth" of aiming crosshairs. Crosshairs need to "stick" for every surface that belongs to the custom trace channel (GameTraceChannel11).
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AdjustCrosshairsDistanceAndScale(class AUrathaCharacter *playerCharacter, class UWidgetComponent *crosshairs, bool weaponEqiuped, float &calculatedFiringDistance);


	///Attack abilities

	//Choose attack abilities to add to UI. Gets the ability info of passed abilities and calls the blueprint implementable event (which switches to calling UI functions) to add them to the UI. 
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AddAttackAbilitiesToUI(TSubclassOf<UUrathaAbility> attackAbilityPress, TSubclassOf<UUrathaAbility> attackAbilityHold);

	//Blueprint implementable event that calls UI function used to display gameplay abilities UI components. Used for both press and hold commands.
	UFUNCTION(BlueprintImplementableEvent, Category = "Uratha")
	void AddAbilityToUI(FUrathaAbilityInfo abilityInfo, bool attackPress);

	//Blueprint implementable event that calls UI function used to display gameplay abilities UI components. Used for both press and hold commands.
	UFUNCTION(BlueprintImplementableEvent, Category = "Uratha")
	void AddAttackAbilityToUI(FUrathaAbilityInfo abilityInfo, bool activatesWithHold);

	///Gifts

	//Choose gifts to add to main UI (HUD). Gets the gift info of passed gifts and calls the blueprint implementable event (which switches to calling UI functions) to add them to the HUD. 
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AddSelectedGiftsToUI(TSubclassOf<UUrathaGift> attackGift, TSubclassOf<UUrathaGift> defenseGift, TSubclassOf<UUrathaGift> utilityGift, 
							FUrathaGiftInfo& attackGiftInfo, FUrathaGiftInfo& defenseGiftInfo, FUrathaGiftInfo& utilityGiftInfo);

	//Uses the gift info stored into game instance to add them to main UI (HUD). After the initial selection, selected gifts are stored into the game instance in order to save the user selection.
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AddSelectedGiftsToUIFromGameInstance();

	//Blueprint implementable event that calls UI function used to display gifts UI components in the HUD.
	UFUNCTION(BlueprintImplementableEvent, Category = "Uratha")
	void AddSelectedGiftsToUIEvent(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo);

	//Choose selected gifts to add to quick access menu. Gets the gift info of passed gifts and calls the blueprint implementable event (which switches to calling UI functions) to add them to the UI. 
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AddSelectedGiftsToQuickAccessMenu(TSubclassOf<UUrathaGift> attackGift, TSubclassOf<UUrathaGift> defenseGift, TSubclassOf<UUrathaGift> utilityGift);

	//Blueprint implementable event that calls UI function used to display gifts UI components in the quick access menu.
	UFUNCTION(BlueprintImplementableEvent, Category = "Uratha")
	void AddSelectedGiftsToQuickAccessMenuEvent(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo);

	//Choose selectee gifts to add to quick access menu. Gets the gift info of passed gifts and calls the blueprint implementable event (which switches to calling UI functions) to add them to the UI. 
	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void AddSelecteeGiftsToQuickAccessMenu(TSubclassOf<UUrathaGift> attackGiftFirst, TSubclassOf<UUrathaGift> attackGiftSecond, TSubclassOf<UUrathaGift> attackGiftThird, 
											TSubclassOf<UUrathaGift> defenseGiftFirst, TSubclassOf<UUrathaGift> defenseGiftSecond, TSubclassOf<UUrathaGift> defenseGiftThird,
											TSubclassOf<UUrathaGift> utilityGiftFirst, TSubclassOf<UUrathaGift> utilityGiftSecond, TSubclassOf<UUrathaGift> utilityGiftThird);

	//Blueprint implementable event that calls UI function used to display gifts UI components in the quick access menu.
	UFUNCTION(BlueprintImplementableEvent, Category = "Uratha")
	void AddSelecteeGiftsToQuickAccessMenuEvent(FUrathaGiftInfo attackGiftInfoFirst, FUrathaGiftInfo attackGiftInfoSecond, FUrathaGiftInfo attackGiftInfoThird,
												FUrathaGiftInfo defenseGiftInfoFirst, FUrathaGiftInfo defenseGiftInfoSecond, FUrathaGiftInfo defenseGiftInfoThird,
												FUrathaGiftInfo utilityGiftInfoFirst, FUrathaGiftInfo utilityGiftInfoSecond, FUrathaGiftInfo utilityGiftInfoThird);
};
