// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "UrathaAIController.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaAIController : public AAIController
{
	GENERATED_BODY()
	
protected:
	//Distance at AI will stop when chasing the player. Ranged NPCs will attack from this distance.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UrathaAI")
	float MinimumDistanceFromPlayer = 400.f;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "UrathaAI")
	float DistanceForDefaultAimingOffset = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAI")
	float DefaultAimingOffset = 55.f;

public:
	FVector GetFocalPointOnActor(const AActor *Actor) const override;

};
