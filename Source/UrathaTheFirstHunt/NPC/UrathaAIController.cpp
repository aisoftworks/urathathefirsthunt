// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaAIController.h"

FVector AUrathaAIController::GetFocalPointOnActor(const AActor *Actor) const
{
	FVector AILocation = GetPawn()->GetActorLocation();
	FVector PlayerLocation = Actor->GetActorLocation();
	float distance = FVector::Distance(AILocation, PlayerLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), distance);
	float offset = DefaultAimingOffset;//Default offset when NPC is 1 meter (100cm) from the Player
	if (distance > DistanceForDefaultAimingOffset)
	{
		float distancePercentage = distance / MinimumDistanceFromPlayer;
		//UE_LOG(LogTemp, Warning, TEXT("Distance percentage: %f"), distancePercentage);
		float offsetIncrement = offset * distancePercentage;
		offset += offsetIncrement;
	}

	return Actor != nullptr ?
		FVector(Actor->GetActorLocation().X, Actor->GetActorLocation().Y, Actor->GetActorLocation().Z + offset)
		: FAISystem::InvalidLocation;
}
