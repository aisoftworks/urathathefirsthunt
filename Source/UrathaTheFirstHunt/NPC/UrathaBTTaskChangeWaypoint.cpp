// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaBTTaskChangeWaypoint.h"
#include "UrathaAIController.h"
#include "Character/UrathaPatrolComponent.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UUrathaBTTaskChangeWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UUrathaPatrolComponent* PatrolComponent = OwnerComp.GetAIOwner()->GetPawn()->FindComponentByClass<UUrathaPatrolComponent>();
	if (!ensure(PatrolComponent))
	{
		UE_LOG(LogTemp, Warning, TEXT("No patrol component!"));
		return EBTNodeResult::Failed;
	}

	TArray<AActor*> PatrolPoints = PatrolComponent->GetPatrolPoints();
	if (PatrolPoints.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("No patrol points assigned!"));
		return EBTNodeResult::Failed;
	}

	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	UE_LOG(LogTemp, Warning, TEXT("Blackboard found!"));
	int Index = BlackboardComponent->GetValueAsInt(NextWaypointIndex.SelectedKeyName);
	BlackboardComponent->SetValueAsObject(Waypoint.SelectedKeyName, PatrolPoints[Index]);

	if (Index == PatrolPoints.Num())
	{
		//avoid skipping by indefinite increase of index beyond 2^32
		Index = 0;
	}
	int NextIndex = (Index + 1) % PatrolPoints.Num();
	BlackboardComponent->SetValueAsInt(NextWaypointIndex.SelectedKeyName, NextIndex);

	return EBTNodeResult::Succeeded;
}
