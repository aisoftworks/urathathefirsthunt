// Fill out your copyright notice in the Description page of Project Settings.

#include "UrathaPlayerController.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaAbility.h"
#include "Weapons/UrathaWeapon.h"
#include "Camera/CameraComponent.h"
#include "Components/WidgetComponent.h"
#include "UrathaGameInstance.h"

void AUrathaPlayerController::AdjustCrosshairsDistanceAndScale(AUrathaCharacter *playerCharacter, UWidgetComponent *crosshairs, bool weaponEqiuped, float &calculatedFiringDistance)
{
	float NewCrosshairsDistance = 0.f;

	//UE_LOG(LogTemp, Warning, TEXT("Tracing."));

	//Setting the weapon (aim) range. If unarmed, aim is set at the 20m.
	float WeaponRange = 0.f;
	if (weaponEqiuped)
	{
		WeaponRange = playerCharacter->GetWeapon()->WeaponRange;
	}
	else
	{
		WeaponRange = 2000.f;
	}

	//Setting up trace vectors.
	FVector CameraLocation = playerCharacter->GetFollowCamera()->GetComponentTransform().GetLocation();
	FVector CameraForwardVector = playerCharacter->GetFollowCamera()->GetForwardVector();

	FVector TraceStartVector = CameraLocation;
	FVector TraceEndVector = CameraLocation + CameraForwardVector * WeaponRange;

	FCollisionQueryParams CollisionQueryParams;
	CollisionQueryParams.AddIgnoredActor(playerCharacter->GetUniqueID());
	CollisionQueryParams.bTraceComplex = false;
	CollisionQueryParams.bReturnPhysicalMaterial = false;

	FHitResult HitResult;
	if (GetWorld()->LineTraceSingleByChannel(HitResult, TraceStartVector, TraceEndVector, ECollisionChannel::ECC_GameTraceChannel11, CollisionQueryParams))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Distance: %f"), HitResult.Distance);

		//Move crosshairs towards a player by a small distance (to avoid rendering problems).
		NewCrosshairsDistance = HitResult.Distance - 50.f;
		//Scale the crosshairs based on the distance from the hit surface.
		float CrosshairsScale = HitResult.Distance / WeaponRange;
		crosshairs->SetRelativeScale3D(FVector(CrosshairsScale, CrosshairsScale, CrosshairsScale));
	}
	else
	{
		//if no surface is hit within the weapon range, resort to default aim range.
		NewCrosshairsDistance = WeaponRange;
		crosshairs->SetRelativeScale3D(FVector(1.f, 1.f, 1.f));
	}
	
	//Set the crosshairs position (by X only).
	crosshairs->SetRelativeLocation(FVector(
		NewCrosshairsDistance,
		crosshairs->GetRelativeTransform().GetLocation().Y,
		crosshairs->GetRelativeTransform().GetLocation().Z
	));

	//return the calculated firing distance used in other functions.
	calculatedFiringDistance = NewCrosshairsDistance;
}

void AUrathaPlayerController::AddAttackAbilitiesToUI(TSubclassOf<UUrathaAbility> attackAbilityPress, TSubclassOf<UUrathaAbility> attackAbilityHold)
{
	UUrathaAbility* AbilityPress = attackAbilityPress->GetDefaultObject<UUrathaAbility>();
	UUrathaAbility* AbilityHold = attackAbilityHold->GetDefaultObject<UUrathaAbility>();

	if (AbilityPress && AbilityHold)
	{
		FUrathaAbilityInfo AbilityPressInfo = AbilityPress->GetAbilityInfo();
		FUrathaAbilityInfo AbilityHoldInfo = AbilityHold->GetAbilityInfo();
		/*AddAbilityToUI(AbilityPressInfo, true);
		AddAbilityToUI(AbilityHoldInfo, false);*/
		AddAttackAbilityToUI(AbilityHoldInfo, true);
		AddAttackAbilityToUI(AbilityPressInfo, false);
	}
}

void AUrathaPlayerController::AddSelectedGiftsToUI(TSubclassOf<UUrathaGift> attackGift, TSubclassOf<UUrathaGift> defenseGift, TSubclassOf<UUrathaGift> utilityGift, 
												FUrathaGiftInfo& attackGiftInfo, FUrathaGiftInfo& defenseGiftInfo, FUrathaGiftInfo& utilityGiftInfo)
{
	UUrathaGift* AttackGift = attackGift->GetDefaultObject<UUrathaGift>();
	UUrathaGift* DefenseGift = defenseGift->GetDefaultObject<UUrathaGift>();
	UUrathaGift* UtilityGift = utilityGift->GetDefaultObject<UUrathaGift>();

	if (AttackGift && DefenseGift && UtilityGift)
	{
		FUrathaGiftInfo AttackGiftInfo = AttackGift->GetGiftInfo();
		FUrathaGiftInfo DefenseGiftInfo = DefenseGift->GetGiftInfo();
		FUrathaGiftInfo UtilityGiftInfo = UtilityGift->GetGiftInfo();
		
		//Pass by reference
		attackGiftInfo = AttackGiftInfo;
		defenseGiftInfo = DefenseGiftInfo;
		utilityGiftInfo = UtilityGiftInfo;

		AddSelectedGiftsToUIEvent(AttackGiftInfo, DefenseGiftInfo, UtilityGiftInfo);
	}
}

void AUrathaPlayerController::AddSelectedGiftsToUIFromGameInstance()
{
	FUrathaGiftInfo AttackGiftInfo;
	FUrathaGiftInfo DefenseGiftInfo;
	FUrathaGiftInfo UtilityGiftInfo;

	UUrathaGameInstance* GameInstance = Cast<UUrathaGameInstance>(GetGameInstance());
	GameInstance->GetSelectedGiftsInfo(AttackGiftInfo, DefenseGiftInfo, UtilityGiftInfo);

	AddSelectedGiftsToUIEvent(AttackGiftInfo, DefenseGiftInfo, UtilityGiftInfo);
}

void AUrathaPlayerController::AddSelectedGiftsToQuickAccessMenu(TSubclassOf<UUrathaGift> attackGift, TSubclassOf<UUrathaGift> defenseGift, TSubclassOf<UUrathaGift> utilityGift)
{
	UUrathaGift* AttackGift = attackGift->GetDefaultObject<UUrathaGift>();
	UUrathaGift* DefenseGift = defenseGift->GetDefaultObject<UUrathaGift>();
	UUrathaGift* UtilityGift = utilityGift->GetDefaultObject<UUrathaGift>();

	if (AttackGift && DefenseGift && UtilityGift)
	{
		FUrathaGiftInfo AttackGiftInfo = AttackGift->GetGiftInfo();
		FUrathaGiftInfo DefenseGiftInfo = DefenseGift->GetGiftInfo();
		FUrathaGiftInfo UtilityGiftInfo = UtilityGift->GetGiftInfo();
		
		AddSelectedGiftsToQuickAccessMenuEvent(AttackGiftInfo, DefenseGiftInfo, UtilityGiftInfo);
	}
}

void AUrathaPlayerController::AddSelecteeGiftsToQuickAccessMenu(TSubclassOf<UUrathaGift> attackGiftFirst, TSubclassOf<UUrathaGift> attackGiftSecond, TSubclassOf<UUrathaGift> attackGiftThird, TSubclassOf<UUrathaGift> defenseGiftFirst, TSubclassOf<UUrathaGift> defenseGiftSecond, TSubclassOf<UUrathaGift> defenseGiftThird, TSubclassOf<UUrathaGift> utilityGiftFirst, TSubclassOf<UUrathaGift> utilityGiftSecond, TSubclassOf<UUrathaGift> utilityGiftThird)
{
	UUrathaGift* AttackGift1 = attackGiftFirst->GetDefaultObject<UUrathaGift>();
	UUrathaGift* AttackGift2 = attackGiftSecond->GetDefaultObject<UUrathaGift>();
	UUrathaGift* AttackGift3 = attackGiftThird->GetDefaultObject<UUrathaGift>();
	UUrathaGift* DefenseGift1 = defenseGiftFirst->GetDefaultObject<UUrathaGift>();
	UUrathaGift* DefenseGift2 = defenseGiftSecond->GetDefaultObject<UUrathaGift>();
	UUrathaGift* DefenseGift3 = defenseGiftThird->GetDefaultObject<UUrathaGift>();
	UUrathaGift* UtilityGift1 = utilityGiftFirst->GetDefaultObject<UUrathaGift>();
	UUrathaGift* UtilityGift2 = utilityGiftSecond->GetDefaultObject<UUrathaGift>();
	UUrathaGift* UtilityGift3 = utilityGiftThird->GetDefaultObject<UUrathaGift>();

	if (AttackGift1 && AttackGift2 && AttackGift3 && DefenseGift1 && DefenseGift2 && DefenseGift3 && UtilityGift1 && UtilityGift2 && UtilityGift3)
	{
		FUrathaGiftInfo AttackGiftInfo1 = AttackGift1->GetGiftInfo();
		FUrathaGiftInfo AttackGiftInfo2 = AttackGift2->GetGiftInfo();
		FUrathaGiftInfo AttackGiftInfo3 = AttackGift3->GetGiftInfo();
		FUrathaGiftInfo DefenseGiftInfo1 = DefenseGift1->GetGiftInfo();
		FUrathaGiftInfo DefenseGiftInfo2 = DefenseGift2->GetGiftInfo();
		FUrathaGiftInfo DefenseGiftInfo3 = DefenseGift3->GetGiftInfo();
		FUrathaGiftInfo UtilityGiftInfo1 = UtilityGift1->GetGiftInfo();
		FUrathaGiftInfo UtilityGiftInfo2 = UtilityGift2->GetGiftInfo();
		FUrathaGiftInfo UtilityGiftInfo3 = UtilityGift3->GetGiftInfo();

		AddSelecteeGiftsToQuickAccessMenuEvent(AttackGiftInfo1, AttackGiftInfo2, AttackGiftInfo3, DefenseGiftInfo1, DefenseGiftInfo2, DefenseGiftInfo3, UtilityGiftInfo1, UtilityGiftInfo2, UtilityGiftInfo3);
	}
}
