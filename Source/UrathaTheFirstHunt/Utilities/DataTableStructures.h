// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "DataTableStructures.generated.h"

USTRUCT()
struct FAbilitiesData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FAbilitiesData() :
		AbilityID(0),
		AbilityName(""),
		AbilityType(""),
		UnlocksAt(0)
	{}

	UPROPERTY(EditDefaultsOnly)
	int AbilityID;

	UPROPERTY(EditDefaultsOnly)
	FString AbilityName;

	UPROPERTY(EditDefaultsOnly)
	FString AbilityType;

	UPROPERTY(EditDefaultsOnly)
	int UnlocksAt;
};

USTRUCT()
struct FTalentLevelsData : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()

public:
	FTalentLevelsData() :
		TalentName(""),
		TalentLevel(0),
		AbilityID(-1),
		AbilityName("")
	{}

	UPROPERTY(EditDefaultsOnly)
		FString TalentName;

	UPROPERTY(EditDefaultsOnly)
		int TalentLevel;

	UPROPERTY(EditDefaultsOnly)
		int AbilityID;

	UPROPERTY(EditDefaultsOnly)
		FString AbilityName;
};

/**
 * 
 */
class URATHATHEFIRSTHUNT_API DataTableStructures : public UObject
{
public:
	DataTableStructures();
	~DataTableStructures();

	void GetSomething()
	{
		
	}
};
