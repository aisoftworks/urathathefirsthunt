// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include <typeinfo>
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "GameServices/BaseGameService.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "EngineMinimal.h"
#include "Character/UrathaGift.h"
#include "UrathaGameInstance.generated.h"

using namespace std;

/**
 * Custom GameInstance class that will implement a variation of ServiceLocator pattern to hold references of different "services" such as Ability descriptions, Save/Load system,
 * etc., and persist them throughout the whole game.
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
protected:
	virtual void Init() override;

private:
	//Old code for service provider pattern. May need to delete this.
	TMap<FString, BaseGameService*> GameServices;

	UPROPERTY(EditDefaultsOnly)
	UDataTable *AbilitiesDataTable;

	UPROPERTY(EditDefaultsOnly)
	UDataTable *TalentLevelDataTable;

protected:
	//Property used to save gift info.
	UPROPERTY(BlueprintReadWrite, Category = "Uratha Game Instance")
	FUrathaGiftInfo SelectedAttackGiftInfo;

	//Property used to save gift info.
	UPROPERTY(BlueprintReadWrite, Category = "Uratha Game Instance")
	FUrathaGiftInfo SelectedDefenseGiftInfo;

	//Property used to save gift info.
	UPROPERTY(BlueprintReadWrite, Category = "Uratha Game Instance")
	FUrathaGiftInfo SelectedUtilityGiftInfo;
	
public:
	UUrathaGameInstance();
	~UUrathaGameInstance();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Uratha UI Gifts")
	TSubclassOf<UUrathaGift> DefaultUIGiftAttack;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Uratha UI Gifts")
	TSubclassOf<UUrathaGift> DefaultUIGiftDefense;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Uratha UI Gifts")
	TSubclassOf<UUrathaGift> DefaultUIGiftUtility;

	/*Gets a GameService instance that's a child class of BaseGameService class. Doesn't accept other types.*/
	template<typename T>
	typename enable_if<is_base_of<BaseGameService, T>::value, T*>::type
	GetGameService()
	{
		return (T*)GameServices.FindChecked(FString(typeid(T).name()));
	};

	UDataTable* GetAbilitiesDataTable()
	{
		return AbilitiesDataTable;
	}

	UDataTable* GetTalentLevelsDataTable()
	{
		return TalentLevelDataTable;
	}

	//Stores the selected gift info.
	UFUNCTION(BlueprintCallable, Category = "Uratha UI Gifts")
	void SetSelectedGiftsInfo(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo);

	//Retrieves the selected gift info.
	UFUNCTION(BlueprintCallable, Category = "Uratha UI Gifts")
	void GetSelectedGiftsInfo(FUrathaGiftInfo &attackGiftInfo, FUrathaGiftInfo &defenseGiftInfo, FUrathaGiftInfo &utilityGiftInfo);
};
