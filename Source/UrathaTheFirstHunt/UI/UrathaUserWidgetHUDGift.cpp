// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDGift.h"

void UUrathaUserWidgetHUDGift::NativeConstruct()
{
	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDGift::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	if (InCooldownState)
	{
		UE_LOG(LogTemp, Error, TEXT("GIFT in cooldown..."));
		if (CooldownTimer <= GiftInfo.Cooldown)
		{
			UE_LOG(LogTemp, Error, TEXT("GIFT CooldownTimer: %f, Cooldown: %f"), CooldownTimer, GiftInfo.Cooldown);
			CooldownTimer += InDeltaTime;
			IconMaterialInstance->SetScalarParameterValue(FName("CooldownPercentage"), CooldownTimer / GiftInfo.Cooldown);
		}
		else
		{
			CooldownTimer = 0.f;
			InCooldownState = false;
		}
	}

	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UUrathaUserWidgetHUDGift::SetGiftHUDComponent(FUrathaGiftInfo giftInfo)
{
	UE_LOG(LogTemp, Error, TEXT("GIFT ADDED TO HUD"));
	GiftInfo = giftInfo;
	UE_LOG(LogTemp, Error, TEXT("GIFT Cooldown: %f"), GiftInfo.Cooldown);
	IconMaterialInstance = UMaterialInstanceDynamic::Create(GiftInfo.UIMaterialInstance, this);
	IconMaterialInstance->SetTextureParameterValue(FName("IconTexture"), GiftInfo.HUDIcon);
	GiftIcon->SetBrushFromMaterial(IconMaterialInstance);
}

void UUrathaUserWidgetHUDGift::StartGiftCooldown()
{
	IconMaterialInstance->SetScalarParameterValue(FName("CooldownPercentage"), 0.f);
	InCooldownState = true;
}
