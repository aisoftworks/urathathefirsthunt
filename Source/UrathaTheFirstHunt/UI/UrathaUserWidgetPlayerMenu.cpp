// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaUserWidgetPlayerMenu.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaCharacterAbilities.h"
#include "Character/UrathaGiftCollection.h"

void UUrathaUserWidgetPlayerMenu::BindRanksDelegates()
{
#pragma region MentalSoakBinds
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Intelligence->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_TechSavviness }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Wits->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Sense, UI_TalentGiftCharacter_Bashing }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Resolve->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Medicine, UI_TalentGiftCharacter_Lethal }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Science->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Medicine }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Investigation->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Sense }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Technology->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_TechSavviness }
	));

	UI_RanksCharacter_Intelligence->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Wits->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Resolve->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Science->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Investigation->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Technology->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
#pragma endregion

#pragma region PhysicalSoakBinds
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Strength->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Unarmed }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Agility->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_SneakAndLarceny }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Physique->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { 
			UI_TalentGiftCharacter_Gunslinging, 
			UI_TalentGiftCharacter_Bashing, UI_TalentGiftCharacter_Lethal, UI_TalentGiftCharacter_Aggravated }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Firearms->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Gunslinging }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Stealth->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_SneakAndLarceny }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Brawl->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Unarmed }
	));

	UI_RanksCharacter_Strength->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Agility->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Physique->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Firearms->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Stealth->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Brawl->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
#pragma endregion

#pragma region SocialBinds
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Presence->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_FrightfulPresence }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Manipulation->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_SmoothTalking }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Composure->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Deception }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Subterfuge->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_Deception }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Persuasion->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_SmoothTalking }
	));
	UIAttributeTalentPairs.Add(
		UI_RanksCharacter_Intimidation->AttributeToAssign,
		FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*>() = { UI_TalentGiftCharacter_FrightfulPresence }
	));

	UI_RanksCharacter_Presence->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Manipulation->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Composure->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Subterfuge->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Persuasion->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
	UI_RanksCharacter_Intimidation->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged);
#pragma endregion

#pragma region GiftBinds
	UIRenownGiftPairs.Add(
		UI_RanksCharacter_Honor->AttributeToAssign,
		FUIGiftArray(TArray<FUIGift>() = 
			{
				FUIGift(UI_GiftNumberCharacter_Attack, EUrathaGiftType::Attack),
				FUIGift(UI_GiftNumberCharacter_Defense, EUrathaGiftType::Defense)
			}	
	));
	UIRenownGiftPairs.Add(
		UI_RanksCharacter_Glory->AttributeToAssign,
		FUIGiftArray(TArray<FUIGift>() =
			{
				FUIGift(UI_GiftNumberCharacter_Attack, EUrathaGiftType::Attack),
				FUIGift(UI_GiftNumberCharacter_Defense, EUrathaGiftType::Defense),
				FUIGift(UI_GiftNumberCharacter_Utility, EUrathaGiftType::Utility)
			}
	));
	UIRenownGiftPairs.Add(
		UI_RanksCharacter_Cunning->AttributeToAssign,
		FUIGiftArray(TArray<FUIGift>() =
			{
				FUIGift(UI_GiftNumberCharacter_Utility, EUrathaGiftType::Utility)
			}
	));
	UIRenownGiftPairs.Add(
		UI_RanksCharacter_Wisdom->AttributeToAssign,
		FUIGiftArray(TArray<FUIGift>() =
			{
				FUIGift(UI_GiftNumberCharacter_Attack, EUrathaGiftType::Attack),
				FUIGift(UI_GiftNumberCharacter_Defense, EUrathaGiftType::Defense),
				FUIGift(UI_GiftNumberCharacter_Utility, EUrathaGiftType::Utility)
			}
	));
	UIRenownGiftPairs.Add(
		UI_RanksCharacter_Purity->AttributeToAssign,
		FUIGiftArray(TArray<FUIGift>() =
			{
				FUIGift(UI_GiftNumberCharacter_Attack, EUrathaGiftType::Attack),
				FUIGift(UI_GiftNumberCharacter_Defense, EUrathaGiftType::Defense)
			}
	));

	UI_RanksCharacter_Honor->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIRenownValueChanged);
	UI_RanksCharacter_Glory->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIRenownValueChanged);
	UI_RanksCharacter_Cunning->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIRenownValueChanged);
	UI_RanksCharacter_Wisdom->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIRenownValueChanged);
	UI_RanksCharacter_Purity->OnAttributeRankChanged.AddDynamic(this, &UUrathaUserWidgetPlayerMenu::UIRenownValueChanged);
#pragma endregion

	GEngine->GameViewport->Viewport->ViewportResizedEvent.AddUObject(this, &UUrathaUserWidgetPlayerMenu::ViewportResized);
}

void UUrathaUserWidgetPlayerMenu::NativeConstruct()
{
	Super::NativeConstruct();
}

void UUrathaUserWidgetPlayerMenu::InitPlayerMenu()
{
	UIAttributeTalentPairs = TMap<FGameplayAttribute, FUITalentArray>();
	
	BindRanksDelegates();

	//TODO: Remove this when gift unlocking is properly implemented.
	UI_GiftNumberCharacter_Attack->InitUIGiftNumberValues();
	UI_GiftNumberCharacter_Defense->InitUIGiftNumberValues();
	UI_GiftNumberCharacter_Utility->InitUIGiftNumberValues();
}

void UUrathaUserWidgetPlayerMenu::PrepareGiftSetForSelection()
{
	/*UUrathaCharacterAbilities* characterAbilities = 
		Cast<UUrathaCharacterAbilities>
		(UI_GiftNumberCharacter_Attack->GetPlayerCharacter()->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));*/

	AUrathaCharacter* PlayerCharacter = Cast<AUrathaCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));

	UUrathaCharacterAbilities* characterAbilities =
		Cast<UUrathaCharacterAbilities>
		(PlayerCharacter->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));

	characterAbilities->GetUrathaGiftCollection()->PrepareGiftSet();
}

void UUrathaUserWidgetPlayerMenu::UIAttributeValueChanged(FGameplayAttribute changedAttribute)
{
	FindAndUpdateUITalentValue(changedAttribute);
}

void UUrathaUserWidgetPlayerMenu::FindAndUpdateUITalentValue(FGameplayAttribute changedAttribute)
{
	FUITalentArray* UITalent = UIAttributeTalentPairs.Find(changedAttribute);
	if (UITalent)
	{
		UITalent->UpdateUITalentLevelValue();
	}
}

void UUrathaUserWidgetPlayerMenu::UIRenownValueChanged(FGameplayAttribute changedRenown)
{
	FindAndUpdateUIGiftNumberValue(changedRenown);
}

void UUrathaUserWidgetPlayerMenu::FindAndUpdateUIGiftNumberValue(FGameplayAttribute changedRenown)
{
	FUIGiftArray* UIGiftNumber = UIRenownGiftPairs.Find(changedRenown);
	if (UIGiftNumber)
	{
		UIGiftNumber->UpdateUIGiftNumberValues(changedRenown);
	}
}

void UUrathaUserWidgetPlayerMenu::ViewportResized(FViewport* viewport, uint32 someInt)
{
	UE_LOG(LogTemp, Warning, TEXT("Resized... DPI scale: %f"), GEngine->GameViewport->GetDPIScale());
	//InvalidationBoxLinesMental->InvalidateCache();
	//this->RebuildWidget();
	//this->Invalidate();
	
}
