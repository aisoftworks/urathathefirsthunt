// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDGiftsAndDodge.h"

void UUrathaUserWidgetHUDGiftsAndDodge::NativeConstruct()
{
	GiftsDodgeCommandIcons = TArray<UUrathaUserWidgetHUDCommandIcon*>();

	GiftsDodgeCommandIcons.Add(UI_HUD_Component_CommandIcon_GiftDefense);
	GiftsDodgeCommandIcons.Add(UI_HUD_Component_CommandIcon_GiftAttack);
	GiftsDodgeCommandIcons.Add(UI_HUD_Component_CommandIcon_GiftUtility);

	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDGiftsAndDodge::SetGiftsInHUD(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo)
{
	UI_HUD_Component_GiftAttack->SetGiftHUDComponent(attackGiftInfo);
	Text_GiftAttack->SetText(FText::FromString(attackGiftInfo.Name));

	UI_HUD_Component_GiftDefense->SetGiftHUDComponent(defenseGiftInfo);
	Text_GiftDefense->SetText(FText::FromString(defenseGiftInfo.Name));

	UI_HUD_Component_GiftUtility->SetGiftHUDComponent(utilityGiftInfo);
	Text_GiftUtility->SetText(FText::FromString(utilityGiftInfo.Name));
}

void UUrathaUserWidgetHUDGiftsAndDodge::ChangeGiftsCommandIcons(bool isGamepadInput)
{
	for (UUrathaUserWidgetHUDCommandIcon* commandIcon : GiftsDodgeCommandIcons)
	{
		commandIcon->SetIconTextureByInput(isGamepadInput);
	}
}

void UUrathaUserWidgetHUDGiftsAndDodge::StartAttackGiftCooldown()
{
	UI_HUD_Component_GiftAttack->StartGiftCooldown();
}

void UUrathaUserWidgetHUDGiftsAndDodge::StartDefenseGiftCooldown()
{
	UI_HUD_Component_GiftDefense->StartGiftCooldown();
}

void UUrathaUserWidgetHUDGiftsAndDodge::StartUtilityGiftCooldown()
{
	UI_HUD_Component_GiftUtility->StartGiftCooldown();
}
