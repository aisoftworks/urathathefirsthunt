// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaWidgetCharacterTalentGift.h"
#include "../Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Misc/Char.h"
#include "Styling/SlateColor.h"
#include "Math/Color.h"
#include "Character/UrathaCharacterAbilities.h"
#include "Character/UrathaGiftCollection.h"

FString UUrathaWidgetCharacterTalentGift::AddSpacesToTalentName(FString talentName)
{
	FString TempTalentName = talentName;
	TArray<TCHAR> TalentNameArray = TempTalentName.GetCharArray();
	int UpperCount = -1;
	int InsertIncrement = -1;
	for (int charIndex = 0; charIndex < TalentNameArray.Num(); charIndex++)
	{
		if (FChar::IsUpper(TalentNameArray[charIndex]) || TalentNameArray[charIndex] == '&')
		{
			UpperCount++;
			if (UpperCount > 0)
			{
				InsertIncrement++;
				TempTalentName.InsertAt(charIndex + InsertIncrement, TEXT(" "));
				//UE_LOG(LogTemp, Error, TEXT("%s"), *TempTalentName);
			}
		}
	}
	
	return TempTalentName;
}

void UUrathaWidgetCharacterTalentGift::SetUIName()
{
	if (TalentToAssign != nullptr && GiftType == EUrathaGiftType::None)
	{
		UCanvasPanelSlot* NameCanvasSlot = Cast<UCanvasPanelSlot>(Name->Slot);
		//UCanvasPanelSlot* NameCanvasSlot = Cast<UCanvasPanelSlot>(InvalidationBoxName->Slot);

		FString TalentName = TalentToAssign.GetName();

		if (TalentName.Contains("And", ESearchCase::CaseSensitive))
		{
			TalentName = TalentName.Replace(TEXT("And"), TEXT("&"), ESearchCase::CaseSensitive);
		}

		TalentName = AddSpacesToTalentName(TalentName);
		//UE_LOG(LogTemp, Error, TEXT("%s"), *TalentName);

		if (TalentName.Len() > 11)
		{
			Name->SetAutoWrapText(true);
			NameCanvasSlot->SetAlignment(FVector2D(0.51f, 0.65f));
		}
		else
		{
			Name->SetAutoWrapText(false);
			NameCanvasSlot->SetAlignment(FVector2D(0.51f, 0.25f));
		}

		Name->SetText(FText::FromString(TalentName));
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("IT IS A GIFT!"));
		TArray<FStringFormatArg> args;

		switch (GiftType)
		{
		case EUrathaGiftType::Attack:
		{
			args.EmplaceAt(0, "Attack");
			Name->SetText(FText::FromString(FString::Format(TEXT("{0} gifts"), args)));
		}
			break;
		case EUrathaGiftType::Defense:
		{
			args.EmplaceAt(0, "Defense");
			Name->SetText(FText::FromString(FString::Format(TEXT("{0} gifts"), args)));
		}
			break;
		case EUrathaGiftType::Utility:
		{
			args.EmplaceAt(0, "Utility");
			Name->SetText(FText::FromString(FString::Format(TEXT("{0} gifts"), args)));
		}
			break;
		}
	}
}

void UUrathaWidgetCharacterTalentGift::NativePreConstruct()
{
	if (TalentPlaceholderImage)
	{
		ImagePlaceholder->SetBrushFromTexture(TalentPlaceholderImage);
	}
	if (UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{
		//UE_LOG(LogTemp, Warning, TEXT("Player found."));
		PlayerCharacter = Cast<AUrathaCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		PlayerAttributesAndTalents = PlayerCharacter->GetAttributeTalentSet();
		SetUIName();
		if (GiftType == EUrathaGiftType::None)
		{
			SetUITalentLevelValue(TalentToAssign.GetNumericValueChecked(PlayerAttributesAndTalents));
		}
		else
		{
			//UE_LOG(LogTemp, Warning, TEXT("UI gift found."));
			SetUIGiftNumberValues(0, 0, 0);
		}
	}

	Super::NativePreConstruct();
}

void UUrathaWidgetCharacterTalentGift::NativeConstruct()
{
	

	Super::NativeConstruct();
}

void UUrathaWidgetCharacterTalentGift::SetUITalentBonusPenalty()
{
	if (PlayerAttributesAndTalents->TalentHasBonus(TalentToAssign))
	{
		Level->SetColorAndOpacity(UWidget::ConvertLinearColorToSlateColor(FLinearColor(0.f, 0.8f, 0.f, 1.f)));
	}
	else if (PlayerAttributesAndTalents->TalentHasPenalty(TalentToAssign))
	{
		Level->SetColorAndOpacity(UWidget::ConvertLinearColorToSlateColor(FLinearColor(1.f, 0.15f, 0.f, 1.f)));
	}
	else
	{
		Level->SetColorAndOpacity(UWidget::ConvertLinearColorToSlateColor(FLinearColor(1.f, 1.f, 1.f, 1.f)));
	}
}

void UUrathaWidgetCharacterTalentGift::SetUITalentLevelValue(float levelValue)
{
	TArray<FStringFormatArg> args;
	args.Add(FStringFormatArg((int)levelValue));
	Level->SetText(FText::FromString(FString::Format(TEXT("{0}"), args)));

	SetUITalentBonusPenalty();
}

void UUrathaWidgetCharacterTalentGift::UpdateUITalentLevelValue()
{
	SetUITalentLevelValue(TalentToAssign.GetNumericValueChecked(PlayerAttributesAndTalents));
}

void UUrathaWidgetCharacterTalentGift::SetUIGiftNumberValues(int attackGiftsNumber, int defenseGiftsNumber, int utilityGiftsNumber)
{
	//UE_LOG(LogTemp, Warning, TEXT("Attack: %d Defense: %d Utility: %d"), attackGiftsNumber, defenseGiftsNumber, utilityGiftsNumber);

	TArray<FStringFormatArg> args;
	switch (GiftType)
	{
	case EUrathaGiftType::Attack:
	{
		args.EmplaceAt(0, FStringFormatArg((int)attackGiftsNumber));
		Level->SetText(FText::FromString(FString::Format(TEXT("{0}/3"), args)));
	}
	break;
	case EUrathaGiftType::Defense:
	{
		args.EmplaceAt(0, FStringFormatArg((int)defenseGiftsNumber));
		Level->SetText(FText::FromString(FString::Format(TEXT("{0}/3"), args)));
	}
	break;
	case EUrathaGiftType::Utility:
	{
		args.EmplaceAt(0, FStringFormatArg((int)utilityGiftsNumber));
		Level->SetText(FText::FromString(FString::Format(TEXT("{0}/3"), args)));
	}
	break;
	}
}

void UUrathaWidgetCharacterTalentGift::UpdateUIGiftNumberValues(FGameplayAttribute changedRenown)
{
	//UE_LOG(LogTemp, Warning, TEXT("Initialized by: %s"), *this->GetFName().ToString());
	UUrathaCharacterAbilities* characterAbilities = Cast<UUrathaCharacterAbilities>(PlayerCharacter->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
	int attackGifts = 0, defenseGifts = 0, utilityGifts = 0;
	characterAbilities->GetUrathaGiftCollection()->CheckForNumberOfUnlockedGifts(changedRenown, PlayerAttributesAndTalents, attackGifts, defenseGifts, utilityGifts);
	SetUIGiftNumberValues(attackGifts, defenseGifts, utilityGifts);
}

void UUrathaWidgetCharacterTalentGift::InitUIGiftNumberValues()
{
	//UE_LOG(LogTemp, Warning, TEXT("Initialized by: %s"), *this->GetFName().ToString());
	UUrathaCharacterAbilities* characterAbilities = Cast<UUrathaCharacterAbilities>(PlayerCharacter->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
	int attackGifts = 0, defenseGifts = 0, utilityGifts = 0;
	characterAbilities->GetUrathaGiftCollection()->CheckForNumberOfUnlockedGiftsOnStart(PlayerAttributesAndTalents, attackGifts, defenseGifts, utilityGifts);
	SetUIGiftNumberValues(attackGifts, defenseGifts, utilityGifts);
}
