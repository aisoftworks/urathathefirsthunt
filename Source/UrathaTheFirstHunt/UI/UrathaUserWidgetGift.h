// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "../Runtime/UMG/Public/Components/Border.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacterAbilities.h"
#include "Character/UrathaGift.h"
#include "Character/UrathaGiftCollection.h"

//#include "UI/UrathaUserWidgetPlayerMainMenu.h"

#include "UrathaUserWidgetGift.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnGiftHovered, FUrathaGiftInfo, giftInfo, UUrathaTalentSet*, playerAttributesAndTalents);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGiftHoveredResetTheRest, int, giftUniqueID);


USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FGiftUIDataModel : public FBaseUIDataModel
{
	GENERATED_BODY()

private:
	FUrathaGiftInfo GiftInfo;
	FUrathaGiftBindInformation GiftBindInfo;

	UTexture2D* GiftIconBorderless;
	EUrathaGiftType GiftType;
	FString GiftTypeString;

public:
	FGiftUIDataModel();

	FGiftUIDataModel(const UObject* worldObject, TSubclassOf<UUrathaGift> assignedGift, UUrathaGift* giftDefaultObject);

	FUrathaGiftInfo GetGiftInfo()
	{
		return GiftInfo;
	}

	FUrathaGiftBindInformation GetGiftBindInfo()
	{
		return GiftBindInfo;
	}

	UTexture2D* GetGiftIconBorderless()
	{
		return GiftIconBorderless;
	}

	FString GetGiftTypeString()
	{
		return GiftTypeString;
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetGift : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	FGiftUIDataModel GiftUIDataModel;

	UUrathaGift* Gift;
	UUrathaGift* ShownGift;

public:
	FOnGiftHovered OnGiftHovered;

	FOnGiftHoveredResetTheRest OnGiftHoveredResetTheRest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	TSubclassOf<UUrathaGift> GiftToAssign;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UBorder* GiftIconPlaceholder;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UImage* GiftIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UImage* GiftConnectorFirst;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UImage* GiftConnectorSecond;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UBorder* Border_GiftRequirementFirst;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UBorder* Border_GiftRequirementSecond;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftRequirementFirst;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftRequirementSecond;

	void SetupGiftUIView();

	virtual void NativeConstruct() override;

	UFUNCTION()
	void ShowGiftPreview();

	void ResetShownGift()
	{
		ShownGift = nullptr;
	}
};
