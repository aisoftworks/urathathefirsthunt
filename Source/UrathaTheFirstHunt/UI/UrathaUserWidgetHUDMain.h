// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Character/UrathaAbility.h"
#include "Character/UrathaGift.h"
//include health/forms
#include "UI/UrathaUserWidgetHUDGiftsAndDodge.h"
#include "UI/UrathaUserWidgetHUDCombatInfo.h"
#include "UrathaUserWidgetHUDMain.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetHUDMain : public UUrathaUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Main", meta = (BindWidget))
	UUrathaUserWidgetHUDCombatInfo* UI_HUD_AttackAbilitiesAndCombatInfo;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Main", meta = (BindWidget))
	UUrathaUserWidgetHUDGiftsAndDodge* UI_HUD_GiftsAndDodge;

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void SetAttackAbilitiesInUI(FUrathaAbilityInfo attackAbilityInfo, bool activatedsWithHold);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void SetGiftsInUI(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void ChangeCommandIcons(bool isGamepadInput);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void SwitchToActiveCommandIconPress(bool isActive, bool isGamepadInput);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void SwitchToActiveCommandIconHold(bool isActive, bool isGamepadInput);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void StartAttackAbilityCooldown(bool activatesWithHold);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void StartAttackGiftCooldown();

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void StartDefenseGiftCooldown();

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void StartUtilityGiftCooldown();
};
