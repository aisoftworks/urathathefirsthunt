// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "UI/UrathaUserWidgetTalentAbilities.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacter.h"

//#include "UI/UrathaUserWidgetPlayerMainMenu.h"

#include "UrathaUserWidgetTalent.generated.h"


USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FTalentUIDataModel : public FBaseUIDataModel
{
	GENERATED_BODY()

private:
	FString TalentName;

public:
	FTalentUIDataModel();

	FTalentUIDataModel(const UObject* worldObject, FGameplayAttribute assignedTalent);

	FString GetTalentName()
	{
		return TalentName;
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetTalent : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	FTalentUIDataModel TalentUIDataModel;

	void SetUIName();

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Uratha Talent")
	FGameplayAttribute TalentToAssign;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Uratha Talent")
	UTexture2D* TalentPlaceholderImage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Uratha Talent")
	UUrathaUserWidgetTalentAbilities* AssignedTalentAbilities;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Talent", meta = (BindWidget))
	UImage* TalentPlaceholder;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Talent", meta = (BindWidget))
	UTextBlock* TalentName;

	virtual void NativeConstruct() override;

	void InitTalentRoot();
};
