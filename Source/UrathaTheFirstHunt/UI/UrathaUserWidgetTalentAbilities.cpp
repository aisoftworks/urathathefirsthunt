// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetTalentAbilities.h"

void UUrathaUserWidgetTalentAbilities::NativeConstruct()
{
	InitUITalentAbilities();

	Super::NativeConstruct();
}

void UUrathaUserWidgetTalentAbilities::InitUITalentAbilities()
{
	TalentAbilities = TArray<UUrathaUserWidgetTalentAbility*>();

	TalentAbilities.Add(UI_TalentAbility1);
	TalentAbilities.Add(UI_TalentAbility2);
	TalentAbilities.Add(UI_TalentAbility3);
	TalentAbilities.Add(UI_TalentAbility4);
	TalentAbilities.Add(UI_TalentAbility5);
	TalentAbilities.Add(UI_TalentAbility6);
	TalentAbilities.Add(UI_TalentAbility7);
	TalentAbilities.Add(UI_TalentAbility8);
	TalentAbilities.Add(UI_TalentAbility9);
	TalentAbilities.Add(UI_TalentAbility10);

	for (auto abilityIterator = TalentAbilities.CreateIterator(); abilityIterator; ++abilityIterator)
	{
		if (TalentAbilities[abilityIterator.GetIndex()]->GetUIAbility() != nullptr)
		{
			TalentAbilities[abilityIterator.GetIndex()]->OnAbilityHovered.AddDynamic(this, &UUrathaUserWidgetTalentAbilities::RespondToShowAbilityInfo);
			TalentAbilities[abilityIterator.GetIndex()]->OnAbilityHoveredResetTheRest.AddDynamic(this, &UUrathaUserWidgetTalentAbilities::RespondToResetTheRest);
		}
	}
}

void UUrathaUserWidgetTalentAbilities::SetUITalentRankColor(float talentBaseValue)
{
	bool TalentAbilityUnlocked = false;

	for (int talentRankIndex = 0; talentRankIndex < TalentAbilities.Num(); talentRankIndex++)
	{
		if (talentRankIndex < talentBaseValue)
		{
			TalentAbilityUnlocked = true;
			//TalentAbilities[talentRankIndex]->SetTalentRankColor(true);
		}
		else
		{
			TalentAbilityUnlocked = false;
			/*TalentAbilities[talentRankIndex]->SetTalentRankColor(false);*/
		}
		//TalentAbilities[talentRankIndex]->SetTalentRankColor(TalentAbilityUnlocked);

		//Check if unlocked and set locked/unlocked color
		TalentAbilities[talentRankIndex]->SetTalentRankColor(talentRankIndex < talentBaseValue, TalentTree);
	}
}

void UUrathaUserWidgetTalentAbilities::SetUIAbilitiesColor(float talentBaseValue)
{
	for (UUrathaUserWidgetTalentAbility* talentAbility : TalentAbilities)
	{
		if (talentAbility->GetUIAbility() != nullptr)
		{
			talentAbility->SetAbilityColor(talentBaseValue);
		}
	}
}

void UUrathaUserWidgetTalentAbilities::HideUITalentRanks(int numberToHide)
{
	for (int talentRankIndex = TalentAbilities.Num() - 1; talentRankIndex >= numberToHide; talentRankIndex--)
	{
		TalentAbilities[talentRankIndex]->SetVisibility(ESlateVisibility::Hidden);
		TalentAbilities[talentRankIndex]->SetIsEnabled(false);
	}
}

void UUrathaUserWidgetTalentAbilities::RespondToShowAbilityInfo(FUrathaAbilityInfo abilityInfo)
{
	UE_LOG(LogTemp, Warning, TEXT("Trying to update preview."));
	if (TalentAbilityPreview != nullptr)
	{
		TalentAbilityPreview->AbilityIcon->SetBrushFromTexture(abilityInfo.UIIcon);
		TalentAbilityPreview->AbilityName->SetText(FText::FromString(abilityInfo.AbilityName));
		TalentAbilityPreview->AbilityDescription->SetText(FText::FromString(abilityInfo.AbilityDescription));
	}
}

void UUrathaUserWidgetTalentAbilities::RespondToResetTheRest(int talentAbilityUniqueID)
{
	OnAbilityHoveredResetAllOthers.Broadcast(talentAbilityUniqueID);
}

void UUrathaUserWidgetTalentAbilities::ResetOtherAbilitiesInTheTree(int talentAbilityUniqueID)
{
	for (auto abilityIterator = TalentAbilities.CreateIterator(); abilityIterator; ++abilityIterator)
	{
		if (TalentAbilities[abilityIterator.GetIndex()]->GetUIAbility() != nullptr)
		{
			if (TalentAbilities[abilityIterator.GetIndex()]->GetUniqueID() != talentAbilityUniqueID)
			{
				TalentAbilities[abilityIterator.GetIndex()]->ResetShownAbility();
			}
		}
	}
}
