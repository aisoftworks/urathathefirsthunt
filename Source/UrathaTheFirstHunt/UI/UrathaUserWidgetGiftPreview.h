// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "UI/UrathaUserWidgetGift.h"
#include "Character/UrathaGift.h"
#include "Character/UrathaTalentSet.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "UrathaUserWidgetGiftPreview.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetGiftPreview : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	TArray<UUrathaUserWidgetGift*> GiftsToHoverOver;

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UImage* GiftIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftDescription;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* GiftType;



	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftAttunement;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftLightning;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftSavageRending;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftHardSkin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftHonorIncarnate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftMotherLunasBlessing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftBetweenTheWeave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftFatherWolfsSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	UUrathaUserWidgetGift* GiftSecondSight;

	UFUNCTION(BlueprintCallable)
	void InitGiftPreview();

	UFUNCTION()
	void RespondToShowGiftInfo(FUrathaGiftInfo giftInfo, UUrathaTalentSet* playerTalentSet);

	UFUNCTION()
	void RespondToResetTheRest(int giftUniqueID);
};
