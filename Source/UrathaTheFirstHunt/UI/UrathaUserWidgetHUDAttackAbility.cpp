// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDAttackAbility.h"

void UUrathaUserWidgetHUDAttackAbility::NativeConstruct()
{
	

	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDAttackAbility::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	if (InCooldownState)
	{
		if (CooldownTimer <= AbilityInfo.Cooldown)
		{
			CooldownTimer += InDeltaTime;
			IconMaterialInstance->SetScalarParameterValue(FName("CooldownPercentage"), CooldownTimer / AbilityInfo.Cooldown);
		}
		else
		{
			CooldownTimer = 0.f;
			InCooldownState = false;
		}
	}

	Super::NativeTick(MyGeometry, InDeltaTime);
}

void UUrathaUserWidgetHUDAttackAbility::SetAttackAbilityHUDComponent(FUrathaAbilityInfo attackAbilityInfo)
{
	AbilityInfo = attackAbilityInfo;
	IconMaterialInstance = UMaterialInstanceDynamic::Create(AbilityInfo.UIMaterialInstance, this);
	IconMaterialInstance->SetTextureParameterValue(FName("IconTexture"), AbilityInfo.HUDIcon);
	AbilityIcon->SetBrushFromMaterial(IconMaterialInstance);
}

void UUrathaUserWidgetHUDAttackAbility::StartAttackAbilityCooldown()
{
	IconMaterialInstance->SetScalarParameterValue(FName("CooldownPercentage"), 0.f);
	InCooldownState = true;
}
