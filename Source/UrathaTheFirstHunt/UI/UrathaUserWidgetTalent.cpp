// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetTalent.h"
#include "../Runtime/UMG/Public/Components/CanvasPanelSlot.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"

FTalentUIDataModel::FTalentUIDataModel()
{

}

FTalentUIDataModel::FTalentUIDataModel(const UObject* worldObject, FGameplayAttribute assignedTalent) :
	FBaseUIDataModel(worldObject)
{
	TalentName = AddSpacesToUIString(assignedTalent.GetName());
}

void UUrathaUserWidgetTalent::SetUIName()
{
	if (TalentToAssign != nullptr)
	{
		UCanvasPanelSlot* NameCanvasSlot = Cast<UCanvasPanelSlot>(TalentName->Slot);

		if (TalentUIDataModel.GetTalentName().Len() > 11)
		{
			TalentName->SetAutoWrapText(true);
			NameCanvasSlot->SetAlignment(FVector2D(0.51f, 0.48f));
		}
		else
		{
			TalentName->SetAutoWrapText(false);
			NameCanvasSlot->SetAlignment(FVector2D(0.51f, 0.24f));
		}

		TalentName->SetText(FText::FromString(TalentUIDataModel.GetTalentName()));
	}
}

void UUrathaUserWidgetTalent::NativeConstruct()
{
	TalentUIDataModel = FTalentUIDataModel(GetWorld(), TalentToAssign);

	if (TalentPlaceholderImage)
	{
		TalentPlaceholder->SetBrushFromTexture(TalentPlaceholderImage);
	}

	SetUIName();

	InitTalentRoot();
	UE_LOG(LogTemp, Warning, TEXT("Creating Ability: %s"), *this->GetName());
	Super::NativeConstruct();
}

void UUrathaUserWidgetTalent::InitTalentRoot()
{
	if (AssignedTalentAbilities != nullptr)
	{
		if (TalentToAssign.GetName().Contains("Aggravated"))
		{
			AssignedTalentAbilities->HideUITalentRanks(5);
		}

		AssignedTalentAbilities->SetUITalentRankColor(TalentToAssign.GetNumericValueChecked(TalentUIDataModel.GetPlayerAttributesAndTalents()));
		AssignedTalentAbilities->SetUIAbilitiesColor(TalentToAssign.GetNumericValueChecked(TalentUIDataModel.GetPlayerAttributesAndTalents()));
	}
}
