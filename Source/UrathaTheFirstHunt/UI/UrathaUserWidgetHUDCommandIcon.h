// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/SizeBoxSlot.h"
#include "UrathaUserWidgetHUDCommandIcon.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetHUDCommandIcon : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	bool IconActive = false;
	bool GamepadInput = false;
	
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Commands", meta = (BindWidget))
	UImage* CommandIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD Commands")
	UTexture2D* KeyboardIconTexture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD Commands")
	UTexture2D* GamepadIconTexture;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD Commands")
	UTexture2D* KeyboardIconTextureActive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD Commands")
	UTexture2D* GamepadIconTextureActive;

	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void SetIconTextureByInput(bool isGamepad);

	void SetIconTextureActiveByInput(bool isGamepad, bool isActive);

	void SetVerticalAlignmentKeyboard();
	
	void SetVerticalAlignmentGamepad();
};
