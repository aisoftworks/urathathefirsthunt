// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Border.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "Character/UrathaGift.h"
#include "UI/UrathaUserWidgetQAMGiftSelected.h"
#include "UrathaUserWidgetQAMGiftSelectee.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetQAMGiftSelectee : public UUrathaUserWidget
{
	GENERATED_BODY()
	

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee")
	TSubclassOf<UUrathaGift> AssignedGift;

	UUrathaGift* Gift;

	bool SelecteeGiftChosen = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee")
	float SelectionAngleMin;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee")
	float SelectionAngleMax;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee")
	UUrathaUserWidgetQAMGiftSelected* CorrespondingSelectedGiftSlot;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee", meta = (BindWidget))
	UBorder* BorderSelecteeGift;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selectee", meta = (BindWidget))
	UImage* SelecteeGiftIcon;

	void SetSelecteeGiftColor(bool giftChosen);

	void SetSelecteeGiftIcon(UTexture2D* giftIcon);

	virtual void NativeConstruct() override;
};
