// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDMain.h"

void UUrathaUserWidgetHUDMain::NativeConstruct()
{


	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDMain::SetAttackAbilitiesInUI(FUrathaAbilityInfo attackAbilityInfo, bool activatedsWithHold)
{
	UI_HUD_AttackAbilitiesAndCombatInfo->SetAttackAbilitiesInHUD(attackAbilityInfo, activatedsWithHold);
}

void UUrathaUserWidgetHUDMain::SetGiftsInUI(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo)
{
	UI_HUD_GiftsAndDodge->SetGiftsInHUD(attackGiftInfo, defenseGiftInfo, utilityGiftInfo);
}

void UUrathaUserWidgetHUDMain::ChangeCommandIcons(bool isGamepadInput)
{
	UI_HUD_AttackAbilitiesAndCombatInfo->ChangeAttackAbilitiesCommandIcons(isGamepadInput);
	UI_HUD_GiftsAndDodge->ChangeGiftsCommandIcons(isGamepadInput);
}

void UUrathaUserWidgetHUDMain::SwitchToActiveCommandIconPress(bool isActive, bool isGamepadInput)
{
	UI_HUD_AttackAbilitiesAndCombatInfo->SwitchToActiveCommandIcons(false, isActive, isGamepadInput);
}

void UUrathaUserWidgetHUDMain::SwitchToActiveCommandIconHold(bool isActive, bool isGamepadInput)
{
	UI_HUD_AttackAbilitiesAndCombatInfo->SwitchToActiveCommandIcons(true, isActive, isGamepadInput);
}

void UUrathaUserWidgetHUDMain::StartAttackAbilityCooldown(bool activatesWithHold)
{
	UI_HUD_AttackAbilitiesAndCombatInfo->StartAttackAbilityCooldown(activatesWithHold);
}

void UUrathaUserWidgetHUDMain::StartAttackGiftCooldown()
{
	UI_HUD_GiftsAndDodge->StartAttackGiftCooldown();
}

void UUrathaUserWidgetHUDMain::StartDefenseGiftCooldown()
{
	UI_HUD_GiftsAndDodge->StartDefenseGiftCooldown();
}

void UUrathaUserWidgetHUDMain::StartUtilityGiftCooldown()
{
	UI_HUD_GiftsAndDodge->StartUtilityGiftCooldown();
}
