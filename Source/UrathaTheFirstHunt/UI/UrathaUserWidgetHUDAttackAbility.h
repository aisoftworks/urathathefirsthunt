// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Character/UrathaAbility.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "UrathaUserWidgetHUDAttackAbility.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetHUDAttackAbility : public UUrathaUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gift", meta = (BindWidget))
	UImage* AbilityIcon;

private:
	FUrathaAbilityInfo AbilityInfo;
	bool InCooldownState = false;
	float CooldownTimer = 0.f;
	UMaterialInstanceDynamic* IconMaterialInstance;

public:
	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;

	void SetAttackAbilityHUDComponent(FUrathaAbilityInfo attackAbilityInfo);
	void StartAttackAbilityCooldown();
};
