// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "UrathaUserWidgetAbilityPreview.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetAbilityPreview : public UUrathaUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UImage* AbilityIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UTextBlock* AbilityName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UTextBlock* AbilityDescription;
};
