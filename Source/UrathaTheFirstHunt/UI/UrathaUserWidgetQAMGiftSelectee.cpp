// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetQAMGiftSelectee.h"

void UUrathaUserWidgetQAMGiftSelectee::SetSelecteeGiftColor(bool giftChosen)
{
	if (giftChosen)
	{
		BorderSelecteeGift->SetBrushColor(UI_COLOR_GREEN_GIFT);
		//CorrespondingSelectedGiftSlot->SetSelectedGiftInfo(Gift->GetGiftInfo());
	}
	else
	{
		BorderSelecteeGift->SetBrushColor(UI_COLOR_WHITE);
	}

	SelecteeGiftChosen = giftChosen;
}

void UUrathaUserWidgetQAMGiftSelectee::SetSelecteeGiftIcon(UTexture2D* giftIcon)
{
	SelecteeGiftIcon->SetBrushFromTexture(giftIcon);
}

void UUrathaUserWidgetQAMGiftSelectee::NativeConstruct()
{
	if (AssignedGift != nullptr)
	{
		Gift = AssignedGift->GetDefaultObject<UUrathaGift>();
		SetSelecteeGiftIcon(Gift->GetGiftInfo().UIIconBorderless);
	}

	Super::NativeConstruct();
}
