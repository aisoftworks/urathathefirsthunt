// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetQAM.h"
#include "Blueprint/WidgetLayoutLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "UrathaGameInstance.h"

void UUrathaUserWidgetQAM::SelectGift()
{
	FindChosenGiftAndSelectIt();
}

void UUrathaUserWidgetQAM::NativeConstruct()
{
	WorldObject = GetWorld();

	RadialMenuCenter = UWidgetLayoutLibrary::GetViewportWidgetGeometry(WorldObject).GetLocalSize() / 2.f;

	GiftSelectees = TArray<UUrathaUserWidgetQAMGiftSelectee*>();
	GiftSelectees.Add(UI_QuickAccessMenuUnified_HardSkin);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_HonorIncarnate);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_MotherLunasBlessing);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_Attunement);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_Lightning);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_SavageRending);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_BetweenTheWeave);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_FatherWolfsSpeed);
	GiftSelectees.Add(UI_QuickAccessMenuUnified_SecondSight);

	GiftSelected = TArray<UUrathaUserWidgetQAMGiftSelected*>();
	GiftSelected.Add(UI_QuickAccessMenuUnified_SelectedDefense);
	GiftSelected.Add(UI_QuickAccessMenuUnified_SelectedAttack);
	GiftSelected.Add(UI_QuickAccessMenuUnified_SelectedUtility);

	InitSelectedGifts();

	//Chose to select gifts on "Fire" input, since selecting gifts on hover can select gifts in other categories 
	//since radial menu was refactored to a unified one (all gift categories on the same radial menu).
	//Will consider a 3rd solution.
	SelectGiftDelegate.BindUFunction(this, FName("SelectGift"));
	ListenForInputAction(FName("Fire"), EInputEvent::IE_Pressed, true, SelectGiftDelegate);
	
	MinimalPositiveSelectionAngle = GetMinimumPositiveSelectionAngle() + SELECTION_ANGLE_SAFE_ZONE;
	MaximumPositiveSelectionAngle = GetMaximumPositiveSelectionAngle() - SELECTION_ANGLE_SAFE_ZONE;

	Super::NativeConstruct();
}

void UUrathaUserWidgetQAM::NativeTick(const FGeometry& MyGeometry, float DeltaTime)
{
	Super::NativeTick(MyGeometry, DeltaTime);

	CursorPositionOnViewport = UWidgetLayoutLibrary::GetMousePositionOnViewport(WorldObject);

	float CursorSelectorAngle = UKismetMathLibrary::FindLookAtRotation(
		FVector(RadialMenuCenter.X, RadialMenuCenter.Y, 0.f),
		FVector(CursorPositionOnViewport.X, CursorPositionOnViewport.Y, 0.f)).Yaw;
	
	//Determine which part of radial menu is cursor hovering above.
	/*if (UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, 0.f, 17.f) ||
		UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, -180.f, 0.f) ||
		UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, 163.f, 180.f))*/
	if (UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, 0.f, MinimalPositiveSelectionAngle) ||
		UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, -180.f, 0.f) ||
		UKismetMathLibrary::InRange_FloatFloat(CursorSelectorAngle, MaximumPositiveSelectionAngle, 180.f))
	{
		//GIFTS
		//UE_LOG(LogTemp, Warning, TEXT("%f"), CursorSelectorAngle);
		ChooseGift(CursorSelectorAngle);
	}
	else
	{
		//ITEMS/SCSs
		ChooseItemsSCSs(CursorSelectorAngle);
	}
}

void UUrathaUserWidgetQAM::NativeDestruct()
{
	SaveSelectedGifts();

	Super::NativeDestruct();
}

float UUrathaUserWidgetQAM::GetMinimumPositiveSelectionAngle()
{
	float minimumPositiveSelectionAngle = 180.f;

	for (UUrathaUserWidgetQAMGiftSelectee* giftSelecte : GiftSelectees)
	{
		if (giftSelecte->SelectionAngleMax < minimumPositiveSelectionAngle && giftSelecte->SelectionAngleMax > 0.f)
		{
			minimumPositiveSelectionAngle = giftSelecte->SelectionAngleMax;
		}
	}

	return minimumPositiveSelectionAngle;
}

float UUrathaUserWidgetQAM::GetMaximumPositiveSelectionAngle()
{
	float maximumPositiveSelectionAngle = 0.f;

	for (UUrathaUserWidgetQAMGiftSelectee* giftSelecte : GiftSelectees)
	{
		if (giftSelecte->SelectionAngleMax > maximumPositiveSelectionAngle && giftSelecte->SelectionAngleMax < 180.f)
		{
			maximumPositiveSelectionAngle = giftSelecte->SelectionAngleMax;
		}
	}

	return maximumPositiveSelectionAngle;
}

void UUrathaUserWidgetQAM::InitSelectedGifts()
{
	FUrathaGiftInfo AttackGiftInfo;
	FUrathaGiftInfo DefenseGiftInfo;
	FUrathaGiftInfo UtilityGiftInfo;

	UUrathaGameInstance* GameInstance = Cast<UUrathaGameInstance>(GetGameInstance());
	GameInstance->GetSelectedGiftsInfo(AttackGiftInfo, DefenseGiftInfo, UtilityGiftInfo);

	UI_QuickAccessMenuUnified_SelectedAttack->SetSelectedGiftInfo(AttackGiftInfo);
	UI_QuickAccessMenuUnified_SelectedDefense->SetSelectedGiftInfo(DefenseGiftInfo);
	UI_QuickAccessMenuUnified_SelectedUtility->SetSelectedGiftInfo(UtilityGiftInfo);
}

void UUrathaUserWidgetQAM::SaveSelectedGifts()
{
	UUrathaGameInstance* GameInstance = Cast<UUrathaGameInstance>(GetGameInstance());
	GameInstance->SetSelectedGiftsInfo(
		UI_QuickAccessMenuUnified_SelectedAttack->SelectedGiftInfo,
		UI_QuickAccessMenuUnified_SelectedDefense->SelectedGiftInfo,
		UI_QuickAccessMenuUnified_SelectedUtility->SelectedGiftInfo
	);
}

void UUrathaUserWidgetQAM::FindChosenGiftAndSelectIt()
{
	for (UUrathaUserWidgetQAMGiftSelectee* giftSelecte : GiftSelectees)
	{
		if (giftSelecte->SelecteeGiftChosen)
		{
			giftSelecte->CorrespondingSelectedGiftSlot->SetSelectedGiftInfo(giftSelecte->Gift->GetGiftInfo());
			break;
		}
	}
}

void UUrathaUserWidgetQAM::ChooseGift(float selectorAngle)
{
	for (UUrathaUserWidgetQAMGiftSelectee* giftSelectee : GiftSelectees)
	{
		if (FMath::Abs(giftSelectee->SelectionAngleMin) > FMath::Abs(giftSelectee->SelectionAngleMax) &&
			giftSelectee->SelectionAngleMin < 0 && giftSelectee->SelectionAngleMax > 0)
		{
			if (UKismetMathLibrary::InRange_FloatFloat(
				selectorAngle,
				-180.f,
				giftSelectee->SelectionAngleMin) ||
				UKismetMathLibrary::InRange_FloatFloat(
					selectorAngle,
					giftSelectee->SelectionAngleMax,
					180.f))
			{
				//UE_LOG(LogTemp, Error, TEXT("%s"), *giftSelectee->GetName());
				giftSelectee->SetSelecteeGiftColor(true);
				
				SelectedGiftName->SetText(FText::FromString(
					giftSelectee->Gift->GetGiftInfo().Name
				));
			}
			else
			{
				giftSelectee->SetSelecteeGiftColor(false);
			}
		}
		else
		{
			if (UKismetMathLibrary::InRange_FloatFloat(
				selectorAngle,
				giftSelectee->SelectionAngleMin,
				giftSelectee->SelectionAngleMax))
			{
				//UE_LOG(LogTemp, Error, TEXT("%s"), *giftSelectee->GetName());
				giftSelectee->SetSelecteeGiftColor(true);
				
				SelectedGiftName->SetText(FText::FromString(
					giftSelectee->Gift->GetGiftInfo().Name
				));
			}
			else
			{
				giftSelectee->SetSelecteeGiftColor(false);
			}
		}
	}
}

void UUrathaUserWidgetQAM::ChooseItemsSCSs(float selectorAngle)
{

}
