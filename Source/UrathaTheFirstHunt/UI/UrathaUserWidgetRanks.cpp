// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaUserWidgetRanks.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"


bool UUrathaUserWidgetRanks::CanBuyNextRank(int nextRank, int urathaPoints, int rankCost)
{
	return nextRank * rankCost <= urathaPoints;
}

void UUrathaUserWidgetRanks::NativeConstruct()
{
	Super::NativeConstruct();

	//Change any widget attributes before construction.
	if (UGameplayStatics::GetPlayerCharacter(GetWorld(), 0) && AttributeToAssign != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Player found."));
		PlayerCharacter = Cast<AUrathaCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		PlayerAttributesAndTalents = PlayerCharacter->GetAttributeTalentSet();
		
		PlayerAttributeData = PlayerAttributesAndTalents->GetAttribute(*AttributeToAssign.GetName());
		PlayerUrathaPoints = PlayerAttributesAndTalents->GetAttribute(FName("UrathaPoints"));

		BaseValue = PlayerAttributeData->GetBaseValue();
		TempCurrentValue = CurrentValue = (int)AttributeToAssign.GetNumericValue(PlayerAttributesAndTalents);
		
		NEW_RANK_COST = PlayerAttributesAndTalents->GetCost(AttributeToAssign);
		TempUrathaPoints = PlayerUrathaPoints->GetCurrentValue();

		RankName->SetText(FText::FromString(AttributeToAssign.GetName()));

		UE_LOG(LogTemp, Warning, TEXT("Honor base: %f, Honor current: %f"), 
			PlayerAttributeData->GetBaseValue(),
			PlayerAttributeData->GetCurrentValue());

		SetPurchasedRanks(BaseValue);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Assign the attribute."));
		return;
	}
}

void UUrathaUserWidgetRanks::InitRanks()
{
	RanksArray = TArray<UCheckBox*>();

	RanksArray.Add(Rank1);
	RanksArray.Add(Rank2);
	RanksArray.Add(Rank3);
	RanksArray.Add(Rank4);
	RanksArray.Add(Rank5);

	Rank1->OnCheckStateChanged.AddDynamic(this, &UUrathaUserWidgetRanks::OnCheckboxClicked);
	Rank2->OnCheckStateChanged.AddDynamic(this, &UUrathaUserWidgetRanks::OnCheckboxClicked);
	Rank3->OnCheckStateChanged.AddDynamic(this, &UUrathaUserWidgetRanks::OnCheckboxClicked);
	Rank4->OnCheckStateChanged.AddDynamic(this, &UUrathaUserWidgetRanks::OnCheckboxClicked);
	Rank5->OnCheckStateChanged.AddDynamic(this, &UUrathaUserWidgetRanks::OnCheckboxClicked);
}

void UUrathaUserWidgetRanks::SetPurchasedRanks(int purchasedRankValue)
{
	/*
	By default, only purchased ranks (checkboxes) are checked -> pass base (both) values from the ability system on set. The rest are hit invisible.
	If the player has enough UP (next_rank * rank_cost), set the next rank visible. Only next rank is visible, even if the player can buy 2 or more ranks. 
	Once next rank is checked -> pass base (old) and current (new) values on set.
	Once next rank is checked (not yet purchased) and the player has enough UP (next_rank * rank_cost), set the next rank visible -> pass base (old) and current (new) values on set.
	*/

	UE_LOG(LogTemp, Error, TEXT("Ranks count: %d"), RanksArray.Num());
	UE_LOG(LogTemp, Error, TEXT("Set ranks called"))
	if (purchasedRankValue < 0)
	{
		return;
	}
	if (purchasedRankValue == 0)
	{
		for (int rankIndex = 0; rankIndex < RanksArray.Num(); rankIndex++)
		{
			RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Unchecked);

			if (rankIndex == purchasedRankValue)
			{
				RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
			}
			else
			{
				RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
			}
		}
	}
	if (purchasedRankValue > 0 && purchasedRankValue <= RanksArray.Num())
	{
		UE_LOG(LogTemp, Error, TEXT("Going through the ranks..."));
		if (RanksArray.Num() > 0)
		{
			UE_LOG(LogTemp, Error, TEXT("Ranks count: %i"), RanksArray.Num());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Array empty!"));
		}
		
		for (int rankIndex = 0; rankIndex < RanksArray.Num(); rankIndex++)
		{
			if (rankIndex < purchasedRankValue)
			{
				RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Checked);
				RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
			}
			else
			{
				RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Unchecked);
				if (rankIndex == purchasedRankValue)
				{
					RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
				}
				else
				{
					RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
				}
			}
		}
	}
}

void UUrathaUserWidgetRanks::ChooseRanks(bool checked)
{
	/*
	If the player has enough UP(next_rank * rank_cost), set the next rank visible.Only next rank is visible, even if the player can buy 2 or more ranks.
	Once next rank is checked->pass base(old) and current(new) values on set.
	Once next rank is checked(not yet purchased) and the player has enough UP(next_rank * rank_cost), set the next rank visible->pass base(old) and current(new) values on set.
	*/

	if (checked)
	{
		TempCurrentValue += 1;

		if (!CanBuyNextRank(TempCurrentValue, TempUrathaPoints, NEW_RANK_COST))
		{
			UE_LOG(LogTemp, Error, TEXT("NOT ENOUGHT POINTS!"));
			return;
		}
		else
		{
			if (TempCurrentValue > 0 && TempCurrentValue <= RanksArray.Num())
			{
				for (int rankIndex = 0; rankIndex < RanksArray.Num(); rankIndex++)
				{
					if (rankIndex < TempCurrentValue - 1)
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Checked);
						RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
					}
					else if (rankIndex == TempCurrentValue - 1)
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Checked);
						RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
						TempUrathaPoints = TempUrathaPoints - TempCurrentValue * NEW_RANK_COST;
					}
					else
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Unchecked);
						if (CanBuyNextRank(rankIndex + 1, TempUrathaPoints, NEW_RANK_COST))
						{
							RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
						}
						else
						{
							RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
						}
					}
				}
			}
		}
		UE_LOG(LogTemp, Error, TEXT("TempUrathaPoints: %d"), TempUrathaPoints);
	}
	else
	{
		TempUrathaPoints = TempUrathaPoints + TempCurrentValue * NEW_RANK_COST;
		TempCurrentValue -= 1;

		if (TempCurrentValue > 0 && TempCurrentValue < RanksArray.Num())
		{
			for (int rankIndex = 0; rankIndex < RanksArray.Num(); rankIndex++)
			{
				if (rankIndex < BaseValue)
				{
					RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Checked);
					RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
				}
				else
				{
					if (rankIndex < TempCurrentValue)
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Checked);
						RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
					}
					else if (rankIndex == TempCurrentValue)
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Unchecked);
						RanksArray[rankIndex]->SetVisibility(ESlateVisibility::Visible);
					}
					else
					{
						RanksArray[rankIndex]->SetCheckedState(ECheckBoxState::Unchecked);
						RanksArray[rankIndex]->SetVisibility(ESlateVisibility::HitTestInvisible);
					}
				}
			}
		}
		UE_LOG(LogTemp, Error, TEXT("TempUrathaPoints: %d"), TempUrathaPoints);
	}
	
	UpdateAttributeDataModelAndUIValues(TempCurrentValue);
}

void UUrathaUserWidgetRanks::UpdateAttributeDataModelAndUIValues(int newValue)
{
	float NewValue = (float)(newValue);
	AttributeToAssign.SetNumericValueChecked(NewValue, PlayerAttributesAndTalents);
	PlayerAttributesAndTalents->CalculateTalents();
	//Broadcast a delegate to GameMenu widget in order to update static UI content???
	OnAttributeRankChanged.Broadcast(AttributeToAssign);
}

void UUrathaUserWidgetRanks::OnCheckboxClicked(bool isChecked)
{
	ChooseRanks(isChecked);
}
