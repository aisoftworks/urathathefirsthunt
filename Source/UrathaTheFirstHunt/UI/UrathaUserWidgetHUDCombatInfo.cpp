// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDCombatInfo.h"

void UUrathaUserWidgetHUDCombatInfo::NativeConstruct()
{
	AttackAbilitiesCommandIcons = TArray< UUrathaUserWidgetHUDCommandIcon*>();

	AttackAbilitiesCommandIcons.Add(UI_HUD_Component_CommandIcon_AttackPress);
	AttackAbilitiesCommandIcons.Add(UI_HUD_Component_CommandIcon_AttackHold);
	AttackAbilitiesCommandIcons.Add(UI_HUD_Component_CommandIcon_SCS);

	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDCombatInfo::SetAttackAbilitiesInHUD(FUrathaAbilityInfo attackAbilityInfo, bool activatesWithHold)
{
	if (activatesWithHold)
	{
		UI_HUD_Component_AttackAbility_Hold->SetAttackAbilityHUDComponent(attackAbilityInfo);
		Text_AttackAbility_Hold->SetText(FText::FromString(attackAbilityInfo.AbilityName));
	}
	else
	{
		UI_HUD_Component_AttackAbility_Press->SetAttackAbilityHUDComponent(attackAbilityInfo);
		Text_AttackAbility_Press->SetText(FText::FromString(attackAbilityInfo.AbilityName));
	}

	//Set SCS ability here
	//Set SCS ability text here
	UI_HUD_Component_CommandIcon_SCS->SetVerticalAlignmentKeyboard();
}

void UUrathaUserWidgetHUDCombatInfo::StartAttackAbilityCooldown(bool activatesWithHold)
{
	if (activatesWithHold)
	{
		UI_HUD_Component_AttackAbility_Hold->StartAttackAbilityCooldown();
	}
	else
	{
		UI_HUD_Component_AttackAbility_Press->StartAttackAbilityCooldown();
	}
}

void UUrathaUserWidgetHUDCombatInfo::ChangeAttackAbilitiesCommandIcons(bool isGamepadInput)
{
	for (UUrathaUserWidgetHUDCommandIcon* commandIcon : AttackAbilitiesCommandIcons)
	{
		commandIcon->SetIconTextureByInput(isGamepadInput);
	}
}

void UUrathaUserWidgetHUDCombatInfo::SwitchToActiveCommandIcons(bool activatesWithHold, bool isActive, bool isGamepadInput)
{
	if (activatesWithHold)
	{
		UI_HUD_Component_CommandIcon_AttackHold->SetIconTextureActiveByInput(isGamepadInput, isActive);
	}
	else
	{
		UI_HUD_Component_CommandIcon_AttackPress->SetIconTextureActiveByInput(isGamepadInput, isActive);
	}
}
