// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "UrathaUserWidgetTalentAbility.h"
#include "UI/UrathaUserWidgetAbilityPreview.h"
#include "Character/UrathaAbility.h"
#include "UrathaUserWidgetTalentAbilities.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityHoveredResetAllOthers, int, talentAbilityUniqueID);

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetTalentAbilities : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	TArray<UUrathaUserWidgetTalentAbility*> TalentAbilities;

public:
	FOnAbilityHoveredResetAllOthers OnAbilityHoveredResetAllOthers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability UI Info")
	EUrathaAbilityTalentTree TalentTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability UI Info")
	UUrathaUserWidgetAbilityPreview* TalentAbilityPreview;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility1;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility2;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility3;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility4;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility5;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility6;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility7;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility8;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility9;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ability UI Info", meta = (BindWidget))
	UUrathaUserWidgetTalentAbility* UI_TalentAbility10;

	virtual void NativeConstruct() override;

	void InitUITalentAbilities();

	void SetUITalentRankColor(float talentBaseValue);

	void SetUIAbilitiesColor(float talentBaseValue);

	void HideUITalentRanks(int numberToHide);

	UFUNCTION()
	void RespondToShowAbilityInfo(FUrathaAbilityInfo abilityInfo);

	UFUNCTION()
	void RespondToResetTheRest(int talentAbilityUniqueID);

	void ResetOtherAbilitiesInTheTree(int talentAbilityUniqueID);
};
