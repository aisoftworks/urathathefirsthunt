// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "UI/UrathaUserWidgetHUDGift.h"
#include "UI/UrathaUserWidgetHUDCommandIcon.h"
#include "Character/UrathaGift.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "UrathaUserWidgetHUDGiftsAndDodge.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetHUDGiftsAndDodge : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	//List of command icons
	TArray<UUrathaUserWidgetHUDCommandIcon*> GiftsDodgeCommandIcons;
	
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDGift* UI_HUD_Component_GiftDefense;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UImage* Command_GiftDefense;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_GiftDefense;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UTextBlock* Text_GiftDefense;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDGift* UI_HUD_Component_GiftAttack;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UImage* Command_GiftAttack;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_GiftAttack;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UTextBlock* Text_GiftAttack;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDGift* UI_HUD_Component_GiftUtility;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UImage* Command_GiftUtility;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_GiftUtility;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD Gifts & Dodge", meta = (BindWidget))
	UTextBlock* Text_GiftUtility;

	virtual void NativeConstruct() override;

	void SetGiftsInHUD(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo);

	void ChangeGiftsCommandIcons(bool isGamepadInput);

	void StartAttackGiftCooldown();
	void StartDefenseGiftCooldown();
	void StartUtilityGiftCooldown();
};
