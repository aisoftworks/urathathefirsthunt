// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Blueprint/UserWidget.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "UI/UrathaUserWidgetQAMGiftSelectee.h"
#include "UI/UrathaUserWidgetQAMGiftSelected.h"
#include "Character/UrathaGift.h"
#include "UrathaUserWidgetQAM.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInputAction);

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetQAM : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	const float SELECTION_ANGLE_SAFE_ZONE = 2.5f;

	float MinimalPositiveSelectionAngle = 0.f;
	float MaximumPositiveSelectionAngle = 180.f;

	UObject* WorldObject;

	FVector2D RadialMenuCenter;

	FVector2D CursorPositionOnViewport;

	//Nine gifts player is able to choose from (outer circle in UI).
	TArray<UUrathaUserWidgetQAMGiftSelectee*> GiftSelectees;

	//3 out of 9 gifts that are selected (1 for each category - attack, defense, utility).
	TArray<UUrathaUserWidgetQAMGiftSelected*> GiftSelected;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (AllowPrivateAccess = "true"))
	bool RadialMenuActive = false;

	//HACK: Until save/load is implemented, used to determine should selected gift data be read from GameInstance.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (AllowPrivateAccess = "true"))
	bool RadialMenuAlreadySelectedBefore = false;

public:
	//Called by a delegate on bound input (left mouse button). 
	UFUNCTION()
	void SelectGift();

	FOnInputAction SelectGiftDelegate;

#pragma region UPROPERTIES
#pragma region UI elements representing selected gifts
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelected* UI_QuickAccessMenuUnified_SelectedDefense;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelected* UI_QuickAccessMenuUnified_SelectedAttack;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelected* UI_QuickAccessMenuUnified_SelectedUtility;
#pragma endregion

#pragma region UI elements representing gifts available for selection
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_HardSkin;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_HonorIncarnate;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_MotherLunasBlessing;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_Attunement;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_Lightning;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_SavageRending;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_BetweenTheWeave;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_FatherWolfsSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UUrathaUserWidgetQAMGiftSelectee* UI_QuickAccessMenuUnified_SecondSight;
#pragma endregion

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu", meta = (BindWidget))
	UTextBlock* SelectedGiftName;
#pragma endregion

#pragma region UUserWidget overrides
	virtual void NativeConstruct() override;

	virtual void NativeTick(const FGeometry& MyGeometry, float DeltaTime) override;

	virtual void NativeDestruct() override;
#pragma endregion

	float GetMinimumPositiveSelectionAngle();

	float GetMaximumPositiveSelectionAngle();

	void InitSelectedGifts();

	void SaveSelectedGifts();

	void FindChosenGiftAndSelectIt();

	void ChooseGift(float selectorAngle);

	void ChooseItemsSCSs(float selectorAngle);
};
