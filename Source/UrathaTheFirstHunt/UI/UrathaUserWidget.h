// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacterAbilities.h"
#include "Character/UrathaAbility.h"
//#include "Character/UrathaAbilityCollection.h"
#include "Character/UrathaGift.h"
//#include "Character/UrathaGiftCollection.h"
#include "UrathaUserWidget.generated.h"


USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FBaseUIDataModel
{
	GENERATED_BODY()

protected:
	AUrathaCharacter* PlayerCharacter;
	UUrathaTalentSet* PlayerAttributesAndTalents;
	UUrathaCharacterAbilities* PlayerAbilitiesAndGifts;
	/*UUrathaAbilityCollection* PlayerAbilitiesCollection;
	UUrathaGiftCollection* PlayerGiftsCollection;*/

	FString AddSpacesToUIString(FString talentName);

public:
	FBaseUIDataModel();

	FBaseUIDataModel(const UObject* worldObject);

	AUrathaCharacter* GetPlayerCharacter()
	{
		return PlayerCharacter;
	}

	UUrathaTalentSet* GetPlayerAttributesAndTalents()
	{
		return PlayerAttributesAndTalents;
	}

	UUrathaCharacterAbilities* GetPlayerAbilitiesAndGifts()
	{
		return PlayerAbilitiesAndGifts;
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	const FLinearColor UI_COLOR_GREEN_GIFT = FLinearColor(0.022f, 0.238f, 0.084f, 1.f);
	const FLinearColor UI_COLOR_WHITE = FLinearColor(1.f, 1.f, 1.f, 1.f);
	const FLinearColor UI_COLOR_RED = FLinearColor(0.51f, 0.f, 0.f, 1.f);
	const FLinearColor UI_COLOR_YELLOW = FLinearColor(0.54f, 0.51f, 0.f, 1.f);
	const FLinearColor UI_COLOR_BLUE = FLinearColor(0.f, 0.16f, 0.34f, 1.f);
	const FLinearColor UI_COLOR_PURPLE = FLinearColor(0.15f, 0.f, 0.2f, 1.f);
	const FLinearColor UI_COLOR_GREEN_PURCHASED = FLinearColor(0.f, 1.f, 0.231373f, 1.f);

	//FString AddSpacesToUITalentName(FString talentName);
};
