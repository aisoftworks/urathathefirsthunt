// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetRenown.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"

FRenownUIDataModel::FRenownUIDataModel()
{

}

FRenownUIDataModel::FRenownUIDataModel(const UObject* worldObject, FGameplayAttribute assignedRenown) : 
	FBaseUIDataModel(worldObject)
{
	RenownName = assignedRenown.GetName();
	RenownLevel = (int)assignedRenown.GetNumericValueChecked(PlayerAttributesAndTalents);
}

void UUrathaUserWidgetRenown::NativeConstruct()
{
	RenownUIDataModel = FRenownUIDataModel(GetWorld(), RenownToAssign);

	RenownName->SetText(FText::FromString(RenownUIDataModel.GetRenownName()));

	TArray< FStringFormatArg > args;
	args.Add(RenownUIDataModel.GetRenownLevel());

	RenownLevel->SetText(FText::FromString(FString::Format(TEXT("{0}"), args)));

	Super::NativeConstruct();
}
