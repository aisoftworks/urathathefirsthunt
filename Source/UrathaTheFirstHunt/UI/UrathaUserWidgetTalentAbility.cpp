// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetTalentAbility.h"
#include "Character/UrathaAbility.h"

void UUrathaUserWidgetTalentAbility::NativeConstruct()
{
	InitUITalentAbility();
	UE_LOG(LogTemp, Warning, TEXT("Creating Ability: %s"), *this->GetName());
	Super::NativeConstruct();
}

void UUrathaUserWidgetTalentAbility::InitUITalentAbility()
{
	if (AbilityToAssign == nullptr)
	{
		Ability = nullptr;
		AbilityIcon->SetVisibility(ESlateVisibility::Hidden);
	}
	else
	{
		//Regular casting is not working (probably because it's a TSubclassOf), so default object should be
		//constructed (like I'm doing for HUD abilities).
		Ability = AbilityToAssign->GetDefaultObject<UUrathaAbility>();

		if (Ability)
		{
			AbilityIcon->SetBrushFromTexture(Ability->UICharacterMenuIcon);
			AbilityBorderIcon->OnMouseMoveEvent.BindUFunction(
				this, FName("ShowAbilityPreview"));
		}
	}
}

void UUrathaUserWidgetTalentAbility::SetTalentRankColor(bool unlocked, EUrathaAbilityTalentTree talentTree)
{
	if (unlocked)
	{
		if (talentTree == EUrathaAbilityTalentTree::Unarmed ||
			talentTree == EUrathaAbilityTalentTree::SneakAndLarceny ||
			talentTree == EUrathaAbilityTalentTree::Gunslinging)
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_RED);
		}
		else if (talentTree == EUrathaAbilityTalentTree::FrightfulPresence ||
			talentTree == EUrathaAbilityTalentTree::SmoothTalking ||
			talentTree == EUrathaAbilityTalentTree::Deception)
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_YELLOW);
		}
		else if (talentTree == EUrathaAbilityTalentTree::TechSavviness ||
			talentTree == EUrathaAbilityTalentTree::Sense ||
			talentTree == EUrathaAbilityTalentTree::Medicine)
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_BLUE);
		}
		else
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_PURPLE);
		}
		//AbilityBorderIcon->SetBrushColor(UI_COLOR_GREEN_PURCHASED);
	}
	else
	{
		AbilityBorderIcon->SetBrushColor(UI_COLOR_WHITE);
	}
}

void UUrathaUserWidgetTalentAbility::SetAbilityColor(float talentBaseValue)
{
	if ((int)talentBaseValue >= Ability->UnlocksAtTalentLevel)
	{
		if (Ability->TalentTree == EUrathaAbilityTalentTree::Unarmed ||
			Ability->TalentTree == EUrathaAbilityTalentTree::SneakAndLarceny ||
			Ability->TalentTree == EUrathaAbilityTalentTree::Gunslinging)
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_RED);
			//AbilityIcon->SetColorAndOpacity(UI_COLOR_RED);
		}
		else
		{
			AbilityBorderIcon->SetBrushColor(UI_COLOR_GREEN_PURCHASED);
			AbilityIcon->SetColorAndOpacity(UI_COLOR_GREEN_PURCHASED);
		}
	}
	else
	{
		AbilityBorderIcon->SetBrushColor(UI_COLOR_WHITE);
		AbilityIcon->SetColorAndOpacity(UI_COLOR_WHITE);
	}
}

void UUrathaUserWidgetTalentAbility::ShowAbilityPreview()
{
	if (Ability != ShownAbility)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hovering..."));
		ShownAbility = Ability;		
		OnAbilityHovered.Broadcast(Ability->GetAbilityInfo());
		OnAbilityHoveredResetTheRest.Broadcast(this->GetUniqueID());
	}
	else
	{
		return;
	}
}
