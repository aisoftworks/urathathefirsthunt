// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetQAMGiftSelected.h"

void UUrathaUserWidgetQAMGiftSelected::NativeConstruct()
{
	if (GiftType != EUrathaGiftType::None)
	{
		switch (GiftType)
		{
		case EUrathaGiftType::Attack:
		{
			SelectedGiftType->SetText(FText::FromString("Attack"));
		}
			break;
		case EUrathaGiftType::Defense:
		{
			SelectedGiftType->SetText(FText::FromString("Defense"));
		}
			break;
		case EUrathaGiftType::Utility:
		{
			SelectedGiftType->SetText(FText::FromString("Utility"));
		}
			break;
		default:
		{
			SelectedGiftType->SetText(FText::FromString("Unknown"));
		}
			break;
		}
	}
}

void UUrathaUserWidgetQAMGiftSelected::SetSelectedGiftInfo(FUrathaGiftInfo giftInfo)
{
	SelectedGiftInfo = giftInfo;
	SelectedGiftIcon->SetBrushFromTexture(giftInfo.UIIconBorderless);

	if (giftInfo.Name.Contains("Lightning"))
	{
		BorderSelectedGift->SetPadding(FMargin(45.f));
	}
	else
	{
		BorderSelectedGift->SetPadding(FMargin(30.f));
	}
}
