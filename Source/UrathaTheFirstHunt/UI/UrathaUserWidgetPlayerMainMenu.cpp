// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetPlayerMainMenu.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Engine/World.h"

void UUrathaUserWidgetPlayerMainMenu::NativeConstruct()
{
	

	Super::NativeConstruct();
}

//FString FBaseUIDataModel::AddSpacesToUIString(FString talentName)
//{
//	FString TempTalentName = talentName;
//
//	if (TempTalentName.Contains("And", ESearchCase::CaseSensitive))
//	{
//		TempTalentName = TempTalentName.Replace(TEXT("And"), TEXT("&"), ESearchCase::CaseSensitive);
//	}
//
//	TArray<TCHAR> TalentNameArray = TempTalentName.GetCharArray();
//	int UpperCount = -1;
//	int InsertIncrement = -1;
//	for (int charIndex = 0; charIndex < TalentNameArray.Num(); charIndex++)
//	{
//		if (FChar::IsUpper(TalentNameArray[charIndex]) || TalentNameArray[charIndex] == '&')
//		{
//			UpperCount++;
//			if (UpperCount > 0)
//			{
//				InsertIncrement++;
//				TempTalentName.InsertAt(charIndex + InsertIncrement, TEXT(" "));
//			}
//		}
//	}
//
//	return TempTalentName;
//}
//
//FBaseUIDataModel::FBaseUIDataModel()
//{
//
//}
//
//FBaseUIDataModel::FBaseUIDataModel(const UObject* worldObject)
//{
//	if (UGameplayStatics::GetPlayerCharacter(worldObject, 0))
//	{
//		PlayerCharacter = Cast<AUrathaCharacter>(UGameplayStatics::GetPlayerCharacter(worldObject, 0));
//		PlayerAttributesAndTalents = PlayerCharacter->GetAttributeTalentSet();
//		PlayerAbilitiesAndGifts = Cast<UUrathaCharacterAbilities>(PlayerCharacter->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
//		/*PlayerAbilitiesCollection = PlayerAbilitiesAndGifts->GetUrathaAbilityCollection();
//		PlayerGiftsCollection = PlayerAbilitiesAndGifts->GetUrathaGiftCollection();*/
//	}
//}
