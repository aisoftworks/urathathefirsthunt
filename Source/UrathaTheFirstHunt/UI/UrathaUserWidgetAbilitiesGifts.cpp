// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetAbilitiesGifts.h"

void UUrathaUserWidgetAbilitiesGifts::OmitClick()
{
	//Basically does nothing, except to prevent "Fire" command to initiate attacks while UI is active 
	//(Game and UI mode is necessary) for other reasons.
	UE_LOG(LogTemp, Warning, TEXT("Click ommited..."));
	return;
}

void UUrathaUserWidgetAbilitiesGifts::NativeConstruct()
{
	InitAbilitiesGiftsMenu();

	OmitClickDelegate.BindUFunction(this, FName("OmitClick"));
	ListenForInputAction(FName("Fire"), EInputEvent::IE_Pressed, true, OmitClickDelegate);
	
	Super::NativeConstruct();
}

void UUrathaUserWidgetAbilitiesGifts::InitAbilitiesGiftsMenu()
{
	WidgetSwitcher_AbilitiesGifts->SetActiveWidgetIndex(0);
	
	AbilitiesDefaultButtonStyle = Button_Abilities->WidgetStyle;
	GiftsDefaultButtonStyle = Button_Gifts->WidgetStyle;
	Button_Abilities->SetStyle(CreateSelectedStyle(Button_Abilities->WidgetStyle));

	Button_Abilities->OnPressed.AddDynamic(this, &UUrathaUserWidgetAbilitiesGifts::ButtonAbilitiesPressed);
	Button_Gifts->OnPressed.AddDynamic(this, &UUrathaUserWidgetAbilitiesGifts::ButtonGiftsPressed);

	TalentAbilitiesTrees = TArray<UUrathaUserWidgetTalentAbilities*>();
	TalentAbilitiesTrees.Add(UI_TalentAbilities_Unarmed);
	TalentAbilitiesTrees.Add(UI_TalentAbilities_SneakAndLarceny);
	TalentAbilitiesTrees.Add(UI_TalentAbilities_Gunslinging);

	for (auto talentTreeIterator = TalentAbilitiesTrees.CreateIterator(); talentTreeIterator; ++talentTreeIterator)
	{
		TalentAbilitiesTrees[talentTreeIterator.GetIndex()]->OnAbilityHoveredResetAllOthers.AddDynamic(this, &UUrathaUserWidgetAbilitiesGifts::RespondToResetAllOthers);
	}
}

FButtonStyle UUrathaUserWidgetAbilitiesGifts::CreateSelectedStyle(FButtonStyle currentStyle)
{
	FButtonStyle newStyle;

	newStyle.Normal = currentStyle.Pressed;
	newStyle.Hovered = currentStyle.Pressed;
	newStyle.Pressed = currentStyle.Pressed;
	newStyle.Disabled = currentStyle.Disabled;
	newStyle.NormalPadding = currentStyle.NormalPadding;
	newStyle.PressedPadding = currentStyle.PressedPadding;
	newStyle.PressedSlateSound = currentStyle.PressedSlateSound;
	newStyle.HoveredSlateSound = currentStyle.HoveredSlateSound;

	return newStyle;
}

void UUrathaUserWidgetAbilitiesGifts::ButtonAbilitiesPressed()
{
	WidgetSwitcher_AbilitiesGifts->SetActiveWidgetIndex(0);

	Button_Abilities->SetStyle(CreateSelectedStyle(Button_Abilities->WidgetStyle));
	Button_Gifts->SetStyle(GiftsDefaultButtonStyle);
}

void UUrathaUserWidgetAbilitiesGifts::ButtonGiftsPressed()
{
	WidgetSwitcher_AbilitiesGifts->SetActiveWidgetIndex(1);

	Button_Gifts->SetStyle(CreateSelectedStyle(Button_Gifts->WidgetStyle));
	Button_Abilities->SetStyle(AbilitiesDefaultButtonStyle);
}

void UUrathaUserWidgetAbilitiesGifts::RespondToResetAllOthers(int talentAbilityUniqueID)
{
	for (auto talentTreeIterator = TalentAbilitiesTrees.CreateIterator(); talentTreeIterator; ++talentTreeIterator)
	{
		TalentAbilitiesTrees[talentTreeIterator.GetIndex()]->ResetOtherAbilitiesInTheTree(talentAbilityUniqueID);
	}
}
