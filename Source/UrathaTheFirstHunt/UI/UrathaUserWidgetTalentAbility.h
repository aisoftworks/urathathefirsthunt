// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
//#include "UI/UrathaUserWidgetAbilityPreview.h"
#include "../Runtime/UMG/Public/Components/Border.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "Character/UrathaAbility.h"
#include "UrathaUserWidgetTalentAbility.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityHovered, FUrathaAbilityInfo, abilityInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAbilityHoveredResetTheRest, int, talentAbilityUniqueID);

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetTalentAbility : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	UUrathaAbility* Ability;
	UUrathaAbility* ShownAbility;

public:
	FOnAbilityHovered OnAbilityHovered;

	FOnAbilityHoveredResetTheRest OnAbilityHoveredResetTheRest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability UI Info")
	TSubclassOf<UUrathaAbility> AbilityToAssign;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UBorder* AbilityBorderIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UImage* AbilityIcon;

	virtual void NativeConstruct() override;

	void InitUITalentAbility();

	void SetTalentRankColor(bool unlocked, EUrathaAbilityTalentTree talentTree);

	void SetAbilityColor(float talentBaseValue);

	UFUNCTION()
	void ShowAbilityPreview();

	UUrathaAbility* GetUIAbility()
	{
		return Ability;
	}

	void ResetShownAbility()
	{
		ShownAbility = nullptr;
	}
};
