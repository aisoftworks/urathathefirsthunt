// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Border.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "Character/UrathaGift.h"
#include "UrathaUserWidgetQAMGiftSelected.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetQAMGiftSelected : public UUrathaUserWidget
{
	GENERATED_BODY()
	
public:
	FUrathaGiftInfo SelectedGiftInfo;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Uratha Quick Access Menu Gift Selected")
	EUrathaGiftType GiftType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selected", meta = (BindWidget))
	UBorder* BorderSelectedGift;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selected", meta = (BindWidget))
	UImage* SelectedGiftIcon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha Quick Access Menu Gift Selected", meta = (BindWidget))
	UTextBlock* SelectedGiftType;

	virtual void NativeConstruct() override;

	void SetSelectedGiftInfo(FUrathaGiftInfo giftInfo);
};
