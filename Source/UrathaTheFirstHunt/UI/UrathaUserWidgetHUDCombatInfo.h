// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Character/UrathaAbility.h"
#include "UI/UrathaUserWidgetHUDAttackAbility.h"
#include "UI/UrathaUserWidgetHUDCommandIcon.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "../Runtime/UMG/Public/Components/SizeBox.h"
#include "UrathaUserWidgetHUDCombatInfo.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetHUDCombatInfo : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	//List of command icons
	TArray<UUrathaUserWidgetHUDCommandIcon*> AttackAbilitiesCommandIcons;
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UUrathaUserWidgetHUDAttackAbility* UI_HUD_Component_AttackAbility_Press;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_AttackPress;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UTextBlock* Text_AttackAbility_Press;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UUrathaUserWidgetHUDAttackAbility* UI_HUD_Component_AttackAbility_Hold;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_AttackHold;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UTextBlock* Text_AttackAbility_Hold;

	//Add SCS Ability here

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UUrathaUserWidgetHUDCommandIcon* UI_HUD_Component_CommandIcon_SCS;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "HUD AttackAbilities & SCS", meta = (BindWidget))
	UTextBlock* Text_SCSAbility;


	virtual void NativeConstruct() override;

	void SetAttackAbilitiesInHUD(FUrathaAbilityInfo attackAbilityInfo, bool activatesWithHold);

	void StartAttackAbilityCooldown(bool activatesWithHold);

	void ChangeAttackAbilitiesCommandIcons(bool isGamepadInput);

	void SwitchToActiveCommandIcons(bool activatesWithHold, bool isActive, bool isGamepadInput);
};
