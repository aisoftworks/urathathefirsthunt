// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaTalentSet.h"

//#include "UI/UrathaUserWidgetPlayerMainMenu.h"

#include "UrathaUserWidgetRenown.generated.h"

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FRenownUIDataModel : public FBaseUIDataModel
{
	GENERATED_BODY()

private:
	FString RenownName;
	int RenownLevel;

public:
	FRenownUIDataModel();

	FRenownUIDataModel(const UObject* worldObject, FGameplayAttribute assignedRenown);

	FString GetRenownName()
	{
		return RenownName;
	}

	int GetRenownLevel()
	{
		return RenownLevel;
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetRenown : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	FRenownUIDataModel RenownUIDataModel;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gift UI Info")
	FGameplayAttribute RenownToAssign;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* RenownName;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Gift UI Info", meta = (BindWidget))
	UTextBlock* RenownLevel;

	virtual void NativeConstruct() override;
};
