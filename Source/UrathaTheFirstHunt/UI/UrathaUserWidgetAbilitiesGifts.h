// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Blueprint/UserWidget.h"
#include "../Runtime/UMG/Public/Components/WidgetSwitcher.h"
#include "../Runtime/UMG/Public/Components/Button.h"
#include "../Runtime/SlateCore/Public/Styling/SlateTypes.h"
#include "UI/UrathaUserWidgetTalentAbilities.h"
#include "UrathaUserWidgetAbilitiesGifts.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetAbilitiesGifts : public UUrathaUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION()
	void OmitClick();

	FOnInputAction OmitClickDelegate;

	virtual void NativeConstruct() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UWidgetSwitcher* WidgetSwitcher_AbilitiesGifts;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UButton* Button_Abilities;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UButton* Button_Gifts;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UUrathaUserWidgetTalentAbilities* UI_TalentAbilities_Unarmed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UUrathaUserWidgetTalentAbilities* UI_TalentAbilities_SneakAndLarceny;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Uratha", meta = (BindWidget))
	UUrathaUserWidgetTalentAbilities* UI_TalentAbilities_Gunslinging;

private:
	TArray<UUrathaUserWidgetTalentAbilities*> TalentAbilitiesTrees;

	FButtonStyle AbilitiesDefaultButtonStyle;
	FButtonStyle GiftsDefaultButtonStyle;

	void InitAbilitiesGiftsMenu();

	FButtonStyle CreateSelectedStyle(FButtonStyle currentStyle);

	UFUNCTION()
	void ButtonAbilitiesPressed();

	UFUNCTION()
	void ButtonGiftsPressed();

	UFUNCTION()
	void RespondToResetAllOthers(int talentAbilityUniqueID);
};
