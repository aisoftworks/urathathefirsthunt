// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetGift.h"
#include "../Runtime/Engine/Classes/Kismet/GameplayStatics.h"

FGiftUIDataModel::FGiftUIDataModel() : FBaseUIDataModel()
{

}

FGiftUIDataModel::FGiftUIDataModel(const UObject* worldObject, TSubclassOf<UUrathaGift> assignedGift, UUrathaGift* giftDefaultObject) :
	FBaseUIDataModel(worldObject)
{
	GiftInfo = giftDefaultObject->GetGiftInfo();
	GiftBindInfo = PlayerAbilitiesAndGifts->GetUrathaGiftCollection()->GetGiftBindInformation(assignedGift);

	GiftIconBorderless = GiftInfo.UIIconBorderless;
	GiftType = GiftInfo.GiftType;

	switch (GiftType)
	{
	case EUrathaGiftType::Attack:
	{
		GiftTypeString = FString("Attack");
	}
		break;
	case EUrathaGiftType::Defense:
	{
		GiftTypeString = FString("Defense");
	}
		break;
	case EUrathaGiftType::Utility:
	{
		GiftTypeString = FString("Utility");
	}
		break;
	default:
	{
		GiftTypeString = FString("Unknown");
	}
		break;
	}
}

void UUrathaUserWidgetGift::SetupGiftUIView()
{
	FUrathaGiftBindInformation giftBindInfo = GiftUIDataModel.GetGiftBindInfo();

	if (giftBindInfo.GiftClass != NULL)
	{
		//Gift visual
		GiftIcon->SetBrushFromTexture(GiftUIDataModel.GetGiftIconBorderless());
		GiftType->SetText(FText::FromString(GiftUIDataModel.GetGiftTypeString()));

		//Is unlocked
		if (giftBindInfo.Unlocked)
		{
			GiftIconPlaceholder->SetBrushColor(UI_COLOR_GREEN_GIFT);
		}
		else
		{
			GiftIconPlaceholder->SetBrushColor(UI_COLOR_WHITE);
		}

		//Connectors
		if (giftBindInfo.GiftRequirements.Num() == 2)
		{
			GiftRequirementFirst->SetText(FText::FromString(FString::FromInt(giftBindInfo.GiftRequirements[0].RenownLevel)));
			GiftRequirementSecond->SetText(FText::FromString(FString::FromInt(giftBindInfo.GiftRequirements[1].RenownLevel)));

			if (giftBindInfo.GiftRequirements[0].RenownLevel <=
				(int)giftBindInfo.GiftRequirements[0].Renown.
				GetNumericValueChecked(GiftUIDataModel.GetPlayerAttributesAndTalents()))
			{
				GiftConnectorFirst->SetColorAndOpacity(UI_COLOR_GREEN_GIFT);
				Border_GiftRequirementFirst->SetBrushColor(UI_COLOR_GREEN_GIFT);
			}
			else
			{
				GiftConnectorFirst->SetColorAndOpacity(UI_COLOR_WHITE);
				Border_GiftRequirementFirst->SetBrushColor(UI_COLOR_WHITE);
			}

			if (giftBindInfo.GiftRequirements[1].RenownLevel <=
				(int)giftBindInfo.GiftRequirements[1].Renown.
				GetNumericValueChecked(GiftUIDataModel.GetPlayerAttributesAndTalents()))
			{
				GiftConnectorSecond->SetColorAndOpacity(UI_COLOR_GREEN_GIFT);
				Border_GiftRequirementSecond->SetBrushColor(UI_COLOR_GREEN_GIFT);
			}
			else
			{
				GiftConnectorSecond->SetColorAndOpacity(UI_COLOR_WHITE);
				Border_GiftRequirementSecond->SetBrushColor(UI_COLOR_WHITE);
			}
		}
		else if (giftBindInfo.GiftRequirements.Num() == 1)
		{
			GiftRequirementFirst->SetText(FText::FromString(FString::FromInt(giftBindInfo.GiftRequirements[0].RenownLevel)));

			GiftConnectorSecond->SetVisibility(ESlateVisibility::Hidden);
			Border_GiftRequirementSecond->SetVisibility(ESlateVisibility::Hidden);

			if (giftBindInfo.GiftRequirements[0].RenownLevel <=
				(int)giftBindInfo.GiftRequirements[0].Renown.
				GetNumericValueChecked(GiftUIDataModel.GetPlayerAttributesAndTalents()))
			{
				GiftConnectorFirst->SetColorAndOpacity(UI_COLOR_GREEN_GIFT);
				Border_GiftRequirementFirst->SetBrushColor(UI_COLOR_GREEN_GIFT);
			}
			else
			{
				GiftConnectorFirst->SetColorAndOpacity(UI_COLOR_WHITE);
				Border_GiftRequirementFirst->SetBrushColor(UI_COLOR_WHITE);
			}
		}
	}
}

void UUrathaUserWidgetGift::NativeConstruct()
{
	if (GiftToAssign == nullptr)
	{
		Gift = nullptr;
	}
	else
	{
		//Regular casting is not working (probably because it's a TSubclassOf), so default object should be
		//constructed (like I'm doing for HUD abilities).
		Gift = GiftToAssign->GetDefaultObject<UUrathaGift>();

		if (Gift)
		{
			GiftUIDataModel = FGiftUIDataModel(GetWorld(), GiftToAssign, Gift);

			SetupGiftUIView();

			GiftIconPlaceholder->OnMouseMoveEvent.BindUFunction(
				this, FName("ShowGiftPreview"));
		}
	}

	Super::NativeConstruct();
}

void UUrathaUserWidgetGift::ShowGiftPreview()
{
	if (Gift != ShownGift)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hovering..."));
		OnGiftHovered.Broadcast(Gift->GetGiftInfo(), GiftUIDataModel.GetPlayerAttributesAndTalents());
		ShownGift = Gift;
		OnGiftHoveredResetTheRest.Broadcast(this->GetUniqueID());
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("Already showing this gift. My ID: %d"), this->GetUniqueID());
		return;
	}
}
