// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/Image.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "../Runtime/UMG/Public/Components/InvalidationBox.h"
#include "AttributeSet.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaGift.h"
#include "UrathaWidgetCharacterTalentGift.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaWidgetCharacterTalentGift : public UUrathaUserWidget
{
	GENERATED_BODY()

private:
	AUrathaCharacter* PlayerCharacter;
	UUrathaTalentSet* PlayerAttributesAndTalents;

	FString AddSpacesToTalentName(FString talentName);

	void SetUIName();

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TalentGift")
	FGameplayAttribute TalentToAssign;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TalentGift")
	UTexture2D* TalentPlaceholderImage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TalentGift")
	EUrathaGiftType GiftType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TalentGift", meta = (BindWidget))
	UImage* ImagePlaceholder;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TalentGift", meta = (BindWidget))
	UTextBlock* Level;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TalentGift", meta = (BindWidget))
	UTextBlock* Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "TalentGift", meta = (BindWidget))
	UInvalidationBox* InvalidationBoxName;

	AUrathaCharacter* GetPlayerCharacter()
	{
		return PlayerCharacter;
	}

	virtual void NativePreConstruct() override;

	virtual void NativeConstruct() override;

	void SetUITalentBonusPenalty();

	void SetUITalentLevelValue(float levelValue);

	void UpdateUITalentLevelValue();

	void SetUIGiftNumberValues(int attackGiftsNumber, int defenseGiftsNumber, int utilityGiftsNumber);

	void UpdateUIGiftNumberValues(FGameplayAttribute changedRenown);

	void InitUIGiftNumberValues();
};
