// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "../Runtime/UMG/Public/Components/CheckBox.h"
#include "../Runtime/UMG/Public/Components/TextBlock.h"
#include "AttributeSet.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacter.h"
#include "UrathaUserWidgetRanks.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRankChanged);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnRankChangedParam, ETalent, affectedTalent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAttributeRankChanged, FGameplayAttribute, changedAttribute);

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetRanks : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	TArray<UCheckBox*> RanksArray;

	int BaseValue;
	int CurrentValue;
	int TempCurrentValue;
	int NEW_RANK_COST;
	int TempUrathaPoints;

	AUrathaCharacter* PlayerCharacter;
	UUrathaTalentSet* PlayerAttributesAndTalents;

	FGameplayAttributeData* PlayerAttributeData;
	FGameplayAttributeData* PlayerUrathaPoints;

	bool CanBuyNextRank(int nextRank, int urathaPoints, int rankCost);

public:
	UPROPERTY(BlueprintAssignable, Category = "Ranks")
	FOnRankChanged OnRankChanged;

	UPROPERTY(BlueprintAssignable, Category = "Ranks")
	FOnRankChangedParam OnRankChangedWithParam;

	UPROPERTY(BlueprintAssignable, Category = "Ranks")
	FOnAttributeRankChanged OnAttributeRankChanged;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Ranks")
	FGameplayAttribute AttributeToAssign;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UCheckBox* Rank1;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UCheckBox* Rank2;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget, RankNumber = "3"))
	UCheckBox* Rank3;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UCheckBox* Rank4;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UCheckBox* Rank5;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Ranks", meta = (BindWidget))
	UTextBlock* RankName;

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	void InitRanks();

	void SetPurchasedRanks(int purchasedRankValue);

	void ChooseRanks(bool checked);

	void UpdateAttributeDataModelAndUIValues(int newValue);

	UFUNCTION()
	void OnCheckboxClicked(bool isChecked);
};
