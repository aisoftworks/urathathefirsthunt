// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetHUDCommandIcon.h"

void UUrathaUserWidgetHUDCommandIcon::NativeConstruct()
{
	SetIconTextureByInput(false);

	Super::NativeConstruct();
}

void UUrathaUserWidgetHUDCommandIcon::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	if (IconActive)
	{
		if (GamepadInput)
		{
			CommandIcon->SetBrushFromTexture(GamepadIconTextureActive);
		}
		else
		{
			CommandIcon->SetBrushFromTexture(KeyboardIconTextureActive);
		}
	}
}

void UUrathaUserWidgetHUDCommandIcon::SetIconTextureByInput(bool isGamepad)
{
	if (isGamepad)
	{
		CommandIcon->SetBrushFromTexture(GamepadIconTexture);
		//SetVerticalAlignmentGamepad();
	}
	else
	{
		CommandIcon->SetBrushFromTexture(KeyboardIconTexture);
		//SetVerticalAlignmentKeyboard();
	}
	
	//Possibly accommodate size of SCS icon, if same class is used.
}

void UUrathaUserWidgetHUDCommandIcon::SetIconTextureActiveByInput(bool isGamepad, bool isActive)
{
	//TODO: Write this in a nicer way.
	if (GamepadIconTextureActive != nullptr && KeyboardIconTextureActive != nullptr)
	{
		/*if (isActive)
		{
			if (isGamepad)
			{
				CommandIcon->SetBrushFromTexture(GamepadIconTextureActive);
			}
			else
			{
				CommandIcon->SetBrushFromTexture(KeyboardIconTextureActive);
			}
		}
		else
		{
			if (isGamepad)
			{
				CommandIcon->SetBrushFromTexture(GamepadIconTexture);
			}
			else
			{
				CommandIcon->SetBrushFromTexture(KeyboardIconTexture);
			}
		}*/

		//Set non-active icons by default
		if (isGamepad)
		{
			CommandIcon->SetBrushFromTexture(GamepadIconTexture);
		}
		else
		{
			CommandIcon->SetBrushFromTexture(KeyboardIconTexture);
		}
		//This will potentially trigger code in Tick method.
		IconActive = isActive;
		GamepadInput = isGamepad;
	}
}

void UUrathaUserWidgetHUDCommandIcon::SetVerticalAlignmentKeyboard()
{
	if (USizeBoxSlot* SizeBoxSlot = Cast<USizeBoxSlot>(this->Slot))
	{
		SizeBoxSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Center);
	}
}

void UUrathaUserWidgetHUDCommandIcon::SetVerticalAlignmentGamepad()
{
	if (USizeBoxSlot* SizeBoxSlot = Cast<USizeBoxSlot>(this->Slot))
	{
		SizeBoxSlot->SetVerticalAlignment(EVerticalAlignment::VAlign_Fill);
	}
}
