// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UI/UrathaUserWidget.h"
#include "Character/UrathaCharacter.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaCharacterAbilities.h"
#include "Character/UrathaAbility.h"
#include "Character/UrathaAbilityCollection.h"
#include "Character/UrathaGift.h"
#include "Character/UrathaGiftCollection.h"
#include "UrathaUserWidgetPlayerMainMenu.generated.h"

//USTRUCT(BlueprintType)
//struct URATHATHEFIRSTHUNT_API FBaseUIDataModel
//{
//	GENERATED_BODY()
//
//protected:
//	AUrathaCharacter* PlayerCharacter;
//	UUrathaTalentSet* PlayerAttributesAndTalents;
//	UUrathaCharacterAbilities* PlayerAbilitiesAndGifts;
//	/*UUrathaAbilityCollection* PlayerAbilitiesCollection;
//	UUrathaGiftCollection* PlayerGiftsCollection;*/
//
//	FString AddSpacesToUIString(FString talentName);
//
//public:
//	FBaseUIDataModel();
//
//	FBaseUIDataModel(const UObject* worldObject);
//
//	AUrathaCharacter* GetPlayerCharacter()
//	{
//		return PlayerCharacter;
//	}
//
//	UUrathaTalentSet* GetPlayerAttributesAndTalents()
//	{
//		return PlayerAttributesAndTalents;
//	}
//
//	UUrathaCharacterAbilities* GetPlayerAbilitiesAndGifts()
//	{
//		return PlayerAbilitiesAndGifts;
//	}
//};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetPlayerMainMenu : public UUrathaUserWidget
{
	GENERATED_BODY()
	
protected:
	/*AUrathaCharacter* PlayerCharacter;
	UUrathaTalentSet* PlayerAttributesAndTalents;
	UUrathaCharacterAbilities* PlayerAbilitiesAndGifts;
	UUrathaAbilityCollection* PlayerAbilitiesCollection;
	UUrathaGiftCollection* PlayerGiftsCollection;*/

public:
	virtual void NativeConstruct() override;
};
