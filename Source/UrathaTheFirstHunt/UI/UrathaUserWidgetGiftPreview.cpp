// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaUserWidgetGiftPreview.h"

void UUrathaUserWidgetGiftPreview::InitGiftPreview()
{
	GiftsToHoverOver = TArray<UUrathaUserWidgetGift*>();

	GiftsToHoverOver.Add(GiftAttunement);
	GiftsToHoverOver.Add(GiftLightning);
	GiftsToHoverOver.Add(GiftSavageRending);
	GiftsToHoverOver.Add(GiftHardSkin);
	GiftsToHoverOver.Add(GiftHonorIncarnate);
	GiftsToHoverOver.Add(GiftMotherLunasBlessing);
	GiftsToHoverOver.Add(GiftBetweenTheWeave);
	GiftsToHoverOver.Add(GiftFatherWolfsSpeed);
	GiftsToHoverOver.Add(GiftSecondSight);

	for (auto giftIterator = GiftsToHoverOver.CreateIterator(); giftIterator; ++giftIterator)
	{
		GiftsToHoverOver[giftIterator.GetIndex()]->OnGiftHovered.AddDynamic(this, &UUrathaUserWidgetGiftPreview::RespondToShowGiftInfo);
		GiftsToHoverOver[giftIterator.GetIndex()]->OnGiftHoveredResetTheRest.AddDynamic(this, &UUrathaUserWidgetGiftPreview::RespondToResetTheRest);
		//UE_LOG(LogTemp, Warning, TEXT("GiftID: %d"), GiftsToHoverOver[giftIterator.GetIndex()]->GetUniqueID());
	}
}

void UUrathaUserWidgetGiftPreview::RespondToShowGiftInfo(FUrathaGiftInfo giftInfo, UUrathaTalentSet* playerTalentSet)
{
	GiftIcon->SetBrushFromTexture(giftInfo.UIIconBorderless);
	
	GiftName->SetText(FText::FromString(giftInfo.Name));

	FString UpdatedDescription;

	if (giftInfo.LevelsUpWith != NULL)
	{
		FString ValueToReplace = FString("$1");
		FString ValueToInsert;
		int GiftValueInt = 1;

		float GiftValue =
			giftInfo.GiftEffectModifier *
			giftInfo.LevelsUpWith.GetGameplayAttributeDataChecked(playerTalentSet)->GetBaseValue();

		UE_LOG(LogTemp, Warning, TEXT("Float value: %f"), GiftValue);
		
		if (FMath::Fmod(GiftValue, 2) > 0)
		{
			GiftValueInt = (int)GiftValue + 1;
			ValueToInsert = FString::FromInt(GiftValueInt);
			UE_LOG(LogTemp, Warning, TEXT("Float value: %d"), GiftValueInt);
		}
		else
		{
			ValueToInsert = FString::FromInt((int)GiftValue);
		}

		UpdatedDescription = giftInfo.Description.Replace(*ValueToReplace, *ValueToInsert);
	}
	else
	{
		UpdatedDescription = giftInfo.Description;
	}
	GiftDescription->SetText(FText::FromString(UpdatedDescription));

	switch (giftInfo.GiftType)
	{
	case EUrathaGiftType::Attack:
	{
		GiftType->SetText(FText::FromString("Attack"));
	}
		break;
	case EUrathaGiftType::Defense:
	{
		GiftType->SetText(FText::FromString("Defense"));
	}
		break;
	case EUrathaGiftType::Utility:
	{
		GiftType->SetText(FText::FromString("Utility"));
	}
		break;
	default:
		break;
	}
}

void UUrathaUserWidgetGiftPreview::RespondToResetTheRest(int giftUniqueID)
{
	for (auto giftIterator = GiftsToHoverOver.CreateIterator(); giftIterator; ++giftIterator)
	{
		if (GiftsToHoverOver[giftIterator.GetIndex()]->GetUniqueID() != giftUniqueID)
		{
			GiftsToHoverOver[giftIterator.GetIndex()]->ResetShownGift();
		}
	}
}
