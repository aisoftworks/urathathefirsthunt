// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include <Array.h>
#include "Engine.h"
#include "UnrealClient.h"
#include "../Runtime/UMG/Public/Components/InvalidationBox.h"
#include "UI/UrathaUserWidget.h"
#include "UI/UrathaUserWidgetRanks.h"
#include "UI/UrathaWidgetCharacterTalentGift.h"
#include "AttributeSet.h"
#include "Character/UrathaTalentSet.h"
#include "Character/UrathaGift.h"
#include "UrathaUserWidgetPlayerMenu.generated.h"

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FUITalentArray
{
	GENERATED_BODY()

	TArray<UUrathaWidgetCharacterTalentGift*> UITalents;

public:
	FUITalentArray()
	{
		UITalents = TArray<UUrathaWidgetCharacterTalentGift*>();
	}

	FUITalentArray(TArray<UUrathaWidgetCharacterTalentGift*> &uiTalents)
	{
		UITalents = uiTalents;
	}

	void UpdateUITalentLevelValue()
	{
		if (UITalents.Num() > 0)
		{
			for (UUrathaWidgetCharacterTalentGift* uiTalent : UITalents)
			{
				uiTalent->UpdateUITalentLevelValue();
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Empty array of UUrathaWidgetCharacterTalentGift"));
		}
	}
};

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FUIGift
{
	GENERATED_BODY()

	UUrathaWidgetCharacterTalentGift* UIGift;

	EUrathaGiftType UIGiftType;

public:
	FUIGift()
	{
		UIGift = nullptr;
		UIGiftType = EUrathaGiftType::Attack;
	}

	FUIGift(UUrathaWidgetCharacterTalentGift* uiGift, EUrathaGiftType giftType)
	{
		UIGift = uiGift;
		UIGiftType = giftType;
	}

	void UpdateUIGiftNumberValue(FGameplayAttribute changedRenown)
	{
		UIGift->UpdateUIGiftNumberValues(changedRenown);
	}
};

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FUIGiftArray
{
	GENERATED_BODY()

	TArray<FUIGift> UIGifts;

public:
	FUIGiftArray()
	{
		UIGifts = TArray<FUIGift>();
	}

	FUIGiftArray(TArray<FUIGift> uiGifts)
	{
		UIGifts = uiGifts;
	}

	void UpdateUIGiftNumberValues(FGameplayAttribute changedRenown)
	{
		for (FUIGift uiGift : UIGifts)
		{
			uiGift.UpdateUIGiftNumberValue(changedRenown);
		}
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaUserWidgetPlayerMenu : public UUrathaUserWidget
{
	GENERATED_BODY()
	
private:
	void BindRanksDelegates();

public:
	TMap<FGameplayAttribute, FUITalentArray> UIAttributeTalentPairs;

	TMap<FGameplayAttribute, FUIGiftArray> UIRenownGiftPairs;

	

#pragma region MENTAL
	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Intelligence;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Wits;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Resolve;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Science;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Investigation;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Technology;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_TechSavviness;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Sense;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Medicine;
#pragma endregion

#pragma region PHYSICAL
	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Strength;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Agility;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Physique;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Firearms;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Stealth;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Brawl;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Bashing;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Lethal;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Aggravated;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Unarmed;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_SneakAndLarceny;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Gunslinging;
#pragma endregion

#pragma region SOCIAL
	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Presence;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Manipulation;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Composure;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Subterfuge;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Persuasion;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Intimidation;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_FrightfulPresence;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_SmoothTalking;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_TalentGiftCharacter_Deception;
#pragma endregion

#pragma region SPIRIT
	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Honor;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Glory;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Cunning;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Wisdom;

	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UUrathaUserWidgetRanks* UI_RanksCharacter_Purity;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_GiftNumberCharacter_Attack;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_GiftNumberCharacter_Defense;

	UPROPERTY(VisibleAnywhere, Category = "UrathaTalentsGifts", meta = (BindWidget))
	UUrathaWidgetCharacterTalentGift* UI_GiftNumberCharacter_Utility;
#pragma endregion

#pragma region INVALIDATION BOXES
	UPROPERTY(VisibleAnywhere, Category = "UrathaRanks", meta = (BindWidget))
	UInvalidationBox* InvalidationBoxLinesMental;
#pragma endregion

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable)
	void InitPlayerMenu();

	UFUNCTION(BlueprintCallable)
	void PrepareGiftSetForSelection();

	UFUNCTION()
	void UIAttributeValueChanged(FGameplayAttribute changedAttribute);

	void FindAndUpdateUITalentValue(FGameplayAttribute changedAttribute);

	UFUNCTION()
	void UIRenownValueChanged(FGameplayAttribute changedRenown);

	void FindAndUpdateUIGiftNumberValue(FGameplayAttribute changedRenown);

	void ViewportResized(FViewport* viewport, uint32 someInt);
};
