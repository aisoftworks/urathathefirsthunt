// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaGameInstance.h"
#include "GameServices/AbilitiesGameService.h"


void UUrathaGameInstance::Init()
{
	Super::Init();

	if (DefaultUIGiftAttack != nullptr && DefaultUIGiftDefense != nullptr && DefaultUIGiftUtility != nullptr)
	{
		SetSelectedGiftsInfo(
			DefaultUIGiftAttack->GetDefaultObject<UUrathaGift>()->GetGiftInfo(),
			DefaultUIGiftDefense->GetDefaultObject<UUrathaGift>()->GetGiftInfo(),
			DefaultUIGiftUtility->GetDefaultObject<UUrathaGift>()->GetGiftInfo()
		);
	}

	
	GameServices.Add(FString(typeid(AbilitiesGameService).name()), new AbilitiesGameService(AbilitiesDataTable));
}

UUrathaGameInstance::UUrathaGameInstance()
{
	GameServices = TMap<FString, BaseGameService*>();
	UE_LOG(LogTemp, Error, TEXT("GameServiceInInstance - CONSCTRUCTOR"));
}

UUrathaGameInstance::~UUrathaGameInstance()
{
	for (auto gameService : GameServices)
	{
		delete gameService.Value;
	}
	GameServices.Empty();
	UE_LOG(LogTemp, Error, TEXT("GameServiceInInstance - DESCTRUCTOR"));
}

void UUrathaGameInstance::SetSelectedGiftsInfo(FUrathaGiftInfo attackGiftInfo, FUrathaGiftInfo defenseGiftInfo, FUrathaGiftInfo utilityGiftInfo)
{
	SelectedAttackGiftInfo = attackGiftInfo;
	SelectedDefenseGiftInfo = defenseGiftInfo;
	SelectedUtilityGiftInfo = utilityGiftInfo;
}

void UUrathaGameInstance::GetSelectedGiftsInfo(FUrathaGiftInfo &attackGiftInfo, FUrathaGiftInfo &defenseGiftInfo, FUrathaGiftInfo &utilityGiftInfo)
{
	attackGiftInfo = SelectedAttackGiftInfo;
	defenseGiftInfo = SelectedDefenseGiftInfo;
	utilityGiftInfo = SelectedUtilityGiftInfo;
}
