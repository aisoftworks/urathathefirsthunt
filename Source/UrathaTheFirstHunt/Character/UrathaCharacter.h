// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../AbilitySystemInterface.h"
#include "../GameplayTags/Classes/GameplayTagContainer.h"
#include "UrathaCharacter.generated.h"

UENUM(BlueprintType)
enum class EHitDirection : uint8
{
	HIT_FRONT UMETA(DisplayName = "hitFront"),
	HIT_BACK UMETA(DisplayName = "hitBack"),
	HIT_LEFT UMETA(DisplayName = "hitLeft"),
	HIT_RIGHT UMETA(DisplayName = "hitRight")
};

UCLASS(config=Game)
class AUrathaCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/** Uratha ability system component*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UAbilitySystemComponent* AbilitySystemComponent;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UUrathaAttributeSet* AttributeSetComponent;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UUrathaTalentSet* AttributeTalentSetComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UUrathaPatrolComponent* PatrolComponent;

	class AUrathaWeapon* Weapon;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<class AUrathaWeapon> WeaponBlueprint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* FistCollider_Left;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* FistCollider_Right;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* ElbowCollider_Left;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* ElbowCollider_Right;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* FootCollider_Left;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* FootCollider_Right;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* KneeCollider_Left;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* KneeCollider_Right;

protected:
#pragma region Basic movement parameters (UProperties)
	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float MovementSpeedCrouching = 150.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float MovementSpeedWalking = 300.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float MovementSpeedRunning = 600.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float MovementSpeedSpriting = 900.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float JumpVelocityCrouching = 200.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float JumpVelocityWalking = 450.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float JumpVelocityRunning = 450.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Setup")
	float JumpVelocitySpriting = 600.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Uratha Character Setup")
	bool Aiming = false;

	uint8 TeamID;
#pragma endregion

public:
	AUrathaCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

private:
	/**
	* Called in order to change movement speed and enable/disable specific state of the character. Also changes jump velocity.
	* @param enabled	Whether to enable or to disable specific state
	* @param newMovementSpeed	Movement speed to set if @param enabled @param is true. Revert to default value if @param enabled @param enabled is false.
	* @param newJumpVelocity	Jump velocity to set if @param enabled @param is true. Revert to default value if @param enabled @param enabled is false.
	*/
	void ToggleSpeedAndJumpVelocity(bool enabled, float newMovementSpeed, float newJumpVelocity);

protected:
	virtual void BeginPlay();

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	void SetupUnarmedCollision();

	void AutoDetermineTeamIDByControllerType();
	
	UFUNCTION(BlueprintCallable, Category = "UrathaCharacter")
	bool IsOtherHostile(AUrathaCharacter* other);

	/*UFUNCTION(BlueprintCallable)
	void AquireAbility(UGameplayAbility abilityToAquire);*/

	UFUNCTION(BlueprintCallable)
	void AquireAbilities();

	UFUNCTION(BlueprintCallable)
	void AquireGifts();

	//Overridden by default it seems...
	UFUNCTION(BlueprintCallable)
	/**
	* Called in order to change movement speed and enable/disable crouching state of the character. Also changes jump velocity.
	* @param enabled	Whether to enable or to disable crouching state
	*/
	void CrouchUratha(bool enabled);

	UFUNCTION(BlueprintCallable)
	/**
	* Called in order to change movement speed and enable/disable walking state of the character. Also changes jump velocity.
	* @param enabled	Whether to enable or to disable walking state
	*/
	void Walk(bool enabled);

	UFUNCTION(BlueprintCallable)
	/**
	* Called in order to change movement speed and enable/disable sprinting state of the character. Also changes jump velocity.
	* @param enabled	Whether to enable or to disable sprinting state
	*/
	void Sprint(bool enabled);

	UFUNCTION(BlueprintCallable)
	/**
	* If player is standing in place or sprinting return true (in those cases player should orient to movement).
	*/
	bool ShouldOrientToMovement();

	UFUNCTION(BlueprintCallable)
	/**
	* Equip predefined player weapon (TripleShotgun).
	*/
	void EquipWeapon(TSubclassOf<AUrathaWeapon> weaponBluprintToSpawn);

	UFUNCTION(BlueprintCallable)
	/**
	* Unequip predefined player weapon (TripleShotgun), and "equip" hands for unarmed combat.
	*/
	void UnequipWeapon();

	UFUNCTION(BlueprintCallable)
	TSubclassOf<class AUrathaWeapon> GetWeaponBlueprint();

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	EHitDirection DetermineAttackDirection(AActor* attacker, AActor* victim, float frontDotTresholdMin = 0.75f, float frontDotTresholdMax = 1.0f, float rightDotTresholdMin = 0.75f, float rightDotTresholdMax = 1.0f);

	UFUNCTION(BlueprintCallable, Category = "Uratha")
	FString DetermineAttackReactionKey(EHitDirection hitDirection, FString abilityName, FGameplayTag receivedEventTag, bool heavyAttack, bool unarmedAttack = true);

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	// Inherited via IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	/*UFUNCTION(BlueprintCallable)
	UUrathaAttributeSet* GetAttributeSet() const;*/

	UFUNCTION(BlueprintCallable)
	UUrathaTalentSet* GetAttributeTalentSet() const;

	UFUNCTION(BlueprintPure)
	class AUrathaWeapon* GetWeapon();

	bool IsAiming();

	uint8 GetTeamID();
};

