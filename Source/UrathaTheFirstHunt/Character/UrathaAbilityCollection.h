// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilitySet.h"
#include "UrathaAbility.h"
#include "UrathaAbilityCollection.generated.h"

USTRUCT()
struct FUrathaAbilityBindInfo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, Category = "Abilities")
	EUrathaAbilityTalentTree AbilityTalentTree;

	UPROPERTY(EditAnywhere, Category = "Abilities")
	TSubclassOf<UUrathaAbility> GameplayAbilityClass;
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaAbilityCollection : public UDataAsset
{
	GENERATED_BODY()
	
public:
	UUrathaAbilityCollection();

	UPROPERTY(EditAnywhere, Category = "AbilityCollection")
	TArray<FUrathaAbilityBindInfo>	Abilities;

	void GiveAllAbilities(UAbilitySystemComponent* abilitySystemComponent);

	void GiveSingleAbility(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaAbility> abilityToGive);

	bool HasThisAbility(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaAbility> abilityToGive);
};
