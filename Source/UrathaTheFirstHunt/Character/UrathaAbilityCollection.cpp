// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaAbilityCollection.h"
#include "AbilitySystemComponent.h"

UUrathaAbilityCollection::UUrathaAbilityCollection()
{
	
}

void UUrathaAbilityCollection::GiveAllAbilities(UAbilitySystemComponent* abilitySystemComponent)
{
	if (abilitySystemComponent)
	{
		for (FUrathaAbilityBindInfo abilityInfo : Abilities)
		{
			if (abilityInfo.GameplayAbilityClass)
			{
				if (!HasThisAbility(abilitySystemComponent, abilityInfo.GameplayAbilityClass))
				{
					abilitySystemComponent->GiveAbility(FGameplayAbilitySpec(abilityInfo.GameplayAbilityClass, 1, 0));
				}
			}
			abilitySystemComponent->InitAbilityActorInfo(abilitySystemComponent->GetOwnerActor(), abilitySystemComponent->GetOwnerActor());
		}
	}
}

void UUrathaAbilityCollection::GiveSingleAbility(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaAbility> abilityToGive)
{
	if (abilitySystemComponent && abilityToGive)
	{
		if (!HasThisAbility(abilitySystemComponent, abilityToGive))
		{
			abilitySystemComponent->GiveAbility(FGameplayAbilitySpec(abilityToGive, 1, 0));
			abilitySystemComponent->InitAbilityActorInfo(abilitySystemComponent->GetOwnerActor(), abilitySystemComponent->GetOwnerActor());
		}
	}
}

bool UUrathaAbilityCollection::HasThisAbility(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaAbility> abilityToGive)
{
	bool HasAbility = false;

	TArray<FGameplayAbilitySpec> ActivatableAbilities = abilitySystemComponent->GetActivatableAbilities();
	
	for (FGameplayAbilitySpec AbilitySpec : ActivatableAbilities)
	{
		if (AbilitySpec.Ability->GetName().Contains(abilityToGive->GetName()))
		{
			HasAbility = true;
			break;
		}
	}

	return HasAbility;
}
