// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "../AbilitySystemComponent.h"
#include "UrathaAttributeSet.h"
#include "UrathaTalentSet.h"
#include "UrathaCharacterAbilities.h"
#include "UrathaAbilityCollection.h"
#include "UrathaGiftCollection.h"
#include "UrathaPatrolComponent.h"
#include "../Weapons/UrathaWeapon.h"

//////////////////////////////////////////////////////////////////////////
// AUrathaTheFirstHuntCharacter

AUrathaCharacter::AUrathaCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and animation blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>("AbilitySystemComponent");
	/*AttributeSetComponent = CreateDefaultSubobject<UUrathaAttributeSet>("AttributeSetComponent");*/
	AttributeTalentSetComponent = CreateDefaultSubobject<UUrathaTalentSet>("AttributeTalentSetComponent");

	PatrolComponent = CreateDefaultSubobject<UUrathaPatrolComponent>("PatrolComponent");

	TeamID = 255;
	SetupUnarmedCollision();
}

void AUrathaCharacter::BeginPlay()
{
	Super::BeginPlay();
	AutoDetermineTeamIDByControllerType();
}

//////////////////////////////////////////////////////////////////////////
// Input

void AUrathaCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	//PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	//PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AUrathaCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AUrathaCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AUrathaCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AUrathaCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &AUrathaCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AUrathaCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AUrathaCharacter::OnResetVR);
}

void AUrathaCharacter::SetupUnarmedCollision()
{
	FistCollider_Left = CreateDefaultSubobject<UBoxComponent>(TEXT("FistCollider_Left"));
	FistCollider_Left->SetCollisionProfileName(FName("Unarmed"));
	FistCollider_Left->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "hand_l");
	/*FistCollider_Left->RelativeLocation = FVector(10.f, 0.f, 0.f);*/
	FistCollider_Left->SetRelativeLocation(FVector(10.f, 0.f, 0.f));
	FistCollider_Left->SetBoxExtent(FVector(8.f, 8.f, 8.f));

	FistCollider_Right = CreateDefaultSubobject<UBoxComponent>(TEXT("FistCollider_Right"));
	FistCollider_Right->SetCollisionProfileName(FName("Unarmed"));
	FistCollider_Right->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "hand_r");
	/*FistCollider_Right->RelativeLocation = FVector(-10.f, 0.f, 0.f);*/
	FistCollider_Right->SetRelativeLocation(FVector(-10.f, 0.f, 0.f));
	FistCollider_Right->SetBoxExtent(FVector(8.f, 8.f, 8.f));

	ElbowCollider_Left = CreateDefaultSubobject<UBoxComponent>(TEXT("ElbowCollider_Left"));
	ElbowCollider_Left->SetCollisionProfileName(FName("Unarmed"));
	ElbowCollider_Left->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "lowerarm_l");
	ElbowCollider_Left->SetBoxExtent(FVector(8.f, 8.f, 8.f));

	ElbowCollider_Right = CreateDefaultSubobject<UBoxComponent>(TEXT("ElbowCollider_Right"));
	ElbowCollider_Right->SetCollisionProfileName(FName("Unarmed"));
	ElbowCollider_Right->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "lowerarm_r");
	ElbowCollider_Right->SetBoxExtent(FVector(8.f, 8.f, 8.f));

	FootCollider_Left = CreateDefaultSubobject<UBoxComponent>(TEXT("FootCollider_Left"));
	FootCollider_Left->SetCollisionProfileName(FName("Unarmed"));
	FootCollider_Left->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "foot_l");
	/*FootCollider_Left->RelativeLocation = FVector(8.f, 0.f, 0.f);*/
	FootCollider_Left->SetRelativeLocation(FVector(8.f, 0.f, 0.f));
	FootCollider_Left->SetBoxExtent(FVector(12.f, 8.f, 8.f));

	FootCollider_Right = CreateDefaultSubobject<UBoxComponent>(TEXT("FootCollider_Right"));
	FootCollider_Right->SetCollisionProfileName(FName("Unarmed"));
	FootCollider_Right->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "foot_r");
	/*FootCollider_Right->RelativeLocation = FVector(-8.f, 0.f, 0.f);*/
	FootCollider_Right->SetRelativeLocation(FVector(-8.f, 0.f, 0.f));
	FootCollider_Right->SetBoxExtent(FVector(12.f, 8.f, 8.f));

	KneeCollider_Left = CreateDefaultSubobject<UBoxComponent>(TEXT("KneeCollider_Left"));
	KneeCollider_Left->SetCollisionProfileName(FName("Unarmed"));
	KneeCollider_Left->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "calf_l");
	KneeCollider_Left->SetBoxExtent(FVector(8.f, 8.f, 8.f));

	KneeCollider_Right = CreateDefaultSubobject<UBoxComponent>(TEXT("KneeCollider_Right"));
	KneeCollider_Right->SetCollisionProfileName(FName("Unarmed"));
	KneeCollider_Right->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, "calf_r");
	KneeCollider_Right->SetBoxExtent(FVector(8.f, 8.f, 8.f));
}

void AUrathaCharacter::AutoDetermineTeamIDByControllerType()
{
	if (GetController() && GetController()->IsPlayerController())
	{
		TeamID = 0;
	}
}

bool AUrathaCharacter::IsOtherHostile(AUrathaCharacter* other)
{
	return TeamID != other->GetTeamID();
}

void AUrathaCharacter::AquireAbilities()
{
	UUrathaCharacterAbilities* characterAbilities = Cast<UUrathaCharacterAbilities>(GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
	if (characterAbilities && AbilitySystemComponent)
	{
		characterAbilities->GetUrathaAbilityCollection()->GiveAllAbilities(AbilitySystemComponent);
	}
}

void AUrathaCharacter::AquireGifts()
{
	UUrathaCharacterAbilities* characterAbilities = Cast<UUrathaCharacterAbilities>(GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
	if (characterAbilities && AbilitySystemComponent)
	{
		characterAbilities->GetUrathaGiftCollection()->GiveAllGifts(AbilitySystemComponent);
	}
}

void AUrathaCharacter::ToggleSpeedAndJumpVelocity(bool enabled, float newMovementSpeed, float newJumpVelocity)
{
	UCharacterMovementComponent* movementComponent = GetCharacterMovement();
	if (movementComponent == nullptr)
	{
		return;
	}
	else
	{
		if (enabled)
		{
			movementComponent->MaxWalkSpeed = newMovementSpeed;
			movementComponent->JumpZVelocity = newJumpVelocity;
		}
		else
		{
			movementComponent->MaxWalkSpeed = MovementSpeedRunning;
			movementComponent->JumpZVelocity = JumpVelocityRunning;
		}
	}
}

void AUrathaCharacter::CrouchUratha(bool enabled)
{
	ToggleSpeedAndJumpVelocity(enabled, MovementSpeedCrouching, JumpVelocityCrouching);
}

void AUrathaCharacter::Walk(bool enabled)
{
	ToggleSpeedAndJumpVelocity(enabled, MovementSpeedWalking, JumpVelocityWalking);
}

void AUrathaCharacter::Sprint(bool enabled)
{
	ToggleSpeedAndJumpVelocity(enabled, MovementSpeedSpriting, JumpVelocitySpriting);
}

bool AUrathaCharacter::ShouldOrientToMovement()
{
	UCharacterMovementComponent* movementComponent = GetCharacterMovement();
	if (movementComponent == nullptr)
	{
		return false;
	}
	else
	{
		if (movementComponent->GetMaxSpeed() > MovementSpeedRunning ||
			movementComponent->Velocity == FVector::ZeroVector)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

void AUrathaCharacter::EquipWeapon(TSubclassOf<AUrathaWeapon> weaponBluprintToSpawn)
{
	if (!weaponBluprintToSpawn)
	{
		UE_LOG(LogTemp, Warning, TEXT("No weapon blueprint selected!"));
		return;
	}

	//UE4 sometimes struggles with passing socket name as TEXT("SocketName"). Using this instead...
	FName SocketName("WeaponSocket");
	FActorSpawnParameters spawnParams;
	spawnParams.Owner = this;

	//Spawn weapon at 0,0,0 with 0,0,0 rotation. A precaution... 
	Weapon = GetWorld()->SpawnActor<AUrathaWeapon>(weaponBluprintToSpawn, FVector(0.f, 0.f, 0.f), FRotator(0.f, 0.f, 0.f), spawnParams);
	UE_LOG(LogTemp, Warning, TEXT("Weapon spawned!"));

	//Attach to player's hand. FINALLY WORKING!
	Weapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
}

void AUrathaCharacter::UnequipWeapon()
{
	if (!Weapon)
	{
		return;
	}
	Weapon->Destroy();
}

TSubclassOf<class AUrathaWeapon> AUrathaCharacter::GetWeaponBlueprint()
{
	return WeaponBlueprint;
}

class AUrathaWeapon* AUrathaCharacter::GetWeapon()
{
	return Weapon;
}

EHitDirection AUrathaCharacter::DetermineAttackDirection(AActor* attacker, AActor* victim, float frontDotTresholdMin, float frontDotTresholdMax, float rightDotTresholdMin, float rightDotTresholdMax)
{
	float frontDotProduct = FVector::DotProduct(attacker->GetActorForwardVector(), victim->GetActorForwardVector());
	float rightDotProduct = FVector::DotProduct(attacker->GetActorForwardVector(), victim->GetActorRightVector());

	if (FMath::IsWithinInclusive<float>(frontDotProduct, -frontDotTresholdMax, -frontDotTresholdMin))
	{
		return EHitDirection::HIT_FRONT;
	}
	if (FMath::IsWithinInclusive<float>(frontDotProduct, frontDotTresholdMin, frontDotTresholdMax))
	{
		return EHitDirection::HIT_BACK;
	}
	if (FMath::IsWithinInclusive<float>(rightDotProduct, -rightDotTresholdMax, -rightDotTresholdMin))
	{
		return EHitDirection::HIT_RIGHT;
	}
	if (FMath::IsWithinInclusive<float>(rightDotProduct, rightDotTresholdMin, rightDotTresholdMax))
	{
		return EHitDirection::HIT_LEFT;
	}

	return EHitDirection::HIT_FRONT;
}

FString AUrathaCharacter::DetermineAttackReactionKey(EHitDirection hitDirection, FString abilityName, FGameplayTag receivedEventTag, bool heavyAttack, bool unarmedAttack/* = true*/)
{
	FString attackReactionKey("");
	FString hitDirectionString("hitFront");
	FString attackModifierString("");

	if (unarmedAttack)
	{
		switch (hitDirection)
		{
		case EHitDirection::HIT_FRONT: { hitDirectionString = "hitFront"; }
			break;
		case EHitDirection::HIT_BACK: { hitDirectionString = "hitBack"; }
			break;
		case EHitDirection::HIT_LEFT: { hitDirectionString = "hitLeft"; }
			break;
		case EHitDirection::HIT_RIGHT: { hitDirectionString = "hitRight"; }
			break;
		default: { hitDirectionString = "hitFront"; }
			break;
		}

		if (abilityName.Contains("combo", ESearchCase::IgnoreCase, ESearchDir::FromStart))
		{
			if (heavyAttack)
			{
				attackModifierString = ".heavy";
			}
			else
			{
				attackModifierString = ".light";
			}
		}
		
		FString eventTagName = receivedEventTag.GetTagName().ToString();

		TArray<FStringFormatArg> stringArguments;
		stringArguments.Add(eventTagName);
		stringArguments.Add(hitDirectionString);
		stringArguments.Add(abilityName);
		stringArguments.Add(attackModifierString);
		attackReactionKey = FString::Format(TEXT("{0}.{1}.{2}{3}"), stringArguments);
	}

	return attackReactionKey;
}

UAbilitySystemComponent* AUrathaCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

UUrathaTalentSet* AUrathaCharacter::GetAttributeTalentSet() const
{
	return AttributeTalentSetComponent;
}

bool AUrathaCharacter::IsAiming()
{
	return Aiming;
}

uint8 AUrathaCharacter::GetTeamID()
{
	return TeamID;
}

//UUrathaAttributeSet* AUrathaCharacter::GetAttributeSet() const
//{
//	return AttributeSetComponent;
//}

void AUrathaCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AUrathaCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void AUrathaCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void AUrathaCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AUrathaCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AUrathaCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AUrathaCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}
