// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterAbility.h"

UrathaCharacterAbility::UrathaCharacterAbility()
{
	AbilityID = 0;
	AbilityName = FString("SampleAbility");
}

UrathaCharacterAbility::UrathaCharacterAbility(int abilityID, FString abilityName)
{
	AbilityID = abilityID;
	AbilityName = abilityName;
}

UrathaCharacterAbility::~UrathaCharacterAbility()
{
	UE_LOG(LogTemp, Error, TEXT("UrathaCharacterAbilityGameService - %s's DESTRUCTOR CALLED"), *this->AbilityName);
}
