// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "../Plugins/Runtime/GameplayAbilities/Source/GameplayAbilities/Public/AttributeSet.h"
#include "UrathaAttributeSet.generated.h"

//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttributeChange);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttributeChangeTwo, FName, attributeName, float, CurrentValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAttributeChange, FName, AttributeName, float, CurrentValue, float, MaxValue);

/**
 * 
 */
UCLASS(Blueprintable)
class URATHATHEFIRSTHUNT_API UUrathaAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

private:
#pragma region Constants (used in logic)
	const int URATHA_POINTS_DIVIDER = 1000; //Gain 1 Uratha point (used to improve character) on every 1000 experience points (XP)
	const int PRIMAL_URGE_DIVIDER = 50; //Gain 1 Primal Urge point on every 50 spent Uratha Points
	int ATTRIBUTE_BUY_COST = 4; //Convert to UPROPERTY???
	int SKILL_BUY_COST = 2; //Convert to UPROPERTY???
	int RENOWN_BUY_COST = 5; //Convert to UPROPERTY???
	const float ATTRIBUTE_MIN_VALUE = 1.f;
	const float ATTRIBUTE_MAX_VALUE = 5.f;
	const float SKILL_MIN_VALUE = 0.f;
	const float SKILL_MAX_VALUE = 5.f;
	const float RENOWN_MIN_VALUE = 0.f;
	const float RENOWN_MAX_VALUE = 5.f;
	const float BASE_HEALTH = 200.f;
#pragma endregion

public:
	UUrathaAttributeSet();

	void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data) override;

	FOnAttributeChange OnAttributeChange;

#pragma region Character basic stats
	/**
	* Character's Health
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")	
	FGameplayAttributeData Health;
	/**
	* Character's Maximum Health. This value will update every time Health is increased by some effect.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData MaxHealth;

	/**
	* Character's Essence
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Essence;
	/**
	* Character's Maximum Essence. This value will update every time Essence is increased by some effect.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData MaxEssence;

	/**
	* Character's PrimalUrge. It will add damage bonus, for now.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData PrimalUrge;
	/**
	* Character's Maximum PrimalUrge. This value will never change since PrimalUrge is capped to 10 by design.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData MaxPrimalUrge;
#pragma endregion
#pragma region Character points
	/**
	* Character's ExperiencePoints. ExperiencePoints are used to determine if character has leveled up. Each level up will yield UrathaPoints.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData ExperiencePoints;

	/**
	* Character's UrathaPoints. Uratha points are used to increase character attributes. This is done by WoD rules of "new dot x attribute price"
	(e.g. Raise Strength to 3 -> 3 x 4 = 12 Uratha points).
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData UrathaPoints;

	/**
	* Character's SpentUrathaPoints. SpentUrathaPoints are used to determine the PrimalUrge level. 50 spent Uratha Points for next PrimalUrge level.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData SpentUrathaPoints;
#pragma endregion
#pragma region Character Attributes
	/**
	* Character's Intelligence
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Intelligence;

	/**
	* Character's Wits
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Wits;

	/**
	* Character's Resolve
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Resolve;

	/**
	* Character's Strength
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Strength;

	/**
	* Character's Agility
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Agility;

	/**
	* Character's Physique
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Physique;

	/**
	* Character's Presence
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Presence;

	/**
	* Character's Manipulation
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Manipulation;

	/**
	* Character's Composure
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Composure;
#pragma endregion
#pragma region Character Derived Attributes
	/**
	* Character's Derived attribute Knowledge = (Intelligence + Wits + Resolve) / 3
	* It will be used to "spot" weaknesses in enemies and add damage bonus
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Knowledge;

	/**
	* Character's Derived attribute Athletics = (Strength + Agility + Physique) / 3
	* It will add Health bonus
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Athletics;

	/**
	* Character's Derived attribute Empathy = (Presence + Manipulation + Composure) / 3
	* It will decrease the penalty to Social interactions imposed by PrimalUrge
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Empathy;
#pragma endregion
#pragma region Character Skills
	/**
	* Character's Technology skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Technology;

	/**
	* Character's Investigation skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Investigation;

	/**
	* Character's Science skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Science;

	/**
	* Character's Brawl skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Skill"))
	FGameplayAttributeData Brawl;

	/**
	* Character's Stealth skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Stealth;

	/**
	* Character's Firearms skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Firearms;

	/**
	* Character's Intimidation skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Intimidation;

	/**
	* Character's Persuasion skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Persuasion;

	/**
	* Character's Subterfuge skill
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet")
	FGameplayAttributeData Subterfuge;
#pragma endregion

#pragma region Character Renown
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Renown"))
	FGameplayAttributeData Honor;
	FGameplayAttribute HonorAttribute()
	{
		static FProperty* Property = FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, Honor));
		return FGameplayAttribute(Property);
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Renown"))
	FGameplayAttributeData Glory;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Renown"))
	FGameplayAttributeData Cunning;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Renown"))
	FGameplayAttributeData Wisdom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaAttributeSet", meta = (RankType = "Renown"))
	FGameplayAttributeData Purity;
#pragma endregion

	UPROPERTY(VisibleAnywhere, Category = "UrathaAttributeSet")
	float HealthValue;

private:
	void CalculateDerivedAttributes();
	void CalculateHealth(float healthBonus = 0.0f);
	void CalculatePrimalUrge();
	//Delete if redundant
	void SetAttributeBaseValue(FName attributeName, float attributeValue);
	void SetAttributeCurrentValue(FName attributeName, float attributeValue);

public:
	FGameplayAttributeData* GetAttribute(FName attributeName);
	
	//Delete if redundant
	void SetAttribute(FName attributeName, float attributeValue);

	int GetCost(FGameplayAttribute gameplayAttribute);
	int GetAttributeCost() const { return ATTRIBUTE_BUY_COST; }
	int GetSkillCost() const { return SKILL_BUY_COST; }
	int GetRenownCost() { return RENOWN_BUY_COST; }
};
