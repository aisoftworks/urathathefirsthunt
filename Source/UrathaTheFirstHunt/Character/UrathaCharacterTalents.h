// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "UrathaCharacterAbility.h"

#include "UrathaCharacterTalents.generated.h"


USTRUCT()
struct FUrathaTalentLevel
{
	GENERATED_USTRUCT_BODY()

private:
	bool Unlocked;
	UrathaCharacterAbility* TalentAbility;

public:
	FUrathaTalentLevel()
	{
		Unlocked = false;
		TalentAbility = nullptr;
	}

	FUrathaTalentLevel(UrathaCharacterAbility* talentAbility)
	{
		Unlocked = false;
		TalentAbility = talentAbility;
	}

	void Unlock()
	{
		Unlocked = true;
	}

	bool IsUnlocked()
	{
		return Unlocked;
	}

	UrathaCharacterAbility* GetTalentAbility()
	{
		return TalentAbility;
	}
};

//TODO:
//Refactor FUrathaTalent with
//Primary and Secondary stat, bonus and penalty, additional bonus and penalty, Talent level, and modified talent level
//Reference to FUrathaTalentLevel struct (TArray<FUrathaTalentLevel>)

//FUrathaTalentLevel
//Level and pointer to Ability (struct for now?)
USTRUCT()
struct FUrathaTalent
{
	GENERATED_USTRUCT_BODY()

private:
	int TalentPrimaryStat;
	int TalentSecondaryStat;
	int TalentBonus;
	int TalentPenalty;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	int TalentScore; //Used as talent index in talent level array
	int TalentTemporaryBonus;
	int TalentTemporaryPenalty;
	int TalentTemoporaryScore;  //Can't be used as talent index in talent level array. It only serves to provide a bigger change of success.
	TArray<FUrathaTalentLevel> TalentLevels;

public:
	FUrathaTalent()
	{
		TalentScore = TalentTemoporaryScore = 1;
		TalentLevels = TArray<FUrathaTalentLevel>();
		//TalentLevels.SetNum(11, false);
		TalentLevels.Add(FUrathaTalentLevel()); //First is always empty in order to use TalentScore as an index. For simplicity sake.
	}

	FUrathaTalent(TArray<UrathaCharacterAbility*> talentAbilities)
	{
		TalentScore = TalentTemoporaryScore = 1;
		TalentLevels = TArray<FUrathaTalentLevel>();
		//TalentLevels.SetNum(talentAbilities.Num(), false);
		TalentLevels.Add(FUrathaTalentLevel()); //First is always empty in order to use TalentScore as an index. For simplicity sake.
		for (UrathaCharacterAbility* ability : talentAbilities)
		{
			TalentLevels.Add(FUrathaTalentLevel(ability));
		}
	}

	FUrathaTalent(int talentPrimaryStat, int talentSecondaryStat, int talentBonus, int talentPenalty)
	{
		TalentPrimaryStat = talentPrimaryStat;
		TalentSecondaryStat = talentSecondaryStat;
		TalentBonus = talentBonus;
		TalentPenalty = talentPenalty;

		TalentScore = TalentTemoporaryScore = 
			TalentPrimaryStat + TalentSecondaryStat - TalentPenalty + TalentBonus;

		TalentLevels = TArray<FUrathaTalentLevel>();
		//TalentLevels.SetNum(11, false);
		TalentLevels.Add(FUrathaTalentLevel()); //First is always empty in order to use TalentScore as an index. For simplicity sake.
	}

	FUrathaTalent(int talentPrimaryStat, int talentSecondaryStat, int talentBonus, int talentPenalty, TArray<UrathaCharacterAbility*> talentAbilities)
	{
		TalentPrimaryStat = talentPrimaryStat;
		TalentSecondaryStat = talentSecondaryStat;
		TalentBonus = talentBonus;
		TalentPenalty = talentPenalty;

		TalentScore = TalentTemoporaryScore =
			TalentPrimaryStat + TalentSecondaryStat - TalentPenalty + TalentBonus;

		TalentLevels = TArray<FUrathaTalentLevel>();
		//TalentLevels.SetNum(talentAbilities.Num(), false);
		TalentLevels.Add(FUrathaTalentLevel()); //First is always empty in order to use TalentScore as an index. For simplicity sake.
		for (UrathaCharacterAbility* ability : talentAbilities)
		{
			TalentLevels.Add(FUrathaTalentLevel(ability));
		}
	}

	/**
	* Adds Talent level without TalentAbility (call default constructor of FUrathaTalentLevel)
	* DEPRECATED
	*/
	void AddTalentLevel()
	{
		TalentLevels.Add(FUrathaTalentLevel());
	}

	/**
	* Adds Talent level with TalentAbility (passes pointer to UrathaCharacterAbility to the constructor of FUrathaTalentLevel)
	* DEPRECATED
	* @param talentAbility	Pointer to UrathaCharacterAbility
	*/
	void AddTalentLevel(UrathaCharacterAbility* talentAbility)
	{
		TalentLevels.Add(FUrathaTalentLevel());
	}

	void AddTalentTemporaryBonus(int talentTemoporaryBonus)
	{
		TalentTemporaryBonus = talentTemoporaryBonus;
		TalentTemoporaryScore = TalentScore + talentTemoporaryBonus;
	}

	void ResetTalentTemporaryBonus()
	{
		TalentTemporaryBonus = 0;
		TalentTemoporaryScore = TalentScore;
	}

	void SubstractTalentTemporaryPenalty(int talentTemoporaryPenalty)
	{
		TalentTemporaryPenalty = talentTemoporaryPenalty;
		TalentTemoporaryScore = TalentScore - talentTemoporaryPenalty;
	}

	void ResetTalentTemporaryPenalty()
	{
		TalentTemporaryPenalty = 0;
		TalentTemoporaryScore = TalentScore;
	}

	void SetTalentScore(int newValue)
	{
		TalentScore = TalentTemoporaryScore = newValue;
	}

	int GetTalentScore()
	{
		return TalentScore;
	}

	TArray<FUrathaTalentLevel> GetTalentLevels()
	{
		return TalentLevels;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class URATHATHEFIRSTHUNT_API UUrathaCharacterTalents : public UActorComponent
{
	GENERATED_BODY()

#pragma region Constants (used to avoid magic strings)
public:
	static const FString NameTalentBasic;

	static const FString NameTalentBashingSoak;
	static const FString NameTalentLethalSoak;
	static const FString NameTalentAggravatedSoak;
	static const FString NameTalentMentalSoak;

	static const FString NameTalentTechSavviness;
	static const FString NameTalentSense;
	static const FString NameTalentMedicine;
	static const FString NameTalentUnarmed;
	static const FString NameTalentSneakAndLarceny;
	static const FString NameTalentGunslinging;
	static const FString NameTalentFrightfulPresence;
	static const FString NameTalentSmoothTalking;
	static const FString NameTalentDeception;
#pragma endregion
#pragma region Logic Constants
	const int TALENT_VALUE_MODIFIER = 10;
#pragma endregion

private:
	class UUrathaCharacterStats* UrathaCharacterStats;
	class UUrathaCharacterAbilities* UrathaCharacterAbilities;

#pragma region Character Soak Talents (UProperties)
protected:
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Soaks")
	int BashingSoak = 20;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Soaks")
	int LethalSoak = 20;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Soaks")
	int AggravatedSoak = 10;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Soaks")
	int MentalSoak = 20;
#pragma endregion

#pragma region Character Talents (UProperties)
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int TechSavviness = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int Sense = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int Medicine = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int Unarmed = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int SneakAndLarceny = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int Gunslinging = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int FrightfulPresence = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int SmoothTalking = 1;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talent Value")
	int Deception = 1;
#pragma endregion

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentBashingSoak;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentLethalSoak;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentAggravatedSoak;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentMentalSoak;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentUnarmed;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentSneakAndLarceny;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentGunslinging;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentTechSavviness;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentSense;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentMedicine;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentFrightfulPresence;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentSmoothTalking;
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Talents FUrathaTalent")
	struct FUrathaTalent TalentDeception;

#pragma region Class default methods
public:	
	// Sets default values for this component's properties
	UUrathaCharacterTalents();
	~UUrathaCharacterTalents();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion

#pragma region Private methods
	/*void SetupUnarmedLevels();
	void SetupSneakAndLarcenyLevels();
	void SetupGunslingingLevels();
	void SetupTechSavvinessLevels();
	void SetupSenseLevels();
	void SetupMedicineLevels();
	void SetupFrightfulPresenceLevels();
	void SetupSmoothTalkingLevels();
	void SetupDeceptionLevels();*/

	void SetupTalentLevels(FUrathaTalent& talent, FString talentName, int primaryStat, int secondaryStat, int bonus, int penalty);
	void SetupTalents();
	
	void CalculateTalentValues();
	
	void AddSpecialBonusToTalentValue(FString talentName, int bonusValue);
#pragma endregion
		
#pragma region Public getter methods
	/**
	* Used for getting Uratha character talent values.
	* @param attributeName	PLEASE USE UUrathaCharacterTalents::NameSkillName
	*/
	int GetTalentValue(FString talentName);

	int GetTalentStructScore(FString talentName);
	FUrathaTalent* GetTalentStruct(FString talentName);
	TArray<FUrathaTalentLevel> GetTalentStructAbilities(FString talentName);

	void SetTalentValue(FString talentName, int newValue);
	void SetTalentStructScore(FString talentName, int newValue);
#pragma endregion

#pragma region Public methods
	void UseTalentAbilityPassive(FString talentName, FString talentValueName, int abilityTalentLevel);
	void UseTalentAbilityActive(FString talentName, int abilityTalentLevel);
#pragma endregion

};
