// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaAttributeSet.h"
#include "GameplayEffectExtension.h"
#include "GameplayEffect.h"

UUrathaAttributeSet::UUrathaAttributeSet()
	: Health(BASE_HEALTH),
	MaxHealth(BASE_HEALTH),
	Essence(100.f),
	MaxEssence(100.f),
	PrimalUrge(1.f),
	MaxPrimalUrge(10.f),
	ExperiencePoints(0.f),
	UrathaPoints(100.f),
	SpentUrathaPoints(50.f),
	Intelligence(ATTRIBUTE_MIN_VALUE),
	Wits(ATTRIBUTE_MIN_VALUE),
	Resolve(ATTRIBUTE_MIN_VALUE),
	Strength(ATTRIBUTE_MIN_VALUE),
	Agility(ATTRIBUTE_MIN_VALUE),
	Physique(ATTRIBUTE_MIN_VALUE),
	Presence(ATTRIBUTE_MIN_VALUE),
	Manipulation(ATTRIBUTE_MIN_VALUE),
	Composure(ATTRIBUTE_MIN_VALUE),
	Knowledge(ATTRIBUTE_MIN_VALUE),
	Athletics(ATTRIBUTE_MIN_VALUE),
	Empathy(ATTRIBUTE_MIN_VALUE),
	Technology(SKILL_MIN_VALUE),
	Investigation(SKILL_MIN_VALUE),
	Science(SKILL_MIN_VALUE),
	Brawl(2.f),
	Stealth(SKILL_MIN_VALUE),
	Firearms(4.f),
	Intimidation(SKILL_MIN_VALUE),
	Persuasion(SKILL_MIN_VALUE),
	Subterfuge(SKILL_MIN_VALUE),
	Honor(4.f),
	Glory(2.f),
	Cunning(RENOWN_MIN_VALUE),
	Wisdom(3.f),
	Purity(2.f)
{
	//TEST
	CalculateDerivedAttributes();
	CalculatePrimalUrge();
	CalculateHealth();
	HealthValue = Health.GetCurrentValue();
	/*UE_LOG(LogTemp, Warning, TEXT("Health hopefully: %f"), GetAttribute(FName("Health"))->GetCurrentValue());*/
}

void UUrathaAttributeSet::PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData &Data)
{
	//Data.EvaluatedData.Attribute.GetGameplayAttributeData(this)->GetBaseValue();
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, Health)))
	{
		//Set value using a method from this class
		HealthValue = Health.GetCurrentValue();
		Health.SetCurrentValue(FMath::Clamp(Health.GetCurrentValue(), 0.f, MaxHealth.GetCurrentValue()));
		Health.SetBaseValue(FMath::Clamp(Health.GetBaseValue(), 0.f, MaxHealth.GetBaseValue()));
		HealthValue = Health.GetCurrentValue();

		OnAttributeChange.Broadcast(GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, Health), Health.GetCurrentValue(), MaxHealth.GetCurrentValue());
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, MaxHealth)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, Essence)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, MaxEssence)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, PrimalUrge)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, MaxPrimalUrge)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, ExperiencePoints)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, UrathaPoints)))
	{
		//Set value using a method from this class
	}
	if (Data.EvaluatedData.Attribute.GetUProperty() == FindFieldChecked<FProperty>(UUrathaAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UUrathaAttributeSet, SpentUrathaPoints)))
	{
		//Set value using a method from this class
	}
	else
	{
		//Broadcast delegate
		//OnAttributeChange.Broadcast();
	}
}

void UUrathaAttributeSet::CalculateDerivedAttributes()
{
	Knowledge.SetCurrentValue(FMath::DivideAndRoundDown<int>((Intelligence.GetCurrentValue() + Wits.GetCurrentValue() + Resolve.GetCurrentValue()), 3.f));
	Knowledge.SetBaseValue(FMath::DivideAndRoundDown<int>((Intelligence.GetBaseValue() + Wits.GetBaseValue() + Resolve.GetBaseValue()), 3.f));

	Athletics.SetCurrentValue(FMath::DivideAndRoundDown<int>((Strength.GetCurrentValue() + Agility.GetCurrentValue() + Physique.GetCurrentValue()), 3.f));
	Athletics.SetBaseValue(FMath::DivideAndRoundDown<int>((Strength.GetBaseValue() + Agility.GetBaseValue() + Physique.GetBaseValue()), 3.f));

	Empathy.SetCurrentValue(FMath::DivideAndRoundDown<int>((Presence.GetCurrentValue() + Manipulation.GetCurrentValue() + Composure.GetCurrentValue()), 3.f));
	Empathy.SetBaseValue(FMath::DivideAndRoundDown<int>((Presence.GetBaseValue() + Manipulation.GetBaseValue() + Composure.GetBaseValue()), 3.f));
}

void UUrathaAttributeSet::CalculateHealth(float healthBonus)
{
	HealthValue = BASE_HEALTH + (Athletics.GetCurrentValue() + Physique.GetCurrentValue()) * 10 + healthBonus;

	MaxHealth.SetCurrentValue(HealthValue);
	MaxHealth.SetBaseValue(HealthValue);

	Health.SetCurrentValue(FMath::Clamp(HealthValue, 0.f, MaxHealth.GetCurrentValue()));
	Health.SetBaseValue(FMath::Clamp(HealthValue, 0.f, MaxHealth.GetBaseValue()));
}

void UUrathaAttributeSet::CalculatePrimalUrge()
{
	PrimalUrge.SetCurrentValue(1.f + FMath::DivideAndRoundDown<int>(SpentUrathaPoints.GetCurrentValue(), PRIMAL_URGE_DIVIDER));
	PrimalUrge.SetBaseValue(1.f + FMath::DivideAndRoundDown<int>(SpentUrathaPoints.GetBaseValue(), PRIMAL_URGE_DIVIDER));
}

void UUrathaAttributeSet::SetAttributeBaseValue(FName attributeName, float attributeValue)
{
	FStructProperty* StructProperty = FindFieldChecked<FStructProperty>(UUrathaAttributeSet::StaticClass(), attributeName);
	if (StructProperty != nullptr)
	{
		FGameplayAttributeData* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);
		StructValuePtr->SetBaseValue(attributeValue);
	}

	return;
}

void UUrathaAttributeSet::SetAttributeCurrentValue(FName attributeName, float attributeValue)
{
	FStructProperty* StructProperty = FindFieldChecked<FStructProperty>(UUrathaAttributeSet::StaticClass(), attributeName);
	if (StructProperty != nullptr)
	{
		FGameplayAttributeData* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);
		StructValuePtr->SetCurrentValue(attributeValue);
	}

	return;
}

FGameplayAttributeData* UUrathaAttributeSet::GetAttribute(FName attributeName)
{
	FStructProperty* StructProperty = FindFieldChecked<FStructProperty>(UUrathaAttributeSet::StaticClass(), attributeName);
	if (StructProperty != nullptr)
	{
		FGameplayAttributeData* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);
		return StructValuePtr;
	}

	return nullptr;
}

void UUrathaAttributeSet::SetAttribute(FName attributeName, float attributeValue)
{
	SetAttributeCurrentValue(attributeName, attributeValue);
	SetAttributeBaseValue(attributeName, attributeValue);
}

int UUrathaAttributeSet::GetCost(FGameplayAttribute gameplayAttribute)
{
	if (gameplayAttribute.GetUProperty()->HasMetaData(FName("RankType")))
	{
		if (gameplayAttribute.GetUProperty()->GetMetaData(FName("RankType")) == FString("Attribute"))
		{
			return GetAttributeCost();
		}
		if (gameplayAttribute.GetUProperty()->GetMetaData(FName("RankType")) == FString("Skill"))
		{
			return GetSkillCost();
		}
		if (gameplayAttribute.GetUProperty()->GetMetaData(FName("RankType")) == FString("Renown"))
		{
			return GetRenownCost();
		}
	}

	return 0;
}
