// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterTalents.h"
#include "GameFramework/Actor.h"

#include "UrathaCharacterStats.h"
#include "Character/UrathaCharacterAbilities.h"


#pragma region Constants (used to avoid magic strings)
const FString UUrathaCharacterTalents::NameTalentBasic = FString("Talent");

const FString UUrathaCharacterTalents::NameTalentBashingSoak = FString("BashingSoak");
const FString UUrathaCharacterTalents::NameTalentLethalSoak = FString("LethalSoak");
const FString UUrathaCharacterTalents::NameTalentAggravatedSoak = FString("AggravatedSoak");
const FString UUrathaCharacterTalents::NameTalentMentalSoak = FString("MentalSoak");

const FString UUrathaCharacterTalents::NameTalentTechSavviness = FString("TechSavviness");
const FString UUrathaCharacterTalents::NameTalentSense = FString("Sense");
const FString UUrathaCharacterTalents::NameTalentMedicine= FString("Medicine");
const FString UUrathaCharacterTalents::NameTalentUnarmed = FString("Unarmed");
const FString UUrathaCharacterTalents::NameTalentSneakAndLarceny = FString("SneakAndLarceny");
const FString UUrathaCharacterTalents::NameTalentGunslinging = FString("Gunslinging");
const FString UUrathaCharacterTalents::NameTalentFrightfulPresence = FString("FrightfulPresence");
const FString UUrathaCharacterTalents::NameTalentSmoothTalking = FString("SmoothTalking");
const FString UUrathaCharacterTalents::NameTalentDeception = FString("Deception");
#pragma endregion

// Sets default values for this component's properties
UUrathaCharacterTalents::UUrathaCharacterTalents()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

UUrathaCharacterTalents::~UUrathaCharacterTalents()
{
	
}


// Called when the game starts
void UUrathaCharacterTalents::BeginPlay()
{
	Super::BeginPlay();

	// ...
	UrathaCharacterStats = Cast<UUrathaCharacterStats>(GetOwner()->GetComponentByClass(UUrathaCharacterStats::StaticClass()));
	UrathaCharacterAbilities = Cast<UUrathaCharacterAbilities>(GetOwner()->GetComponentByClass(UUrathaCharacterAbilities::StaticClass()));
	if (UrathaCharacterStats && UrathaCharacterAbilities)
	{
		SetupTalents();
		CalculateTalentValues();
		//UrathaCharacterAbilities->ExecutePassiveAbility(&UUrathaCharacterAbilities::TestFunction);
		//UE_LOG(LogTemp, Warning, TEXT("TalentScore: %d"), GetTalentStructScore(NameTalentTechSavviness));
		//UE_LOG(LogTemp, Warning, TEXT("TalentScore: %d"), GetTalentStruct(NameTalentTechSavviness)->GetTalentScore());
		UseTalentAbilityPassive(NameTalentUnarmed, NameTalentBashingSoak, 4);
		UseTalentAbilityActive(NameTalentUnarmed, 1);
	}
}


// Called every frame
void UUrathaCharacterTalents::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//Sets up talent levels
void UUrathaCharacterTalents::SetupTalentLevels(FUrathaTalent& talent, FString talentName, int primaryStat, int secondaryStat, int bonus, int penalty)
{
	talent = FUrathaTalent(
		primaryStat,
		secondaryStat,
		bonus,
		penalty,
		UrathaCharacterAbilities->SetupTalentAbilities(talentName)
	);

	if (talent.GetTalentLevels().Num() > 1)
	{
		UE_LOG(LogTemp, Warning, TEXT("TalentLevels: %d"), talent.GetTalentLevels().Num());
		for (int i = 1; i < 11; i++)
		{
			UE_LOG(LogTemp, Warning, TEXT("TalentName: %s"), *talent.GetTalentLevels()[i].GetTalentAbility()->GetAbilityName());
		}
	}
}

void UUrathaCharacterTalents::SetupTalents()
{
	if (UrathaCharacterStats == nullptr)
	{
		return;
	}

	//Soaks
	SetupTalentLevels(
		TalentBashingSoak,
		NameTalentBashingSoak,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributePhysique),
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeAgility),
		0,
		0
	);

	SetupTalentLevels(
		TalentLethalSoak,
		NameTalentLethalSoak,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributePhysique),
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeWits),
		0,
		0
	);

	SetupTalentLevels(
		TalentAggravatedSoak,
		NameTalentAggravatedSoak,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributePhysique),
		0,
		0,
		0
	);

	SetupTalentLevels(
		TalentMentalSoak,
		NameTalentMentalSoak,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeResolve),
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeComposure),
		0,
		0
	);

	//Physical Talents
	SetupTalentLevels(
		TalentUnarmed,
		NameTalentUnarmed,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeStrength),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillBrawl),
		0,
		0
	);

	SetupTalentLevels(
		TalentSneakAndLarceny,
		NameTalentSneakAndLarceny,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeAgility),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillStealth),
		0,
		0
	);

	SetupTalentLevels(
		TalentGunslinging,
		NameTalentGunslinging,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributePhysique),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillFirearms),
		0,
		0
	);

	//Mental Talents
	SetupTalentLevels(
		TalentTechSavviness,
		NameTalentTechSavviness,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeIntelligence),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillTechnology),
		0,
		0
	);

	SetupTalentLevels(
		TalentSense,
		NameTalentSense,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeWits),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillInvestigation),
		0,
		0
	);

	SetupTalentLevels(
		TalentMedicine,
		NameTalentMedicine,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeResolve),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillScience),
		0,
		0
	);

	//Social Talents
	SetupTalentLevels(
		TalentFrightfulPresence,
		NameTalentFrightfulPresence,
		UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributePresence),
		UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillIntimidation),
		UrathaCharacterStats->GetStatPrimalUrge() / 2,
		0
	);

	if (UrathaCharacterStats->GetStatPrimalUrge() / 2 > 0)
	{
		SetupTalentLevels(
			TalentSmoothTalking,
			NameTalentSmoothTalking,
			UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeManipulation),
			UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillPersuasion),
			UrathaCharacterStats->GetDerivedAttribute(UUrathaCharacterStats::NameDerivedAttributeEmpathy),
			UrathaCharacterStats->GetStatPrimalUrge()
		);

		SetupTalentLevels(
			TalentDeception,
			NameTalentDeception,
			UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeComposure),
			UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillSubterfuge),
			UrathaCharacterStats->GetDerivedAttribute(UUrathaCharacterStats::NameDerivedAttributeEmpathy),
			UrathaCharacterStats->GetStatPrimalUrge()
		);
	}
	else
	{
		SetupTalentLevels(
			TalentSmoothTalking,
			NameTalentSmoothTalking,
			UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeManipulation),
			UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillPersuasion),
			0,
			0
		);

		SetupTalentLevels(
			TalentDeception,
			NameTalentDeception,
			UrathaCharacterStats->GetAttribute(UUrathaCharacterStats::NameAttributeComposure),
			UrathaCharacterStats->GetSkill(UUrathaCharacterStats::NameSkillSubterfuge),
			0,
			0
		);
	}
}

void UUrathaCharacterTalents::CalculateTalentValues()
{
	BashingSoak = TalentBashingSoak.GetTalentScore() * TALENT_VALUE_MODIFIER;
	LethalSoak = TalentLethalSoak.GetTalentScore() * TALENT_VALUE_MODIFIER;
	AggravatedSoak = TalentAggravatedSoak.GetTalentScore() * TALENT_VALUE_MODIFIER;
	MentalSoak = TalentMentalSoak.GetTalentScore() * TALENT_VALUE_MODIFIER;

	TechSavviness = TalentTechSavviness.GetTalentScore();
	Sense = TalentSense.GetTalentScore();
	Medicine = TalentMedicine.GetTalentScore();

	Unarmed = TalentUnarmed.GetTalentScore() * TALENT_VALUE_MODIFIER;
	SneakAndLarceny = TalentSneakAndLarceny.GetTalentScore();
	Gunslinging = TalentGunslinging.GetTalentScore() * TALENT_VALUE_MODIFIER;

	FrightfulPresence = TalentFrightfulPresence.GetTalentScore();
	SmoothTalking = TalentSmoothTalking.GetTalentScore();
	Deception = TalentDeception.GetTalentScore();
}

void UUrathaCharacterTalents::AddSpecialBonusToTalentValue(FString talentName, int bonusValue)
{
	FIntProperty* TalentProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*talentName));
	void* ValuePtr = TalentProperty->ContainerPtrToValuePtr<void>(this);
	TalentProperty->SetPropertyValue(ValuePtr, TalentProperty->GetPropertyValue(ValuePtr) + bonusValue);
}

int UUrathaCharacterTalents::GetTalentValue(FString talentName)
{
	FIntProperty* TalentProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*talentName));
	if (TalentProperty == nullptr)
	{
		return 0;
	}
	void* ValuePtr = TalentProperty->ContainerPtrToValuePtr<void>(this);
	return TalentProperty->GetPropertyValue(ValuePtr);
}

int UUrathaCharacterTalents::GetTalentStructScore(FString talentName)
{
	FStructProperty* StructProperty = FindFProperty<FStructProperty>(this->GetClass(), FName(*(NameTalentBasic + talentName)));
	if (StructProperty == nullptr)
	{
		return 0;
	}
	FUrathaTalent* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalent>(this);
	return StructValuePtr->GetTalentScore();
}

FUrathaTalent* UUrathaCharacterTalents::GetTalentStruct(FString talentName)
{
	FStructProperty* StructProperty = FindFProperty<FStructProperty>(this->GetClass(), FName(*("Talent" + talentName)));
	if (StructProperty != nullptr)
	{
		FUrathaTalent* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalent>(this);
		return StructValuePtr;
	}
	else
	{
		return nullptr;
	}
}

TArray<FUrathaTalentLevel> UUrathaCharacterTalents::GetTalentStructAbilities(FString talentName)
{
	FStructProperty* StructProperty = FindFProperty<FStructProperty>(this->GetClass(), FName(*("Talent" + talentName)));
	if (StructProperty != nullptr)
	{
		FUrathaTalent* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalent>(this);
		return StructValuePtr->GetTalentLevels();
	}
	else
	{
		return TArray<FUrathaTalentLevel>();
	}
}

void UUrathaCharacterTalents::SetTalentValue(FString talentName, int newValue)
{
	FIntProperty* TalentProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*talentName));
	void* ValuePtr = TalentProperty->ContainerPtrToValuePtr<void>(this);
	TalentProperty->SetPropertyValue(ValuePtr, newValue);
}

void UUrathaCharacterTalents::SetTalentStructScore(FString talentName, int newValue)
{
	FStructProperty* StructProperty = FindFProperty<FStructProperty>(this->GetClass(), FName(*(NameTalentBasic + talentName)));
	if (StructProperty == nullptr)
	{
		return;
	}
	FUrathaTalent* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalent>(this);
	StructValuePtr->SetTalentScore(newValue);
}

void UUrathaCharacterTalents::UseTalentAbilityPassive(FString talentName, FString talentValueName, int abilityTalentLevel)
{
	int newTalentValue;

	auto talentStruct = GetTalentStruct(talentName);
	auto talentStructLevels = GetTalentStructAbilities(talentName);
	UE_LOG(LogTemp, Warning, TEXT("Used TalentName: %s"), *talentStruct->GetTalentLevels()[abilityTalentLevel].GetTalentAbility()->GetAbilityName());
	UE_LOG(LogTemp, Warning, TEXT("Used TalentScore: %d"), GetTalentStruct(talentName)->GetTalentScore());

	UrathaCharacterAbilities->ExecutePassiveAbility(
		(UrathaCharacterPassiveAbility*)GetTalentStruct(talentName)->GetTalentLevels()[abilityTalentLevel].GetTalentAbility(),
		GetTalentStruct(talentName)->GetTalentScore(),
		GetTalentValue(talentValueName),
		OUT newTalentValue
	);
	UE_LOG(LogTemp, Warning, TEXT("New Value: %d"), newTalentValue);
	SetTalentValue(talentValueName, newTalentValue);
}

void UUrathaCharacterTalents::UseTalentAbilityActive(FString talentName, int abilityTalentLevel)
{
	auto talentStruct = GetTalentStruct(talentName);
	auto talentStructLevels = GetTalentStructAbilities(talentName);

	UrathaCharacterAbilities->ExecuteActiveAbility(
		(UrathaCharacterActiveAbility*)GetTalentStruct(talentName)->GetTalentLevels()[abilityTalentLevel].GetTalentAbility(),
		GetTalentStruct(talentName)->GetTalentScore(),
		GetTalentValue(talentName)
	);
}
