// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "GameplayAbilitySet.h"
#include "Engine/DataAsset.h"
#include "Abilities/GameplayAbility.h"
#include "UrathaGift.h"
#include "UrathaTalentSet.h"
#include "Character/Utilities/UrathaGiftBindInfo.h"
#include "UrathaGiftCollection.generated.h"

USTRUCT(BlueprintType)
struct FUrathaGiftBindInformation
{
	GENERATED_USTRUCT_BODY()

public:
	FUrathaGiftBindInformation()
	{
		GiftType = EUrathaGiftType::None;
		GiftClass = nullptr;
		GiftRequirements = TArray<FUrathaGiftRequirements>();
		NumberOfRequirements = 0;
		NumberOfFullfiledRequirements = 0;
		Unlocked = false;
		UnlockedForPreview = false;
	}

	FUrathaGiftBindInformation(const FUrathaGiftBindInformation& giftBindInformation)
	{
		GiftType = giftBindInformation.GiftType;
		GiftClass = giftBindInformation.GiftClass;
		GiftRequirements = giftBindInformation.GiftRequirements;
		NumberOfRequirements = 0;
		NumberOfFullfiledRequirements = 0;
		Unlocked = giftBindInformation.Unlocked;
		UnlockedForPreview = giftBindInformation.UnlockedForPreview;
	}

	UPROPERTY(EditAnywhere, Category = "Gifts")
	EUrathaGiftType GiftType;

	UPROPERTY(EditAnywhere, Category = "Gifts")
	TSubclassOf<UUrathaGift> GiftClass;

	UPROPERTY(EditAnywhere, Category = "Gifts")
	TArray<FUrathaGiftRequirements> GiftRequirements;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	int NumberOfRequirements;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	int NumberOfFullfiledRequirements;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	bool Unlocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	bool UnlockedForPreview;

	bool MeetsRequirements()
	{
		if (NumberOfRequirements != GiftRequirements.Num())
		{
			NumberOfRequirements = GiftRequirements.Num();
		}

		return NumberOfRequirements == NumberOfFullfiledRequirements;
	}

	int RequirementDifference()
	{
		return NumberOfFullfiledRequirements - NumberOfRequirements;
	}

	void CheckRequirements(UUrathaTalentSet* talentSet)
	{
		if (GiftRequirements.Num() == 1 && !UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel == (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 1;
			}
			else
			{
				NumberOfFullfiledRequirements = 0;
			}
		}
		else if (GiftRequirements.Num() == 2 && !UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[1].RenownLevel, GiftRequirements[1].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel == (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) &&
				GiftRequirements[1].RenownLevel == (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 2;
			}
			else if (GiftRequirements[0].RenownLevel == (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) ||
				GiftRequirements[1].RenownLevel == (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 1;
			}
			else
			{
				NumberOfFullfiledRequirements = 0;
			}
		}
	}

	void CheckLostRequirements(UUrathaTalentSet* talentSet)
	{
		if (GiftRequirements.Num() == 1 && UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel > (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 0;
			}
		}
		else if (GiftRequirements.Num() == 2 && UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[1].RenownLevel, GiftRequirements[1].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel > (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) &&
				GiftRequirements[1].RenownLevel > (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 0;
			}
			else if (GiftRequirements[0].RenownLevel > (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) ||
				GiftRequirements[1].RenownLevel > (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 1;
			}
			else
			{
				NumberOfFullfiledRequirements = 2;
			}
		}
	}

	void CheckRequirementsOnStart(UUrathaTalentSet* talentSet)
	{
		if (GiftRequirements.Num() == 1 && !UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel <= (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 1;
			}
			else
			{
				NumberOfFullfiledRequirements = 0;
			}
		}
		else if (GiftRequirements.Num() == 2 && !UnlockedForPreview)
		{
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[0].RenownLevel, GiftRequirements[0].Renown.GetNumericValueChecked(talentSet));
			UE_LOG(LogTemp, Warning, TEXT("Req: %d Lvl: %f"),
				GiftRequirements[1].RenownLevel, GiftRequirements[1].Renown.GetNumericValueChecked(talentSet));

			if (GiftRequirements[0].RenownLevel <= (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) &&
				GiftRequirements[1].RenownLevel <= (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 2;
			}
			else if (GiftRequirements[0].RenownLevel <= (int)GiftRequirements[0].Renown.GetNumericValueChecked(talentSet) ||
				GiftRequirements[1].RenownLevel <= (int)GiftRequirements[1].Renown.GetNumericValueChecked(talentSet))
			{
				NumberOfFullfiledRequirements = 1;
			}
			else
			{
				NumberOfFullfiledRequirements = 0;
			}
		}
	}

	FString GetGiftName()
	{
		return GiftClass->GetName();
	}
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaGiftCollection : public UDataAsset
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int AttackGiftsUnlocked = 0;
	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int DefenseGiftsUnlocked = 0;
	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int UtilityGiftsUnlocked = 0;

	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int AttackGiftsUnlockedForPreview = 0;
	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int DefenseGiftsUnlockedForPreview = 0;
	UPROPERTY(EditAnywhere, Category = "GiftCollection", meta = (ClampMin = "0", ClampMax = "3"))
	int UtilityGiftsUnlockedForPreview = 0;
	
public:
	UUrathaGiftCollection();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "GiftCollection")
	TArray<FUrathaGiftBindInformation> Gifts;

	void GiveAllGifts(UAbilitySystemComponent* abilitySystemComponent);

	void GiveSingleGift(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaGift> giftToGive);

	bool HasThisGift(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaGift> giftToGive);

	FUrathaGiftBindInformation GetGiftBindInformation(TSubclassOf<UUrathaGift> giftToFind);

	void PrepareGiftSet();

	void CheckForNumberOfUnlockedGifts(FGameplayAttribute renown, UUrathaTalentSet* talentSet, int& attackGiftsNumber, int& defenseGiftsNumber, int& utilityGiftsNumber);

	void CheckForNumberOfUnlockedGiftsOnStart(UUrathaTalentSet* talentSet, int& attackGiftsNumber, int& defenseGiftsNumber, int& utilityGiftsNumber);
};
