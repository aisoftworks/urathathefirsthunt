// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UrathaCharacterAbility.h"

/**
 * 
 */
class URATHATHEFIRSTHUNT_API UrathaCharacterPassiveAbility : public UrathaCharacterAbility
{
private:
	int UnlocksAtLevel;
	int PercentBonusRatePerLevel;//TODO: Put BonusRatePerLevel to data table

public:
	UrathaCharacterPassiveAbility();
	UrathaCharacterPassiveAbility(int abilityID, FString abilityName);
	UrathaCharacterPassiveAbility(int abilityID, FString abilityName, int unlocksAtLevel);
	~UrathaCharacterPassiveAbility();

	//int CalculateTalentValueWithPassivePercentageBonus(int talentScore, int bonusRatePerLevel, int talentValue);

	int CalculatePassivePercentageBonus(int talentScore, int talentValue);

	int CalculateTalentValueWithPassivePercentageBonus(int talentScore, int talentValue);
};
