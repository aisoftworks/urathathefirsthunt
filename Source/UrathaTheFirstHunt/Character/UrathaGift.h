// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "AttributeSet.h"
//#include "Character/Utilities/UrathaAbilityUtilities.h"
#include "UrathaGift.generated.h"

class UMaterialInstance;
class UTexture2D;

UENUM(BlueprintType)
enum class EUrathaGiftType : uint8
{
	None = 0 UMETA(DisplayName = "None"),
	Attack = 1 UMETA(DisplayName = "Attack"),
	Defense = 2 UMETA(DisplayName = "Defense"),
	Utility = 3 UMETA(DisplayName = "Utility"),
};

UENUM(BlueprintType)
enum class EGiftCostType : uint8
{
	Essence
};

USTRUCT(BlueprintType)
struct FUrathaGiftRequirements
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftRequirements")
	FGameplayAttribute Renown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftRequirements")
	int RenownLevel;
};

USTRUCT(BlueprintType)
struct FUrathaGiftInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	FString Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	EUrathaGiftType GiftType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	FGameplayAttribute LevelsUpWith;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	float GiftEffectModifier;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	float Cooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	float Cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	EGiftCostType CostType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	UMaterialInstance* UIMaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GiftInfo")
	UTexture2D* HUDIcon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GiftInfo")
	UTexture2D* UIIcon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GiftInfo")
	UTexture2D* UIIconBorderless;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GiftInfo")
	TSubclassOf<UGameplayAbility> AbilityClass;

	FUrathaGiftInfo();
	FUrathaGiftInfo(FString name, FString description, EUrathaGiftType giftType, FGameplayAttribute levelsUpWith, float modifier, float cooldown, float cost, EGiftCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* hUDIcon, UTexture2D* uIIcon, UTexture2D* uIIconBorderless, TSubclassOf<UGameplayAbility> abilityClass);
};

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaGift : public UGameplayAbility
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	bool Unlocked = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	FString GiftName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	FString GiftDescription;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	EUrathaGiftType GiftType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	FGameplayAttribute LevelsUpWith;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	float GiftEffectModifier = 1.f;

	/**
	* MaterialInstance used for UI
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	UMaterialInstance* UIMaterialInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	UTexture2D* HUDIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	UTexture2D* UIIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaGift")
	UTexture2D* UIIconBorderless;

	UUrathaGift();

	/**
	* Used to set ability info during call. It's a bit hacky, since I want to avoid setting this during the ability construction.
	*/
	UFUNCTION(BlueprintCallable, Category = "GiftInfo")
	FUrathaGiftInfo GetGiftInfo();
};
