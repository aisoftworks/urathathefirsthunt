// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UrathaCharacterAbility.h"

/**
 * 
 */
class URATHATHEFIRSTHUNT_API UrathaCharacterActiveAbility : public UrathaCharacterAbility
{
private:
	int UnlocksAtLevel;

public:
	UrathaCharacterActiveAbility();
	UrathaCharacterActiveAbility(int abilityID, FString abilityName);
	UrathaCharacterActiveAbility(int abilityID, FString abilityName, int unlocksAtLevel);
	~UrathaCharacterActiveAbility();

	void ExecuteActiveAbility(int talentScore, int talentValue);
};
