// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Character/UrathaAbilityCollection.h"
#include "Character/UrathaGiftCollection.h"
#include "UrathaCharacterPassiveAbility.h"
#include "UrathaCharacterAbilities.generated.h"

#define OUT

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class URATHATHEFIRSTHUNT_API UUrathaCharacterAbilities : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Abilities", meta = (AllowPrivateAccess = "true"))
	UUrathaAbilityCollection* UrathaAbilitySet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Character Gifts", meta = (AllowPrivateAccess = "true"))
	UUrathaGiftCollection* UrathaGiftSet;
	
	/*class UUrathaGameInstance* gameInstance;
	class AbilitiesGameService* abilityGameService;*/

public:	
	// Sets default values for this component's properties
	UUrathaCharacterAbilities();

	UUrathaAbilityCollection* GetUrathaAbilityCollection() const
	{
		return UrathaAbilitySet;
	}

	UUrathaGiftCollection* GetUrathaGiftCollection() const
	{
		return UrathaGiftSet;
	}

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:		
	[[deprecated("Used before introducing GameplayAbilitySystem. Still present for purposes of testing smart pointers.")]]
	TArray<class UrathaCharacterAbility*> SetupTalentAbilities(FString talentName);

	//TEST
	void ExecutePassiveAbility(class UrathaCharacterPassiveAbility* abilityToExecute, int talentScore, int talentValue, OUT int& passiveBonusValue); //Possible candidate for refactor
	void ExecuteActiveAbility(class UrathaCharacterActiveAbility* abilityToExecute, int talentScore, int talentValue);
};
