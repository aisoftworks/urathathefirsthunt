// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaGift.h"

FUrathaGiftInfo::FUrathaGiftInfo()
{
	Name = FString();
	Description = FString();
	GiftType = EUrathaGiftType::None;
	LevelsUpWith = nullptr;
	GiftEffectModifier = 1.f;
	Cooldown = 0.f;
	Cost = 0.f;
	CostType = EGiftCostType::Essence;
	UIMaterialInstance = nullptr;
	HUDIcon = nullptr;
	UIIcon = nullptr;
	UIIconBorderless = nullptr;
	AbilityClass = nullptr;
}

FUrathaGiftInfo::FUrathaGiftInfo(FString name, FString description, EUrathaGiftType giftType, FGameplayAttribute levelsUpWith, float modifier, float cooldown, float cost, EGiftCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* hUDIcon, UTexture2D* uIIcon, UTexture2D* uIIconBorderless, TSubclassOf<UGameplayAbility> abilityClass)
{
	Name = name;
	Description = description;
	GiftType = giftType;
	LevelsUpWith = levelsUpWith;
	GiftEffectModifier = modifier;
	Cooldown = cooldown;
	Cost = cost;
	CostType = costType;
	UIMaterialInstance = uiMaterialInstance;
	HUDIcon = hUDIcon;
	UIIcon = uIIcon;
	UIIconBorderless = uIIconBorderless;
	AbilityClass = abilityClass;
}

UUrathaGift::UUrathaGift()
{

}

FUrathaGiftInfo UUrathaGift::GetGiftInfo()
{
	UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
	UGameplayEffect* CostEffect = GetCostGameplayEffect();

	if (CostEffect || CooldownEffect)
	{
		float Cooldown = 0.f;
		float Cost = 0.f;
		EGiftCostType CostType = EGiftCostType::Essence;

		if (CooldownEffect)
		{
			CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, Cooldown);
		}
		if (CostEffect)
		{
			CostEffect->Modifiers[0].ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Cost);
			FGameplayAttribute CostAttribute = CostEffect->Modifiers[0].Attribute;
			if (CostAttribute.AttributeName == "Essence")
			{
				CostType = EGiftCostType::Essence;
			}
		}
		//UE_LOG(LogTemp, Warning, TEXT("Creating with parameters"));
		return FUrathaGiftInfo(GiftName, GiftDescription, GiftType, LevelsUpWith, GiftEffectModifier, Cooldown, Cost, CostType, UIMaterialInstance, HUDIcon, UIIcon, UIIconBorderless, GetClass());
	}
	//UE_LOG(LogTemp, Warning, TEXT("Creating without parameters"));
	return FUrathaGiftInfo();
}
