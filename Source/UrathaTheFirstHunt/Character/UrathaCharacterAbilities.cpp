// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterAbilities.h"

#include "Engine/World.h"
#include "UrathaGameInstance.h"
#include "GameServices/AbilitiesGameService.h"
#include "UrathaCharacterAbility.h"
#include "UrathaCharacterPassiveAbility.h"
#include "UrathaCharacterActiveAbility.h"
#include "UrathaCharacterTalents.h"
#include "Utilities/DataTableStructures.h"
#include "UrathaAbilityCollection.h"
#include "UrathaGiftCollection.h"


// Sets default values for this component's properties
UUrathaCharacterAbilities::UUrathaCharacterAbilities()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...

}

// Called when the game starts
void UUrathaCharacterAbilities::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

//Returns setup abilities
TArray<class UrathaCharacterAbility*> UUrathaCharacterAbilities::SetupTalentAbilities(FString talentName)
{
	UUrathaGameInstance* gameInstance = (UUrathaGameInstance*)GetWorld()->GetGameInstance();//Get custom game instance
	AbilitiesGameService* abilityGameService = gameInstance->GetGameService<AbilitiesGameService>();//Get game service from the game instance

	TArray<UrathaCharacterAbility*> abilities = TArray<UrathaCharacterAbility*>();

	TArray<FTalentLevelsData*> dataTableTalentLevels;
	gameInstance->GetTalentLevelsDataTable()->GetAllRows<FTalentLevelsData>(FString("Get all rows"), dataTableTalentLevels);

	for (FTalentLevelsData* talentLevelRow : dataTableTalentLevels)
	{
		if (talentLevelRow->TalentName == talentName)
		{
			if (talentLevelRow->AbilityID == -1)
			{
				abilities.Add(new UrathaCharacterAbility());
			}
			else
			{
				abilities.Add(abilityGameService->GetAbilityByEnumID(static_cast<AbilitiesGameService::EAbilities>(talentLevelRow->AbilityID)));
			}
		}
	}

	return abilities;
}

void UUrathaCharacterAbilities::ExecutePassiveAbility(class UrathaCharacterPassiveAbility* abilityToExecute, int talentScore, int talentValue, OUT int& passiveBonusValue)
{
	passiveBonusValue = abilityToExecute->CalculateTalentValueWithPassivePercentageBonus(talentScore, talentValue);
}

void UUrathaCharacterAbilities::ExecuteActiveAbility(class UrathaCharacterActiveAbility* abilityToExecute, int talentScore, int talentValue)
{
	abilityToExecute->ExecuteActiveAbility(talentScore, talentValue);
}

