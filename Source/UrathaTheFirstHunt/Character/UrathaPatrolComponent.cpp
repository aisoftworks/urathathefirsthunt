// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaPatrolComponent.h"

// Sets default values for this component's properties
UUrathaPatrolComponent::UUrathaPatrolComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


TArray<AActor*> UUrathaPatrolComponent::GetPatrolPoints()
{
	return PatrolPoints;
}

// Called when the game starts
void UUrathaPatrolComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

