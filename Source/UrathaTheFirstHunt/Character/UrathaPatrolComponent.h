// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UrathaPatrolComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class URATHATHEFIRSTHUNT_API UUrathaPatrolComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, Category = "Patrol")
	TArray<AActor*> PatrolPoints;

public:	
	// Sets default values for this component's properties
	UUrathaPatrolComponent();

	TArray<AActor*> GetPatrolPoints();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

		
};
