// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaTalentSet.h"
#include "../AbilitySystemComponent.h"


UUrathaTalentSet::UUrathaTalentSet()
	:
	Bashing(Physique, Agility, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Physique), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Agility)),
	Lethal(Physique, Wits, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Physique), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Wits)),
	Aggravated(Physique, FGameplayAttributeData(), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Physique), FName()),
	Mental(Resolve, Composure, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Resolve), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Composure)),
	TechSavviness(Intelligence, Technology, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Intelligence), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Technology)),
	Sense(Wits, Investigation, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Wits), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Investigation)),
	Medicine(Resolve, Science, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Resolve), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Science)),
	Unarmed(Strength, Brawl, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Strength), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Brawl)),
	SneakAndLarceny(Agility, Stealth, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Agility), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Stealth)),
	Gunslinging(Physique, Firearms, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Physique), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Firearms)),
	FrightfulPresence(Presence, Intimidation, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Presence), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Intimidation)),
	SmoothTalking(Manipulation, Persuasion, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Manipulation), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Persuasion)),
	Deception(Composure, Subterfuge, GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Composure), GET_MEMBER_NAME_CHECKED(UUrathaTalentSet, Subterfuge))
{
	OnAttributeChange.AddDynamic(this, &UUrathaTalentSet::OnAttributeChanged);
	
	ApplyBonusesAndPenalties();

	UE_LOG(LogTemp, Warning, TEXT("PrimalUrge hopefully: %f"), GetAttribute(FName("PrimalUrge"))->GetCurrentValue());
	UE_LOG(LogTemp, Warning, TEXT("Health hopefully: %f"), GetAttribute(FName("Health"))->GetCurrentValue());
	UE_LOG(LogTemp, Warning, TEXT("Unarmed test: %f"), GetTalent(FName("Unarmed"))->GetCurrentValue());
	UE_LOG(LogTemp, Warning, TEXT("LethalSoak test: %f"), GetTalent(FName("Lethal"))->GetCurrentValue());
	UE_LOG(LogTemp, Warning, TEXT("AggravatedSoak test: %f"), GetTalent(FName("Aggravated"))->GetCurrentValue());
}

void UUrathaTalentSet::PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue)
{
	UE_LOG(LogTemp, Error, TEXT("TalentSet: Changed attribute %s to value %f"), *Attribute.GetName(), NewValue);
	//Do not call before the actual change
	//CalculateTalents();

	//Dance around the delegates...
	//GetOwningAbilitySystemComponent()->GetGameplayAttributeValueChangeDelegate(FGameplayAttribute());
}

void UUrathaTalentSet::OnAttributeChanged(FName AttributeName, float CurrentValue, float MaxValue)
{
	UE_LOG(LogTemp, Warning, TEXT("DELEGATE CALLED!"));
	UE_LOG(LogTemp, Warning, TEXT("%s -> New health: %f"), *GetOwningActor()->GetName(), GetAttribute(AttributeName)->GetCurrentValue());
	//Call calculate talents
	CalculateTalents();
}

FUrathaTalentAttributeData* UUrathaTalentSet::GetTalent(FName talentName)
{
	FStructProperty* StructProperty = FindFieldChecked<FStructProperty>(UUrathaTalentSet::StaticClass(), talentName);
	if (StructProperty != nullptr)
	{
		FUrathaTalentAttributeData* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalentAttributeData>(this);
		return StructValuePtr;
	}

	return nullptr;
}

void UUrathaTalentSet::CalculateTalents()
{
	for (TFieldIterator<FStructProperty> PropertyIterator(this->StaticClass(), EFieldIteratorFlags::ExcludeSuper); PropertyIterator; ++PropertyIterator)
	{
		FStructProperty* StructProperty = *PropertyIterator;
		//
		FString VariableName = StructProperty->GetName();
		//
		FUrathaTalentAttributeData* StructValuePtr = StructProperty->ContainerPtrToValuePtr<FUrathaTalentAttributeData>(this);
		
		FStructProperty* FirstProperty = FindFieldChecked<FStructProperty>(UUrathaAttributeSet::StaticClass(), StructValuePtr->GetFirstAttributeName());
		FStructProperty* SecondProperty = FindFieldChecked<FStructProperty>(UUrathaAttributeSet::StaticClass(), StructValuePtr->GetSecondAttributeName());

		if (FirstProperty && SecondProperty)
		{
			UE_LOG(LogTemp, Error, TEXT("TalentAttribute1: %s TalentAttribute2: %s"), 
				*StructValuePtr->GetFirstAttributeName().ToString(), *StructValuePtr->GetSecondAttributeName().ToString());
			FGameplayAttributeData* FirstPropertyValuePtr = FirstProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);
			FGameplayAttributeData* SecondPropertyValuePtr = SecondProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);

			StructValuePtr->CalculateTalentValue(FirstPropertyValuePtr, SecondPropertyValuePtr);
		}
		else if (FirstProperty)
		{
			UE_LOG(LogTemp, Error, TEXT("TalentAttribute1: %s TalentAttribute2: NONE"),
				*StructValuePtr->GetFirstAttributeName().ToString());
			FGameplayAttributeData* FirstPropertyValuePtr = FirstProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(this);

			StructValuePtr->CalculateTalentValue(FirstPropertyValuePtr);
		}

		UE_LOG(LogTemp, Warning, TEXT("Talent: %s with value: %f"), *VariableName, StructValuePtr->GetCurrentValue());
	}

	ApplyBonusesAndPenalties();
}

void UUrathaTalentSet::ApplyBonusesAndPenalties()
{
	UE_LOG(LogTemp, Warning, TEXT("PrimalUrge: %f"), PrimalUrge.GetCurrentValue());
	if (FMath::FloorToInt(PrimalUrge.GetBaseValue() / 2.f))
	{
		/*FrightfulPresence.SetBaseValue(FrightfulPresence.GetBaseValue() + FMath::FloorToInt(PrimalUrge.GetBaseValue() / 2.f));
		FrightfulPresence.SetCurrentValue(FrightfulPresence.GetCurrentValue() + FMath::FloorToInt(PrimalUrge.GetCurrentValue() / 2.f));*/
		FrightfulPresence.ApplyBonus(FMath::FloorToInt(PrimalUrge.GetBaseValue() / 2.f));
	}

	if (PrimalUrge.GetBaseValue() / 5 >= 1)//Tweak logic if needed. Current logic -> If PrimalUrge is greater or equal 5, apply PrimalUrge as penalty and Empathy as a bonus.
	{
		int BonusPenalty = FMath::FloorToInt(Empathy.GetBaseValue()) - FMath::FloorToInt(PrimalUrge.GetBaseValue() / 2.f);
		if (BonusPenalty < 0)
		{
			SmoothTalking.ApplyPenalty(BonusPenalty);
			Deception.ApplyPenalty(BonusPenalty);
		}
		else if (BonusPenalty > 0)
		{
			SmoothTalking.ApplyBonus(BonusPenalty);
			Deception.ApplyBonus(BonusPenalty);
		}
		/*SmoothTalking.SetBaseValue(SmoothTalking.GetBaseValue() + Empathy.GetBaseValue() - PrimalUrge.GetBaseValue() / 2.f);
		SmoothTalking.SetCurrentValue(SmoothTalking.GetCurrentValue() + Empathy.GetCurrentValue() - PrimalUrge.GetCurrentValue() / 2.f);*/

		/*Deception.SetBaseValue(Deception.GetBaseValue() + Empathy.GetBaseValue() - PrimalUrge.GetBaseValue() / 2.f);
		Deception.SetCurrentValue(Deception.GetCurrentValue() + Empathy.GetCurrentValue() - PrimalUrge.GetCurrentValue() / 2.f);*/
	}
	/*else
	{
		if (PrimalUrge.GetBaseValue() / 2 >= 1)
		{
			SmoothTalking.SetBaseValue(SmoothTalking.GetBaseValue() + Empathy.GetBaseValue());
			SmoothTalking.SetCurrentValue(SmoothTalking.GetCurrentValue() + Empathy.GetCurrentValue());

			Deception.SetBaseValue(Deception.GetBaseValue() + Empathy.GetBaseValue());
			Deception.SetCurrentValue(Deception.GetCurrentValue() + Empathy.GetCurrentValue());
		}
	}*/
	//Empathy is primarily used to counter PrimalUrge penalty to social talents, but maybe it should be included as a bonus?

	UE_LOG(LogTemp, Warning, TEXT("FrightfulPresence: %f"), GetTalent(FName("FrightfulPresence"))->GetBaseValue());
	UE_LOG(LogTemp, Warning, TEXT("SmoothTalking: %f"), GetTalent(FName("SmoothTalking"))->GetBaseValue());
	UE_LOG(LogTemp, Warning, TEXT("Deception: %f"), GetTalent(FName("Deception"))->GetBaseValue());
}

bool UUrathaTalentSet::TalentHasBonus(FGameplayAttribute talent)
{
	return GetTalent(*talent.GetName())->HasBonus();
}

bool UUrathaTalentSet::TalentHasPenalty(FGameplayAttribute talent)
{
	return GetTalent(*talent.GetName())->HasPenalty();
}

void FUrathaTalentAttributeData::CalculateTalentValue(FGameplayAttributeData* firstAttribute, FGameplayAttributeData* secondAttribute)
{
	BaseValue = (firstAttribute->GetBaseValue() + secondAttribute->GetBaseValue());
	CurrentValue = (firstAttribute->GetCurrentValue() + secondAttribute->GetCurrentValue());
}

void FUrathaTalentAttributeData::CalculateTalentValue(FGameplayAttributeData* firstAttribute)
{
	BaseValue = firstAttribute->GetBaseValue();
	CurrentValue = firstAttribute->GetCurrentValue();
}
