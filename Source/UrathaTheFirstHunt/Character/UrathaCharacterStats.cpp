// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterStats.h"


#pragma region Constants (used to avoid magic strings)
const FString UUrathaCharacterStats::NameAttributeIntelligence = FString("Intelligence");
const FString UUrathaCharacterStats::NameAttributeWits = FString("Wits");
const FString UUrathaCharacterStats::NameAttributeResolve= FString("Resolve");
const FString UUrathaCharacterStats::NameAttributeStrength = FString("Strength");
const FString UUrathaCharacterStats::NameAttributeAgility = FString("Agility");
const FString UUrathaCharacterStats::NameAttributePhysique = FString("Physique");
const FString UUrathaCharacterStats::NameAttributePresence = FString("Presence");
const FString UUrathaCharacterStats::NameAttributeManipulation = FString("Manipulation");
const FString UUrathaCharacterStats::NameAttributeComposure = FString("Composure");

const FString UUrathaCharacterStats::NameDerivedAttributeKnowledge = FString("Knowledge");
const FString UUrathaCharacterStats::NameDerivedAttributeAthletics = FString("Athletics");
const FString UUrathaCharacterStats::NameDerivedAttributeEmpathy = FString("Empathy");

const FString UUrathaCharacterStats::NameSkillTechnology = FString("Technology");
const FString UUrathaCharacterStats::NameSkillInvestigation = FString("Investigation");
const FString UUrathaCharacterStats::NameSkillScience = FString("Science");
const FString UUrathaCharacterStats::NameSkillBrawl = FString("Brawl");
const FString UUrathaCharacterStats::NameSkillStealth = FString("Stealth");
const FString UUrathaCharacterStats::NameSkillFirearms = FString("Firearms");
const FString UUrathaCharacterStats::NameSkillIntimidation = FString("Intimidation");
const FString UUrathaCharacterStats::NameSkillPersuasion = FString("Persuasion");
const FString UUrathaCharacterStats::NameSkillSubterfuge = FString("Subterfuge");
#pragma endregion

// Sets default values for this component's properties
UUrathaCharacterStats::UUrathaCharacterStats()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UUrathaCharacterStats::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CalculateDerivedAttributes();
	CalculateHealth(0.f);
	//Load current Uratha points
	//Load spent Uratha points
	//Load current experience points
	CalculatePrimalUrge();
	//UE_LOG(LogTemp, Warning, TEXT("Health: %f"), Health);
}


// Called every frame
void UUrathaCharacterStats::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UUrathaCharacterStats::CalculateDerivedAttributes()
{
	Knowledge = (int)((Intelligence + Wits + Resolve) / 3);
	Athletics = (int)((Strength + Agility + Physique) / 3);
	Empathy = (int)((Presence + Manipulation + Composure) / 3);
}

void UUrathaCharacterStats::CalculateHealth(float bonus)
{
	Health = BaseHealth + (Athletics + Physique) * 10 + bonus;
}

void UUrathaCharacterStats::CalculatePrimalUrge()
{
	PrimalUrge = 1 + (int)(SpentUrathaPoints / PRIMAL_URGE_DIVIDER);
}

void UUrathaCharacterStats::AddExperiencePoints(int amount)
{
	ExperiencePoints += amount;

	if (ExperiencePoints >= URATHA_POINTS_DIVIDER)
	{
		UrathaPoints += ExperiencePoints / URATHA_POINTS_DIVIDER;
		ExperiencePoints = ExperiencePoints % URATHA_POINTS_DIVIDER;
	}
}

void UUrathaCharacterStats::SetStatValue(FString statName, int newValue)
{
	FIntProperty* IntProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*statName));
	void* ValuePtr = IntProperty->ContainerPtrToValuePtr<void>(this);
	IntProperty->SetPropertyValue(ValuePtr, newValue);
}

void UUrathaCharacterStats::SetStatValue(FString statName, float newValue)
{
	FFloatProperty* FloatProperty = FindFProperty<FFloatProperty>(this->GetClass(), FName(*statName));
	void* ValuePtr = FloatProperty->ContainerPtrToValuePtr<void>(this);
	FloatProperty->SetPropertyValue(ValuePtr, newValue);
}

int UUrathaCharacterStats::GetAttribute(FString attributeName)
{
	FIntProperty* AttributeProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*attributeName));
	if (AttributeProperty == nullptr)
	{
		return 0;
	}
	void* ValuePtr = AttributeProperty->ContainerPtrToValuePtr<void>(this);
	return AttributeProperty->GetPropertyValue(ValuePtr);
}

int UUrathaCharacterStats::GetDerivedAttribute(FString derivedAttributeName)
{
	FIntProperty* DerivedAttributeProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*derivedAttributeName));
	if (DerivedAttributeProperty == nullptr)
	{
		return 0;
	}
	void* ValuePtr = DerivedAttributeProperty->ContainerPtrToValuePtr<void>(this);
	return DerivedAttributeProperty->GetPropertyValue(ValuePtr);
}

int UUrathaCharacterStats::GetSkill(FString skillName)
{
	FIntProperty* SkillProperty = FindFProperty<FIntProperty>(this->GetClass(), FName(*skillName));
	if (SkillProperty == nullptr)
	{
		return -1;
	}
	void* ValuePtr = SkillProperty->ContainerPtrToValuePtr<void>(this);
	return SkillProperty->GetPropertyValue(ValuePtr);
}
