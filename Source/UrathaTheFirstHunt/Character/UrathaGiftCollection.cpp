// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaGiftCollection.h"
#include "AbilitySystemComponent.h"

UUrathaGiftCollection::UUrathaGiftCollection()
{
	AttackGiftsUnlocked = 0;
	DefenseGiftsUnlocked = 0;
	UtilityGiftsUnlocked = 0;

	Gifts = TArray<FUrathaGiftBindInformation>();
}

void UUrathaGiftCollection::GiveAllGifts(UAbilitySystemComponent* abilitySystemComponent)
{
	if (abilitySystemComponent)
	{
		for (FUrathaGiftBindInformation giftInfo : Gifts)
		{
			if (giftInfo.GiftClass)
			{
				if (!HasThisGift(abilitySystemComponent, giftInfo.GiftClass))
				{
					abilitySystemComponent->GiveAbility(FGameplayAbilitySpec(giftInfo.GiftClass, 1, 0));
				}
			}
			abilitySystemComponent->InitAbilityActorInfo(abilitySystemComponent->GetOwnerActor(), abilitySystemComponent->GetOwnerActor());
		}
	}

	auto AllAbilities = abilitySystemComponent->GetActivatableAbilities();
	for (auto ability : AllAbilities)
	{
		UE_LOG(LogTemp, Warning, TEXT("Ability class: %s"), *ability.Ability->GetClass()->GetName());
	}
}

void UUrathaGiftCollection::GiveSingleGift(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaGift> giftToGive)
{
	if (abilitySystemComponent && giftToGive)
	{
		if (!HasThisGift(abilitySystemComponent, giftToGive))
		{
			abilitySystemComponent->GiveAbility(FGameplayAbilitySpec(giftToGive, 1, 0));
			abilitySystemComponent->InitAbilityActorInfo(abilitySystemComponent->GetOwnerActor(), abilitySystemComponent->GetOwnerActor());
		}
	}
}

bool UUrathaGiftCollection::HasThisGift(UAbilitySystemComponent* abilitySystemComponent, TSubclassOf<UUrathaGift> giftToGive)
{
	bool HasGift = false;

	TArray<FGameplayAbilitySpec> ActivatableGifts = abilitySystemComponent->GetActivatableAbilities();

	for (FGameplayAbilitySpec GiftSpec : ActivatableGifts)
	{
		if (GiftSpec.Ability->GetName().Contains(giftToGive->GetName()))
		{
			HasGift = true;
			break;
		}
	}

	return HasGift;
}

FUrathaGiftBindInformation UUrathaGiftCollection::GetGiftBindInformation(TSubclassOf<UUrathaGift> giftToFind)
{
	for (FUrathaGiftBindInformation giftInfo : Gifts)
	{
		if (giftInfo.GiftClass == giftToFind)
		{
			return giftInfo;
		}
	}

	return FUrathaGiftBindInformation();
}

void UUrathaGiftCollection::PrepareGiftSet()
{
	int AttackGifts = 0;
	int DefenseGifts = 0;
	int UtilityGifts = 0;

	for (auto giftBindInfo = Gifts.CreateIterator(); giftBindInfo; ++giftBindInfo)
	{
		if (!giftBindInfo->Unlocked)
		{
			giftBindInfo->UnlockedForPreview = false;
			giftBindInfo->NumberOfFullfiledRequirements = 0;
		}
		else
		{
			giftBindInfo->UnlockedForPreview = true;
			switch (giftBindInfo->GiftType)
			{
			case EUrathaGiftType::Attack:
			{
				AttackGifts++;
			}
				break;
			case EUrathaGiftType::Defense:
			{
				DefenseGifts++;
			}
				break;
			case EUrathaGiftType::Utility:
			{
				UtilityGifts++;
			}
				break;
			default:
				break;
			}
		}
	}

	AttackGiftsUnlocked = AttackGiftsUnlockedForPreview = AttackGifts;
	DefenseGiftsUnlocked = DefenseGiftsUnlockedForPreview = DefenseGifts;
	UtilityGiftsUnlocked = UtilityGiftsUnlockedForPreview = UtilityGifts;
}

void UUrathaGiftCollection::CheckForNumberOfUnlockedGifts(FGameplayAttribute renown, UUrathaTalentSet* talentSet, int& attackGiftsNumber, int& defenseGiftsNumber, int& utilityGiftsNumber)
{
	int AttackGifts = AttackGiftsUnlockedForPreview;
	int DefenseGifts = DefenseGiftsUnlockedForPreview;
	int UtilityGifts = UtilityGiftsUnlockedForPreview;

	///In order to be able to edit values in TArray of USTRUCTS, TArray iterator must be used since 
	///"regular" foreach uses copy constructor and it's working with copies - hence no value change where you need it.
	for (auto giftBindInfo = Gifts.CreateIterator(); giftBindInfo; ++giftBindInfo)
	{
		if (!giftBindInfo->Unlocked)
		{
			UE_LOG(LogTemp, Error, TEXT("Gift: %s"), *giftBindInfo->GetGiftName());
		

			giftBindInfo->CheckRequirements(talentSet);
			giftBindInfo->CheckLostRequirements(talentSet);

			UE_LOG(LogTemp, Warning, TEXT("ReqDif: %d"), giftBindInfo->RequirementDifference());

			if (giftBindInfo->RequirementDifference() == 0 && 
				!giftBindInfo->UnlockedForPreview)
			{
				giftBindInfo->UnlockedForPreview = true;

				switch (giftBindInfo->GiftType)
				{
				case EUrathaGiftType::Attack:
				{
					AttackGifts++;
				}
				break;
				case EUrathaGiftType::Defense:
				{
					DefenseGifts++;
				}
				break;
				case EUrathaGiftType::Utility:
				{
					UtilityGifts++;
				}
				break;
				default:
					break;
				}
			}
			///Work on this
			if (giftBindInfo->RequirementDifference() < 0 && 
				giftBindInfo->UnlockedForPreview)
			{
				giftBindInfo->UnlockedForPreview = false;

				switch (giftBindInfo->GiftType)
				{
				case EUrathaGiftType::Attack:
				{
					AttackGifts--;
				}
				break;
				case EUrathaGiftType::Defense:
				{
					DefenseGifts--;
				}
				break;
				case EUrathaGiftType::Utility:
				{
					UtilityGifts--;
				}
				break;
				default:
					break;
				}
			}
		}
	}

	attackGiftsNumber = AttackGiftsUnlockedForPreview = AttackGifts;
	defenseGiftsNumber = DefenseGiftsUnlockedForPreview = DefenseGifts;
	utilityGiftsNumber = UtilityGiftsUnlockedForPreview = UtilityGifts;
}

void UUrathaGiftCollection::CheckForNumberOfUnlockedGiftsOnStart(UUrathaTalentSet* talentSet, int& attackGiftsNumber, int& defenseGiftsNumber, int& utilityGiftsNumber)
{
	int AttackGifts = AttackGiftsUnlockedForPreview;
	int DefenseGifts = DefenseGiftsUnlockedForPreview;
	int UtilityGifts = UtilityGiftsUnlockedForPreview;

	///In order to be able to edit values in TArray of USTRUCTS, TArray iterator must be used since 
	///"regular" foreach uses copy constructor and it's working with copies - hence no value change where you need it.
	for (auto giftBindInfo = Gifts.CreateIterator(); giftBindInfo; ++giftBindInfo)
	{
		if (!giftBindInfo->Unlocked)
		{
			UE_LOG(LogTemp, Error, TEXT("Gift: %s"), *giftBindInfo->GetGiftName());


			giftBindInfo->CheckRequirementsOnStart(talentSet);
			//giftBindInfo->CheckLostRequirements(talentSet);

			UE_LOG(LogTemp, Warning, TEXT("ReqDif: %d"), giftBindInfo->RequirementDifference());

			if (giftBindInfo->RequirementDifference() == 0 &&
				!giftBindInfo->UnlockedForPreview)
			{
				giftBindInfo->UnlockedForPreview = true;
				giftBindInfo->Unlocked = true;

				switch (giftBindInfo->GiftType)
				{
				case EUrathaGiftType::Attack:
				{
					AttackGifts++;
				}
				break;
				case EUrathaGiftType::Defense:
				{
					DefenseGifts++;
				}
				break;
				case EUrathaGiftType::Utility:
				{
					UtilityGifts++;
				}
				break;
				default:
					break;
				}
			}
		}
	}

	attackGiftsNumber = AttackGiftsUnlockedForPreview = AttackGifts;
	defenseGiftsNumber = DefenseGiftsUnlockedForPreview = DefenseGifts;
	utilityGiftsNumber = UtilityGiftsUnlockedForPreview = UtilityGifts;
}
