// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterPassiveAbility.h"

UrathaCharacterPassiveAbility::UrathaCharacterPassiveAbility()
{
	UnlocksAtLevel = 1;
	PercentBonusRatePerLevel = 5;
}

UrathaCharacterPassiveAbility::UrathaCharacterPassiveAbility(int abilityID, FString abilityName) : UrathaCharacterAbility(abilityID, abilityName)
{
	UnlocksAtLevel = 1;
	PercentBonusRatePerLevel = 5;
}

UrathaCharacterPassiveAbility::UrathaCharacterPassiveAbility(int abilityID, FString abilityName, int unlocksAtLevel) : UrathaCharacterAbility(abilityID, abilityName)
{
	UnlocksAtLevel = unlocksAtLevel;
	PercentBonusRatePerLevel = 5;
}

UrathaCharacterPassiveAbility::~UrathaCharacterPassiveAbility()
{
}


//int UrathaCharacterPassiveAbility::CalculateTalentValueWithPassivePercentageBonus(int talentScore, int bonusRatePerLevel, int talentValue)
//{
//	if (talentScore < UnlocksAtLevel)
//	{
//		return talentValue;
//	}
//
//	float percentageBonus = ((talentScore - UnlocksAtLevel + 1) * bonusRatePerLevel) / 100;
//	return talentValue + (int)(talentValue * percentageBonus);
//}


int UrathaCharacterPassiveAbility::CalculatePassivePercentageBonus(int talentScore, int talentValue)
{
	if (talentScore < UnlocksAtLevel)
	{
		return 0;
	}

	return (int)(talentValue * ((talentScore - UnlocksAtLevel + 1) * PercentBonusRatePerLevel) / 100); //It will return only the bonus value (e.g. 7) that needs to be added
}

int UrathaCharacterPassiveAbility::CalculateTalentValueWithPassivePercentageBonus(int talentScore, int talentValue)
{
	if (talentScore < UnlocksAtLevel)
	{
		return talentValue;
	}
	// NewValue = OldValue + (OldValue * (CalculatedTalentLevel * PercentBonusRatePerLevel) / 100)
	// ? = 1 + (1 * (2 * 5) / 100)
	// ? = 1 + (1 * 0.1)
	// ? = 1 + 0.1
	// 1.1 <--- 2 levels of 5%

	// ? = 2 + (2 * (2 * 5) / 100)
	// ? = 2 + (2 * 0.1)
	// ? = 2 + 0.2
	// 2.2 <--- 2 levels of 5%
	return talentValue + (int)(talentValue * ((talentScore - UnlocksAtLevel + 1) * PercentBonusRatePerLevel) / 100);
}
