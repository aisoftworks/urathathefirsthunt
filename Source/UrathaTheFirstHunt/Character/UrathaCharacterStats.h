// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UrathaCharacterStats.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class URATHATHEFIRSTHUNT_API UUrathaCharacterStats : public UActorComponent
{
	GENERATED_BODY()

#pragma region Constants (used in logic)
	const int URATHA_POINTS_DIVIDER = 1000; //Gain 1 Uratha point (used to improve character) on every 1000 experience points (XP)
	const int PRIMAL_URGE_DIVIDER = 50; //Gain 1 Primal Urge point on every 50 spent Uratha Points
	const int ATTRIBUTE_BUY_COST = 4; //Convert to UPROPERTY?
	const int SKILL_BUY_COST = 2; //Convert to UPROPERTY?
	const int ATTRIBUTE_MIN_VALUE = 1;
	const int SKILL_MIN_VALUE = 0;
#pragma endregion

#pragma region Constants (used to avoid magic strings)
public:
	static const FString NameAttributeIntelligence;
	static const FString NameAttributeWits;
	static const FString NameAttributeResolve;
	static const FString NameAttributeStrength;
	static const FString NameAttributeAgility;
	static const FString NameAttributePhysique;
	static const FString NameAttributePresence;
	static const FString NameAttributeManipulation;
	static const FString NameAttributeComposure;

	static const FString NameDerivedAttributeKnowledge;
	static const FString NameDerivedAttributeAthletics;
	static const FString NameDerivedAttributeEmpathy;

	static const FString NameSkillTechnology;
	static const FString NameSkillInvestigation;
	static const FString NameSkillScience;
	static const FString NameSkillBrawl;
	static const FString NameSkillStealth;
	static const FString NameSkillFirearms;
	static const FString NameSkillIntimidation;
	static const FString NameSkillPersuasion;
	static const FString NameSkillSubterfuge;
#pragma endregion

#pragma region Private properties

#pragma endregion
//Add BlueprintReadOnly?
#pragma region Character Attributes (UProperties)
protected:
	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Intelligence = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Wits = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Resolve = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Strength = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Agility = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Physique = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Presence = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Manipulation = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Composure = 1;
#pragma endregion

#pragma region Character Derived Attributes (UProperties)
	///Knowledge = (Intelligence + Wits + Resolve) / 3
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Derived Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Knowledge = 1;

	///Athletics = (Strength + Agility + Physique) / 3
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Derived Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Athletics = 1;

	///Empathy = (Presence + Manipulation + Composure) / 3
	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Derived Attributes", meta = (UIMin = "1", UIMax = "5"))
	int Empathy = 1;
#pragma endregion

#pragma region Character Skills (UProperties)
	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Technology = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Investigation = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Science = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Brawl = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Stealth = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Firearms = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Intimidation = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Persuasion = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Skills", meta = (UIMin = "0", UIMax = "5"))
	int Subterfuge = 0;
#pragma endregion

#pragma region Character Base Stats (UProperties)
	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Stats")
	float BaseHealth = 200.f;

	UPROPERTY(VisibleAnywhere, Category = "Uratha Character Stats")
	float Health = 200.f;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Stats")
	int PrimalUrge = 1;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Stats")
	int ExperiencePoints = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Stats")
	int UrathaPoints = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Uratha Character Stats")
	int SpentUrathaPoints = 0;

#pragma endregion

#pragma region Class default methods
public:
	// Sets default values for this component's properties
	UUrathaCharacterStats();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
#pragma endregion

#pragma region Private methods
private:
	void CalculateDerivedAttributes();

	void CalculateHealth(float bonus);

	void CalculatePrimalUrge();
#pragma endregion		

#pragma region Public methods
public:
	void AddExperiencePoints(int amount);
	void SetStatValue(FString statName, int newValue);
	void SetStatValue(FString statName, float newValue);
#pragma endregion

#pragma region Public getter methods
public:
	/*inline int GetAttributeIntelligence() { return Intelligence; }
	inline int GetAttributeWits() { return Wits; }
	inline int GetAttributeResolve() { return Resolve; }
	inline int GetAttributeStrength() { return Strength; }
	inline int GetAttributeAgility() { return Agility; }
	inline int GetAttributePhysique() { return Physique; }
	inline int GetAttributePresence() { return Presence; }
	inline int GetAttributeManipulation() { return Manipulation; }
	inline int GetAttributeComposure() { return Composure; }

	inline int GetDerivedAttributeKnowledge() { return Knowledge; }
	inline int GetDerivedAttributeAthletics() { return Athletics; }
	inline int GetDerivedAttributeEmpathy() { return Empathy; }

	inline int GetSkillTechnology() { return Technology; }
	inline int GetSkillInvestigation() { return Investigation; }
	inline int GetSkillScience() { return Science; }
	inline int GetSkillBrawl() { return Brawl; }
	inline int GetSkillStealth() { return Stealth; }
	inline int GetSkillFirearms() { return Firearms; }
	inline int GetSkillIntimidation() { return Intimidation; }
	inline int GetSkillPersuasion() { return Persuasion; }
	inline int GetSkillSubterfure() { return Subterfuge; }*/

	inline float GetStatHealth() { return Health; }
	inline int GetStatPrimalUrge() { return PrimalUrge; }

	//Reflection getters

	/**
	* Used for getting Uratha character attribute values.
	* @param attributeName	PLEASE USE UUrathaCharacterStats::NameAtributeName
	*/
	int GetAttribute(FString attributeName);
	/**
	* Used for getting Uratha character derived attribute values.
	* @param attributeName	PLEASE USE UUrathaCharacterStats::NameDerivedAttributeName
	*/
	int GetDerivedAttribute(FString derivedAttributeName);
	/**
	* Used for getting Uratha character skill values.
	* @param attributeName	PLEASE USE UUrathaCharacterStats::NameSkillName
	*/
	int GetSkill(FString skillName);
#pragma endregion
	
};
