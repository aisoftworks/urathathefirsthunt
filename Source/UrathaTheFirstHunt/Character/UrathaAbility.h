// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
//#include "Character/Utilities/UrathaAbilityUtilities.h"
#include "UObject/EnumProperty.h"
#include "UrathaAbility.generated.h"

class UMaterialInstance;
class UTexture2D;

UENUM(BlueprintType)
enum class EUrathaAbilityTalentTree : uint8
{
	General = 0 UMETA(DisplayName = "General"),
	TechSavviness = 1 UMETA(DisplayName = "TechSavviness"),
	Sense = 2 UMETA(DisplayName = "Sense"),
	Medicine = 3 UMETA(DisplayName = "Medicine"),
	Unarmed = 4 UMETA(DisplayName = "Unarmed"),
	SneakAndLarceny = 5 UMETA(DisplayName = "SneakAndLarceny"),
	Gunslinging = 6 UMETA(DisplayName = "Gunslinging"),
	FrightfulPresence = 7 UMETA(DisplayName = "FrightfulPresence"),
	SmoothTalking = 8 UMETA(DisplayName = "SmoothTalking"),
	Deception = 9 UMETA(DisplayName = "Deception")
};

UENUM(BlueprintType)
enum class EAbilityCostType : uint8
{
	Essence
};

USTRUCT(BlueprintType)
struct FUrathaAbilityInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	FString AbilityName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	FString AbilityDescription;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	float Cooldown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	float Cost;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	EAbilityCostType CostType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	UMaterialInstance* UIMaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
	UTexture2D* HUDIcon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
	UTexture2D* UIIcon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
	UTexture2D* UICharacterMenuIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
	TSubclassOf<UGameplayAbility> AbilityClass;

	FUrathaAbilityInfo();
	FUrathaAbilityInfo(FString name, FString description, float cooldown, float cost, EAbilityCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* hUDIcon, UTexture2D* uIIcon, UTexture2D* uICharacterMenuIcon, TSubclassOf<UGameplayAbility> abilityClass);
};

/*
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:
	UUrathaAbility();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	EUrathaAbilityTalentTree TalentTree;

	/**
	* Level at which talent level ability unlocks
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	int UnlocksAtTalentLevel;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	FString UIAbilityName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	FString UIAbilityDescription;

	/**
	* MaterialInstance used for UI
	*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	UMaterialInstance* UIMaterialInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	UTexture2D* HUDIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	UTexture2D* UIIcon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "UrathaAbility")
	UTexture2D* UICharacterMenuIcon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UrathaAbility")
	bool ActivateWithButtonHold;

	/**
	* Used to set ability info during call. It's a bit hacky, since I want to avoid setting this during the ability construction.
	*/
	UFUNCTION(BlueprintCallable, Category = "AbilityInfo")
	FUrathaAbilityInfo GetAbilityInfo();

	UFUNCTION(BlueprintPure, Category = "Tags")
	FGameplayTagContainer GetAbilityTags();

	FName GetAbilityTalentTreeName();
};
