// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class URATHATHEFIRSTHUNT_API UrathaCharacterAbility
{
protected:
	int AbilityID;
	FString AbilityName;


public:
	UrathaCharacterAbility();
	UrathaCharacterAbility(int abilityID, FString abilityName);
	~UrathaCharacterAbility();

	int GetAbilityID() { return AbilityID; }
	FString GetAbilityName() { return AbilityName; }

	//TODO:
	//Create a connection between logic if ability is unlocked (move Unlocked to Ability or not?)
};
