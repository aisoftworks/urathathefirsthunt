// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaAbility.h"

FUrathaAbilityInfo::FUrathaAbilityInfo()
{
	AbilityName = FString("Sample name");
	AbilityDescription = FString("Sample description");
	Cooldown = 0.f;
	Cost = 0.f;
	CostType = EAbilityCostType::Essence;
	UIMaterialInstance = nullptr;
	HUDIcon = nullptr;
	UIIcon = nullptr;
	UICharacterMenuIcon = nullptr;
	AbilityClass = nullptr;
}

FUrathaAbilityInfo::FUrathaAbilityInfo(FString name, FString description, float cooldown, float cost, EAbilityCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* hUDIcon, UTexture2D* uIIcon, UTexture2D* uICharacterMenuIcon, TSubclassOf<UGameplayAbility> abilityClass)
{
	AbilityName = name;
	AbilityDescription = description;
	Cooldown = cooldown;
	Cost = cost;
	CostType = costType;
	UIMaterialInstance = uiMaterialInstance;
	HUDIcon = hUDIcon;
	UIIcon = uIIcon;
	UICharacterMenuIcon = uICharacterMenuIcon;
	AbilityClass = abilityClass;
}

UUrathaAbility::UUrathaAbility()
{
	
}

FUrathaAbilityInfo UUrathaAbility::GetAbilityInfo()
{
	UGameplayEffect* CooldownEffect = GetCooldownGameplayEffect();
	UGameplayEffect* CostEffect = GetCostGameplayEffect();	

	if (CostEffect || CooldownEffect)
	{
		float Cooldown = 0.f;
		float Cost = 0.f;
		EAbilityCostType CostType = EAbilityCostType::Essence;

		if (CooldownEffect)
		{
			CooldownEffect->DurationMagnitude.GetStaticMagnitudeIfPossible(1, Cooldown);
		}
		if (CostEffect)
		{
			CostEffect->Modifiers[0].ModifierMagnitude.GetStaticMagnitudeIfPossible(1, Cost);
			FGameplayAttribute CostAttribute = CostEffect->Modifiers[0].Attribute;
			if (CostAttribute.AttributeName == "Essence")
			{
				CostType = EAbilityCostType::Essence;
			}
		}
		return FUrathaAbilityInfo(UIAbilityName, UIAbilityDescription, Cooldown, Cost, CostType, UIMaterialInstance, HUDIcon, UIIcon, UICharacterMenuIcon, GetClass());
	}
	
	return FUrathaAbilityInfo();
}

FGameplayTagContainer UUrathaAbility::GetAbilityTags()
{
	return AbilityTags;
}

FName UUrathaAbility::GetAbilityTalentTreeName()
{
	FName TalentTreeName;
	FText treeName;
	FEnumProperty* EnumPtr = FindFieldChecked<FEnumProperty>(UUrathaAbility::StaticClass(), FName("TalentTree"));
	if (EnumPtr)
	{
		if (EnumPtr->GetEnum())
		{
			treeName = EnumPtr->GetEnum()->GetDisplayNameTextByIndex(static_cast<uint8>(TalentTree));
			TalentTreeName = FName(*treeName.ToString());
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Ptr not found..."));
		TalentTreeName = FName();
	}

	UE_LOG(LogTemp, Warning, TEXT("TalentTree FNAME: %s"), *TalentTreeName.ToString());
	return TalentTreeName;
}
