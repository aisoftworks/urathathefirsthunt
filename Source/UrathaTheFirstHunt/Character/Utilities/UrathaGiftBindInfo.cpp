// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.


#include "UrathaGiftBindInfo.h"

UUrathaGiftBindInfo::UUrathaGiftBindInfo()
{
	GiftType = EUrathaGiftType::None;
	GiftClass = nullptr;
	GiftRequirements = TArray<FUrathaGiftRequirements>();
	Unlocked = false;
	UnlockedForPreview = false;
}

//UUrathaGiftBindInfo::UUrathaGiftBindInfo(const UUrathaGiftBindInfo& urathaGiftBindInfo)
//{
//	GiftType = urathaGiftBindInfo.GiftType;
//	GiftClass = urathaGiftBindInfo.GiftClass;
//	GiftRequirements = urathaGiftBindInfo.GiftRequirements;
//	Unlocked = urathaGiftBindInfo.Unlocked;
//	UnlockedForPreview = urathaGiftBindInfo.UnlockedForPreview;
//}
