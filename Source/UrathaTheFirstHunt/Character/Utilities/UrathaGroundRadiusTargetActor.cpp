// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaGroundRadiusTargetActor.h"
#include "GameplayAbility.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/PlayerController.h"
#include "../Runtime/Engine/Public/DrawDebugHelpers.h"

void AUrathaGroundRadiusTargetActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	FVector LookPoint;
	GetPlayerLookingPoint(LookPoint);
	DrawDebugSphere(GetWorld(), LookPoint, TargetRadius, 32, FColor::Red, false, -1.f, 0, 5.f);
}

AUrathaGroundRadiusTargetActor::AUrathaGroundRadiusTargetActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AUrathaGroundRadiusTargetActor::StartTargeting(UGameplayAbility* Ability)
{
	Super::StartTargeting(Ability);
	MasterPC = Cast<APlayerController>(Ability->GetOwningActorFromActorInfo()->GetInstigatorController());
}

void AUrathaGroundRadiusTargetActor::ConfirmTargetingAndContinue()
{
	FVector ViewPoint;
	GetPlayerLookingPoint(ViewPoint);
	
	TArray<FOverlapResult> OverlapResults;
	TArray<TWeakObjectPtr<AActor>> OverlappedActors;

	FCollisionQueryParams CollisionParams;
	CollisionParams.bTraceComplex = false;
	CollisionParams.bReturnPhysicalMaterial = false;
	APawn* MasterPawn = MasterPC->GetPawn();
	CollisionParams.AddIgnoredActor(MasterPawn->GetUniqueID());

	bool TryOverlap = GetWorld()->OverlapMultiByObjectType(
		OverlapResults, 
		ViewPoint, 
		FQuat::Identity, 
		FCollisionObjectQueryParams(ECC_Pawn), 
		FCollisionShape::MakeSphere(TargetRadius), 
		CollisionParams);

	if (TryOverlap)
	{
		for (int overlapsIndex = 0; overlapsIndex < OverlapResults.Num(); overlapsIndex++)
		{
			APawn* OverlappedPawn = Cast<APawn>(OverlapResults[overlapsIndex].GetActor());
			if (OverlappedPawn && !OverlappedActors.Contains(OverlappedPawn))
			{
				OverlappedActors.Add(OverlappedPawn);
			}
		}
	}

	if (OverlappedActors.Num() > 0)
	{
		FGameplayAbilityTargetDataHandle TargetData = StartLocation.MakeTargetDataHandleFromActors(OverlappedActors);
		TargetDataReadyDelegate.Broadcast(TargetData);
	}
	else
	{
		TargetDataReadyDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
	}
}

bool AUrathaGroundRadiusTargetActor::GetPlayerLookingPoint(FVector& lookingPoint)
{
	FVector ViewLocation;
	FRotator ViewRotation;
	MasterPC->GetPlayerViewPoint(ViewLocation, ViewRotation);
	FHitResult HitResult;
	FCollisionQueryParams CollisionParams;

	CollisionParams.bTraceComplex = false;
	APawn* MasterPawn = MasterPC->GetPawn();
	if (MasterPawn)
	{
		CollisionParams.AddIgnoredActor(MasterPawn->GetUniqueID());
	}

	bool TryTrace = GetWorld()->LineTraceSingleByChannel(
		HitResult, 
		ViewLocation, 
		ViewLocation + ViewRotation.Vector() * WeaponRange, 
		ECC_Visibility, 
		CollisionParams);

	if (TryTrace)
	{
		lookingPoint = HitResult.ImpactPoint;
	}
	else
	{
		lookingPoint = FVector();
	}

	return TryTrace;
}
