// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaMagnitudeCalculation.h"
#include "Character/UrathaTalentSet.h"
#include "../AttributeSet.h"


//float UUrathaMagnitudeCalculation::CalculateBaseMagnitude(const FGameplayEffectSpec& Spec) const
//{
//	UE_LOG(LogTemp, Warning, TEXT("Log message"));UE_LOG(LogTemp, Warning, TEXT("Log message"));
//	return -20.f;
//}

float UUrathaMagnitudeCalculation::GetMagnitudePerAttribute(FGameplayEffectSpec spec, const FGameplayEffectAttributeCaptureDefinition captureAttribute) const
{
	UE_LOG(LogTemp, Warning, TEXT("Getting attribute value..."));
	/*const UStructProperty* StructProperty = const_cast<UStructProperty*>(CastChecked<UStructProperty>(captureAttribute.AttributeToCapture.GetUProperty()));
	check(StructProperty);
	const FGameplayAttributeData* DataPtr = StructProperty->ContainerPtrToValuePtr<FGameplayAttributeData>(captureAttribute.AttributeToCapture.GetUProperty()->GetOuter());
	if (ensure(DataPtr))
	{
		return DataPtr->GetCurrentValue();
	}*/

	auto talentSet = spec.GetEffectContext().GetInstigator()->GetDefaultSubobjectByName("AttributeTalentSetComponent");

	if (((UUrathaTalentSet*)talentSet)->GetAttribute(*captureAttribute.AttributeToCapture.AttributeName)->GetCurrentValue() > -1.f)
	{
		return ((UUrathaTalentSet*)talentSet)->GetAttribute(*captureAttribute.AttributeToCapture.AttributeName)->GetCurrentValue();
	}

	return 0.f;

	//return captureAttribute.AttributeToCapture.GetNumericValueChecked(const_cast<UUrathaTalentSet*>(CastChecked<UUrathaTalentSet>(captureAttribute.AttributeToCapture.GetAttributeSetClass())));
}

float UUrathaMagnitudeCalculation::GetMagnitudePerAttributeCommon(FGameplayEffectAttributeCaptureDefinition captureAttribute)
{
	return captureAttribute.AttributeToCapture.GetNumericValueChecked(CastChecked<UUrathaTalentSet>(captureAttribute.AttributeToCapture.GetAttributeSetClass()));
}

FString UUrathaMagnitudeCalculation::GetDiffrenetNames(FGameplayEffectSpec spec, FGameplayEffectAttributeCaptureDefinition captureAttribute) const
{
	//return captureAttribute.AttributeToCapture.GetUProperty()->GetOuter()->GetFullName();
	//return captureAttribute.AttributeToCapture.GetAttributeSetClass()->GetFullName();
	//return UUrathaTalentSet::StaticClass()->GetFullName();
	
	spec.AddModifiedAttribute(captureAttribute.AttributeToCapture);
	auto talentSet = spec.GetEffectContext().GetInstigator()->GetDefaultSubobjectByName("AttributeTalentSetComponent");
	
	if (((UUrathaTalentSet*)talentSet)->GetAttribute(*captureAttribute.AttributeToCapture.AttributeName)->GetCurrentValue() > -1.f)
	{
		return FString("SUCCESS!");
	}
	return FString("Fail");

	/*if (Cast<UObject>(captureAttribute.AttributeToCapture.GetAttributeSetClass()))
	{
		if (Cast<UUrathaTalentSet>(Cast<UObject>(captureAttribute.AttributeToCapture.GetAttributeSetClass())))
		{
			return FString("SUCCESS!");
		}
		return Cast<UObject>(captureAttribute.AttributeToCapture.GetAttributeSetClass())->GetFullName();
	}
	return FString("Fail");*/
}
