// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "Character/UrathaAbility.h"
//#include "UrathaAbilityUtilities.generated.h"

//class UMaterialInstance;
//class UTexture2D;
//
//UENUM(BlueprintType)
//enum class EGiftCostType : uint8
//{
//	Essence
//};
//
//USTRUCT(BlueprintType)
//struct FUrathaAbilityInfo
//{
//	GENERATED_BODY()
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	float Cooldown;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	float Cost;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	EGiftCostType CostType;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	UMaterialInstance* UIMaterialInstance;
//
//	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
//	UTexture2D* UIIcon;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	TSubclassOf<UUrathaAbility> AbilityClass;
//
//	FUrathaAbilityInfo();
//	FUrathaAbilityInfo(float cooldown, float cost, EGiftCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* uIIcon, TSubclassOf<UUrathaAbility> abilityClass);
//};
