// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Character/UrathaGift.h"
#include "UrathaGiftBindInfo.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class URATHATHEFIRSTHUNT_API UUrathaGiftBindInfo : public UObject
{
	GENERATED_BODY()

public:
	UUrathaGiftBindInfo();

	//UUrathaGiftBindInfo(const UUrathaGiftBindInfo& urathaGiftBindInfo);

	UPROPERTY(EditAnywhere, Category = "Gifts")
	EUrathaGiftType GiftType;

	UPROPERTY(EditAnywhere, Category = "Gifts")
	TSubclassOf<UUrathaGift> GiftClass;

	UPROPERTY(EditAnywhere, Category = "Gifts")
	TArray<FUrathaGiftRequirements> GiftRequirements;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	bool Unlocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gifts")
	bool UnlockedForPreview;
};
