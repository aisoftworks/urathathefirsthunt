// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaEffectExecCalculation.h"
#include "../AbilitySystemComponent.h"

UUrathaEffectExecCalculation::UUrathaEffectExecCalculation(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	RelevantAttributesToCapture.Add(capturedAttributes.HealthDef);
	RelevantAttributesToCapture.Add(capturedAttributes.MaxHealthDef);
	RelevantAttributesToCapture.Add(capturedAttributes.BashingDef);
	RelevantAttributesToCapture.Add(capturedAttributes.LethalDef);
	RelevantAttributesToCapture.Add(capturedAttributes.AggravatedDef);
	RelevantAttributesToCapture.Add(capturedAttributes.MentalDef);
	RelevantAttributesToCapture.Add(capturedAttributes.UnarmedDef);
	RelevantAttributesToCapture.Add(capturedAttributes.GunslingingDef);

	InitializeCalculateFunctions();
}

void UUrathaEffectExecCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	///Get ability system components.
	UAbilitySystemComponent* SourceAbilitySystemComponent = ExecutionParams.GetSourceAbilitySystemComponent();
	UAbilitySystemComponent* TargetAbilitySystemComponent = ExecutionParams.GetTargetAbilitySystemComponent();

	///Get target/source Actors.
	AActor* SourceActor = SourceAbilitySystemComponent->GetOwnerActor();
	AActor* TargetActor = TargetAbilitySystemComponent->GetOwnerActor();

	///Get Gameplay Effect spec.
	const FGameplayEffectSpec EffectSpec = ExecutionParams.GetOwningSpec();

	///Get Gameplay Effect tags.
	const FGameplayTagContainer* SourceTags = EffectSpec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = EffectSpec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	FString AbilityName = FString();
	TArray<FString> TagArray;
	FString AbilityTagName = FString();
	
	//Check if gameplay talent is used for attack, and its name (gunslinging, unarmed, etc.).
	for (int tagIndex = 0; tagIndex < SourceTags->Num(); tagIndex++)
	{
		FString tagName = SourceTags->GetByIndex(tagIndex).GetTagName().GetPlainNameString();
		if (tagName.Contains("attack"))
		{
			AbilityTagName = tagName;
			break;
		}
	}

	//If gameplay talent is used for attack, and its name is in TMap, use talent name to call appropriate function.
	if (AbilityTagName.Len() > 0)
	{
		AbilityTagName.ParseIntoArray(TagArray, TEXT("."));
		for (int arrayIndex = 0; arrayIndex < TagArray.Num(); arrayIndex++)
		{
			if (FunctionMap.Contains(TagArray[arrayIndex]))
			{
				AbilityName = TagArray[arrayIndex];
				break;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATTACK TAG MISSING FROM ABILITY TAGS!"));
	}
	
	float DamageDone = 0.f;

	//If ability name is valid, call appropriate function by name and function pointer.
	if (AbilityName.Len() > 0)
	{
		CalculateDamage(AbilityName, ExecutionParams, EvaluationParameters, DamageDone);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ABILITY NAME NOT FOUND!"));
	}

	if (DamageDone > 0.f)
	{
		UE_LOG(LogTemp, Warning, TEXT("CUSTOM EXECUTING DAMAGE!"));
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(capturedAttributes.HealthProperty, EGameplayModOp::Additive, -DamageDone));
		OutExecutionOutput.MarkConditionalGameplayEffectsToTrigger();
	}
}

void UUrathaEffectExecCalculation::InitializeCalculateFunctions()
{
	///Add talent names to TMap.
	FunctionMap.Add(FString("gunslinging"), 0);
	FunctionMap.Add(FString("unarmed"), 1);

	///Assign custom type functions to be called based on talent index.
	CalculationFunctionsVoid[0] = &UUrathaEffectExecCalculation::CalculateDamageFromGunslinging;
	CalculationFunctionsVoid[1] = &UUrathaEffectExecCalculation::CalculateDamageFromUnarmed;
}

void UUrathaEffectExecCalculation::CalculateDamageFromGunslinging(const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const
{
	float Damage = 0.f;
	
	float Gunslinging = 0.f;
	executionParams.AttemptCalculateCapturedAttributeMagnitude(capturedAttributes.GunslingingDef, evaluationParameters, Gunslinging);

	float LethalSoak = 0.f;
	executionParams.AttemptCalculateCapturedAttributeMagnitude(capturedAttributes.LethalDef, evaluationParameters, LethalSoak);

	//Subtract 1 because gameplay effect adds 1 in order for custom calculation to work.
	Gunslinging--;

	UE_LOG(LogTemp, Warning, TEXT("Source talent: %f"), Gunslinging);
	UE_LOG(LogTemp, Warning, TEXT("Target soak: %f"), LethalSoak);

	Damage = Gunslinging * DamageModifier - LethalSoak * SoakModifier;

	damageDone = Damage;

	UE_LOG(LogTemp, Warning, TEXT("Calculated gunslinging damage: %f"), damageDone);
}

void UUrathaEffectExecCalculation::CalculateDamageFromUnarmed(const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const
{
	float Damage = 0.f;

	float Unarmed = 0.f;
	executionParams.AttemptCalculateCapturedAttributeMagnitude(capturedAttributes.UnarmedDef, evaluationParameters, Unarmed);

	float BashingSoak = 0.f;
	executionParams.AttemptCalculateCapturedAttributeMagnitude(capturedAttributes.BashingDef, evaluationParameters, BashingSoak);

	//Subtract 1 because gameplay effect adds 1 in order for custom calculation to work.
	Unarmed--;

	UE_LOG(LogTemp, Warning, TEXT("Source talent: %f"), Unarmed);
	UE_LOG(LogTemp, Warning, TEXT("Target soak: %f"), BashingSoak);

	Damage = Unarmed * DamageModifier - BashingSoak * SoakModifier;

	damageDone = Damage;

	UE_LOG(LogTemp, Warning, TEXT("Calculated unarmed damage: %f"), damageDone);
}

void UUrathaEffectExecCalculation::CalculateDamage(FString talentTag, const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const
{
	uint8 FunctionIndex = FunctionMap.FindChecked(talentTag);

	(this->* (CalculationFunctionsVoid[FunctionIndex]))(executionParams, evaluationParameters, damageDone);
}
