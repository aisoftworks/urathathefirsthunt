// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "UrathaTheFirstHunt/Character/UrathaTalentSet.h"
#include "UrathaEffectExecCalculation.generated.h"

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FUrathaCapturedAttributes
{
	GENERATED_BODY()

	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxHealth);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Bashing);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Lethal);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Aggravated);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Mental);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Unarmed);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Gunslinging);

	FUrathaCapturedAttributes()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Health, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, MaxHealth, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Bashing, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Lethal, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Aggravated, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Mental, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Unarmed, Source, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UUrathaTalentSet, Gunslinging, Source, false);
	}
};
/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaEffectExecCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_UCLASS_BODY()


	//Captured attributes in the time of ability execution.
	FUrathaCapturedAttributes capturedAttributes;

	//Damage modifier used to calculate damage and soak.
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float DamageModifier = 10.f;

	//Damage modifier used to calculate damage and soak.
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float SoakModifier = 5.f;

	//Implementation of Execute method. This will calculate damage and subtract it from Health attribute.
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

private:
	//TMap used for gameplay talent names (gunslinging, unarmed, etc.).
	TMap<FString, uint8> FunctionMap;

	//Custom type for damage calculation classes (gunslinging, unarmed, etc.).
	typedef void (UUrathaEffectExecCalculation::*FunctionPtrTypeVoid)(const FGameplayEffectCustomExecutionParameters&, FAggregatorEvaluateParameters&, float&) const;
	
	//Array of function pointers. Used to call functions based on talent type (gunslinging, unarmed, etc.).
	FunctionPtrTypeVoid CalculationFunctionsVoid[3];

	void InitializeCalculateFunctions();

	void CalculateDamageFromGunslinging(const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const;
	void CalculateDamageFromUnarmed(const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const;

	void CalculateDamage(FString talentTag, const FGameplayEffectCustomExecutionParameters& executionParams, FAggregatorEvaluateParameters& evaluationParameters, OUT float& damageDone) const;
};
