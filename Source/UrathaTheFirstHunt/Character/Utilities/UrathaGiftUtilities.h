// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "Character/UrathaGift.h"
//#include "UrathaGiftUtilities.generated.h"

//class UMaterialInstance;
//class UTexture2D;
//
//UENUM(BlueprintType)
//enum class EAbilityCostType : uint8
//{
//	Essence
//};
//
//USTRUCT(BlueprintType)
//struct FUrathaGiftInfo
//{
//	GENERATED_BODY()
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	float Cooldown;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	float Cost;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	EAbilityCostType CostType;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	UMaterialInstance* UIMaterialInstance;
//
//	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
//	UTexture2D* UIIcon;
//
//	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AbilityInfo")
//	UTexture2D* QuickAccessUIIcon;
//
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AbilityInfo")
//	TSubclassOf<UUrathaGift> AbilityClass;
//
//	FUrathaGiftInfo();
//	FUrathaGiftInfo(float cooldown, float cost, EAbilityCostType costType, UMaterialInstance* uiMaterialInstance, UTexture2D* uIIcon, UTexture2D* quickAccessUIIcon, TSubclassOf<UUrathaGift> abilityClass);
//};

