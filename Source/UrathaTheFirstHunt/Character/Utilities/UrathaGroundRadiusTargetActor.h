// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbilityTargetActor.h"
#include "UrathaGroundRadiusTargetActor.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaGroundRadiusTargetActor : public AGameplayAbilityTargetActor
{
	GENERATED_BODY()
	
protected:
	virtual void Tick(float DeltaSeconds) override;

public:
	AUrathaGroundRadiusTargetActor();

	virtual void StartTargeting(UGameplayAbility* Ability) override;
	virtual void ConfirmTargetingAndContinue() override;
	
	UFUNCTION(BlueprintCallable, Category = "GroundRadiusTarget")
	bool GetPlayerLookingPoint(FVector& lookingPoint);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = true), Category = "GroundRadiusTarget")
	float TargetRadius;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ExposeOnSpawn = true), Category = "GroundRadiusTarget")
	float WeaponRange;
};
