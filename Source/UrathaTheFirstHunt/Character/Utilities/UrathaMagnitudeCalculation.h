// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
//#include "../UrathaTalentSet.h"
#include "GameplayModMagnitudeCalculation.h"
#include "UrathaMagnitudeCalculation.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API UUrathaMagnitudeCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	//float CalculateBaseMagnitude(const FGameplayEffectSpec& Spec) const;

	UFUNCTION(BlueprintCallable, Category = "Calculation")
	float GetMagnitudePerAttribute(FGameplayEffectSpec spec, const FGameplayEffectAttributeCaptureDefinition captureAttribute) const;

	UFUNCTION(BlueprintCallable, Category = "Calculation")
	static float GetMagnitudePerAttributeCommon(FGameplayEffectAttributeCaptureDefinition captureAttribute);

	UFUNCTION(BlueprintCallable, Category = "Calculation")
	FString GetDiffrenetNames(FGameplayEffectSpec spec, FGameplayEffectAttributeCaptureDefinition captureAttribute) const;
};
