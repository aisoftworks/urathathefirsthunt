// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaCharacterActiveAbility.h"

UrathaCharacterActiveAbility::UrathaCharacterActiveAbility()
{
	UnlocksAtLevel = 1;
}

UrathaCharacterActiveAbility::UrathaCharacterActiveAbility(int abilityID, FString abilityName) : UrathaCharacterAbility(abilityID, abilityName)
{
	UnlocksAtLevel = 1;
}

UrathaCharacterActiveAbility::UrathaCharacterActiveAbility(int abilityID, FString abilityName, int unlocksAtLevel) : UrathaCharacterAbility(abilityID, abilityName)
{
	UnlocksAtLevel = unlocksAtLevel;
}

UrathaCharacterActiveAbility::~UrathaCharacterActiveAbility()
{
}

void UrathaCharacterActiveAbility::ExecuteActiveAbility(int talentScore, int talentValue)
{
	UE_LOG(LogTemp, Warning, TEXT("ABILITY %s EXECUTED!"), *AbilityName);
}
