// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/UrathaAttributeSet.h"
#include "AttributeSet.h"
#include "../Plugins/Runtime/GameplayAbilities/Source/GameplayAbilities/Public/AttributeSet.h"
#include "UrathaTalentSet.generated.h"

UENUM(BlueprintType)
enum class ETalent : uint8
{
	AnyGift UMETA(DisplayName = "Gift - handle later"),
	TechSavviness UMETA(DisplayName = "Tech Savviness"),
	Sense UMETA(DisplayName = "Sense"),
	Medicine UMETA(DisplayName = "Medicine"),
	Unarmed UMETA(DisplayName = "Unarmed"),
	SneakAndLarceny UMETA(DisplayName = "Sneak & Larceny"),
	Gunslinging UMETA(DisplayName = "Gunslinging"),
	FrightfulPresence UMETA(DisplayName = "Frightful Presence"),
	SmoothTalking UMETA(DisplayName = "Smooth Talking"),
	Deception UMETA(DisplayName = "Deception")
};

USTRUCT(BlueprintType)
struct URATHATHEFIRSTHUNT_API FUrathaTalentAttributeData : public FGameplayAttributeData
{
	GENERATED_BODY()
	
	FUrathaTalentAttributeData()
	{
		FirstTalentAttribute = FGameplayAttributeData(0.f);
		SecondTalentAttribute = FGameplayAttributeData(0.f);

		BaseValue = (FirstTalentAttribute.GetBaseValue() + SecondTalentAttribute.GetBaseValue());
		CurrentValue = (FirstTalentAttribute.GetCurrentValue() + SecondTalentAttribute.GetCurrentValue());
	}

	FUrathaTalentAttributeData(float defaultValue) : 
		Super(defaultValue)
	{

	}

	FUrathaTalentAttributeData(const FGameplayAttributeData& firstAttibute, const FGameplayAttributeData& secondAttibute)
	{
		FirstTalentAttribute = FGameplayAttributeData(firstAttibute);
		SecondTalentAttribute = FGameplayAttributeData(secondAttibute);

		BaseValue = (FirstTalentAttribute.GetBaseValue() + SecondTalentAttribute.GetBaseValue());
		CurrentValue = (FirstTalentAttribute.GetCurrentValue() + SecondTalentAttribute.GetCurrentValue());
	}

	FUrathaTalentAttributeData(const FGameplayAttributeData& firstAttibute, const FGameplayAttributeData& secondAttibute, FName firstAttributeName, FName secondAttributeName)
	{
		FirstTalentAttribute = FGameplayAttributeData(firstAttibute);
		SecondTalentAttribute = FGameplayAttributeData(secondAttibute);

		FirstTalentAttributeName = firstAttributeName;
		SecondTalentAttributeName = secondAttributeName;

		BaseValue = (FirstTalentAttribute.GetBaseValue() + SecondTalentAttribute.GetBaseValue());
		CurrentValue = (FirstTalentAttribute.GetCurrentValue() + SecondTalentAttribute.GetCurrentValue());
	}

	FUrathaTalentAttributeData(FGameplayAttributeData firstAttibute, FGameplayAttributeData secondAttibute, int talentModifier)
	{
		FirstTalentAttribute = FGameplayAttributeData(firstAttibute);
		SecondTalentAttribute = FGameplayAttributeData(secondAttibute);

		BaseValue = (FirstTalentAttribute.GetBaseValue() + SecondTalentAttribute.GetBaseValue()) * talentModifier;
		CurrentValue = (FirstTalentAttribute.GetCurrentValue() + SecondTalentAttribute.GetCurrentValue()) * talentModifier;

		UE_LOG(LogTemp, Warning, TEXT("Calculated soak: %f"), CurrentValue);
	}

protected:
	UPROPERTY(BlueprintReadOnly, Category = "Attribute")
	FGameplayAttributeData FirstTalentAttribute;

	UPROPERTY(BlueprintReadOnly, Category = "Attribute")
	FGameplayAttributeData SecondTalentAttribute;

	FName FirstTalentAttributeName;
	FName SecondTalentAttributeName;

	bool AppliedBonus = false;
	bool AppliedPenalty = false;

	FString TalentUIName;

public:
	void CalculateTalentValue(FGameplayAttributeData* firstAttribute, FGameplayAttributeData* secondAttribute);
	void CalculateTalentValue(FGameplayAttributeData* firstAttribute);

	FName GetFirstAttributeName()
	{
		return FirstTalentAttributeName;
	};

	FName GetSecondAttributeName()
	{
		return SecondTalentAttributeName;
	};

	bool HasBonus()
	{
		return AppliedBonus;
	};

	bool HasPenalty()
	{
		return AppliedPenalty;
	};

	//Apply bonus to talent. Write method for removing the bonus.
	void ApplyBonus(int bonusValue)
	{
		SetBaseValue(GetBaseValue() + bonusValue);
		SetCurrentValue(GetCurrentValue() + bonusValue);

		AppliedBonus = true;
	};

	//Apply penalty to talent. Write method for removing the penalty.
	void ApplyPenalty(int bonusValue)
	{
		SetBaseValue(GetBaseValue() - bonusValue);
		SetCurrentValue(GetCurrentValue() - bonusValue);

		AppliedPenalty = true;
	};
};

/**
 * 
 */
UCLASS(Blueprintable)
class URATHATHEFIRSTHUNT_API UUrathaTalentSet : public UUrathaAttributeSet
{
	GENERATED_BODY()

public:
	UUrathaTalentSet();

	void PreAttributeChange(const FGameplayAttribute& Attribute, float& NewValue) override;
	
private:
#pragma region Logic Constants
	const int TALENT_COMBAT_VALUE_MODIFIER = 10;
	const int TALENT_NORMAL_VALUE_MODIFIER = 1;
#pragma endregion

public:
#pragma region Character Soak Talents (UProperties)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Bashing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Lethal;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Aggravated;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Mental;
#pragma endregion

#pragma region Character Talents (UProperties)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData TechSavviness;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Sense;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Medicine;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Unarmed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData SneakAndLarceny;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Gunslinging;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData FrightfulPresence;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData SmoothTalking;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UrathaTalentSet")
	FUrathaTalentAttributeData Deception;
#pragma endregion

	//TODO: Implement USTRUCT for talents. Use AbilitySet to assign abilities to talents.

public:
	FUrathaTalentAttributeData* GetTalent(FName talentName);

	UFUNCTION()
	void OnAttributeChanged(FName AttributeName, float CurrentValue, float MaxValue);

	void CalculateTalents();

	void ApplyBonusesAndPenalties();

	bool TalentHasBonus(FGameplayAttribute talent);

	bool TalentHasPenalty(FGameplayAttribute talent);
};
