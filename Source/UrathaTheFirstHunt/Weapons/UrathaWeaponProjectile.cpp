// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaWeaponProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
AUrathaWeaponProjectile::AUrathaWeaponProjectile(const FObjectInitializer& OI) : Super(OI)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ProjectileRoot = OI.CreateDefaultSubobject<UCapsuleComponent>(this, FName("ProjectileRoot"));
	SetRootComponent(ProjectileRoot);
	ProjectileRoot->SetCapsuleRadius(2.f);
	ProjectileRoot->SetCapsuleHalfHeight(5.f);
	
	ProjectileMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(FName("ProjectileMeshComponent"));
	ProjectileMeshComponent->SetupAttachment(ProjectileRoot);

	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ProjectileMovementComponent->UpdatedComponent = ProjectileRoot;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->bShouldBounce = true;
	

	// Die after 5 seconds by default
	InitialLifeSpan = 5.0f;
}

// Called when the game starts or when spawned
void AUrathaWeaponProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AUrathaWeaponProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UProjectileMovementComponent* AUrathaWeaponProjectile::GetProjectileMovementComponent()
{
	return ProjectileMovementComponent;
}

