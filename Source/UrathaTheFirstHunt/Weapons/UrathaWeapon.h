// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Utilities/MultipleTargetCollection.h"
#include "UrathaWeapon.generated.h"

UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaWeapon : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	UBoxComponent* WeaponRoot;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TSubclassOf<class AUrathaWeaponProjectile> WeaponProjectileBlueprint;

	UMultipleTargetCollection* MultipleTargetCollection;
	
public:	
	// Sets default values for this actor's properties
	//It is important to use ObjectInitializer version, since RootComponent attachment can get buggy when trying to set it up in the constructor.
	AUrathaWeapon(const FObjectInitializer& OI);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UStaticMeshComponent* WeaponMeshComponent;

	//Weapon's range in CENTIMETERS
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float WeaponRange = 2000.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure)
	UStaticMeshComponent* GetWeaponMesh();

	UFUNCTION(BlueprintPure)
	UMultipleTargetCollection* GetTargetCollection();

	UFUNCTION(BlueprintCallable)
	void FireWeapon(FVector VelocityVector);

	//Attacks with equipped weapon. Able to fire 1 or more bullets, and store them in AActor array.
	UFUNCTION(BlueprintCallable, Category = "UrathaWeapon")
	bool WeaponAttack(class AUrathaCharacter* character, int bulletsPerAttack = 1, float weaponFiringDistance = 0.f, float firingDeviationAngle = 0.f, float firingAimDeviationAngle = 0.f);
};
