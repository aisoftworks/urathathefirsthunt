// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "MultipleTargetCollection.h"

UMultipleTargetCollection::UMultipleTargetCollection()
{
	TargetCollection = TArray<AActor*>();
	WeakTargetCollection = TArray<TWeakObjectPtr<AActor>>();
}

TArray<AActor*>  UMultipleTargetCollection::GetCollection()
{
	return TargetCollection;
}

void UMultipleTargetCollection::AddTarget(AActor* targetActor)
{
	if (ensure(targetActor) && IsValid(targetActor))
	{
		TargetCollection.Add(targetActor);
		TargetCollection.Shrink();
	}
}

int UMultipleTargetCollection::GetTargetCollectionLength()
{
	return TargetCollection.Num();
}

TArray<AActor*> UMultipleTargetCollection::GetTargetsValues()
{
	//Before this, collection was a TMap, so I had to transfer values to an TArray.
	return TargetCollection;
}

void UMultipleTargetCollection::ClearTargetCollection()
{
	TargetCollection.Empty(5);
}
