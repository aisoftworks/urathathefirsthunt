// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/Actor.h"
#include "MultipleTargetCollection.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class URATHATHEFIRSTHUNT_API UMultipleTargetCollection : public UObject
{
	GENERATED_BODY()
	
public:
	/*TMap<FString, AActor*> TargetCollection;*/
	UPROPERTY(VisibleAnywhere)
	TArray<AActor*> TargetCollection;

	UPROPERTY(VisibleAnywhere)
	TArray<TWeakObjectPtr<AActor>> WeakTargetCollection;

	UMultipleTargetCollection();

	UFUNCTION(BlueprintPure, Category = "MultipleTargetCollection")
	TArray<AActor*> GetCollection();
	/*TMap<FString, AActor*> GetCollection();*/

	UFUNCTION(BlueprintCallable, Category = "MultipleTargetCollection")
	void AddTarget(AActor* targetActor);

	UFUNCTION(BlueprintCallable, Category = "MultipleTargetCollection")
	int GetTargetCollectionLength();

	UFUNCTION(BlueprintCallable, Category = "MultipleTargetCollection")
	TArray<AActor*> GetTargetsValues();

	UFUNCTION(BlueprintCallable, Category = "MultipleTargetCollection")
	void ClearTargetCollection();
};
