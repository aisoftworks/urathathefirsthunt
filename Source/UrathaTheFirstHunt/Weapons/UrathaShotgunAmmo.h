// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Weapons/UrathaWeaponProjectile.h"
#include "UrathaShotgunAmmo.generated.h"

/**
 * 
 */
UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaShotgunAmmo : public AUrathaWeaponProjectile
{
	GENERATED_BODY()
	
};
