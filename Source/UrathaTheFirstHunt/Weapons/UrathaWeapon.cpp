// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#include "UrathaWeapon.h"
#include "UrathaWeaponProjectile.h"
#include "../Character/UrathaCharacter.h"
#include "../Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "../Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "../Runtime/Engine/Public/DrawDebugHelpers.h"

// Sets default values
AUrathaWeapon::AUrathaWeapon(const FObjectInitializer& OI) : Super(OI)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//This is a recommendable way to create sub object when attachment to RootComponent is buggy (e.g. relative location is screwed).
	WeaponRoot = OI.CreateDefaultSubobject<UBoxComponent>(this, FName("WeaponRoot"));
	SetRootComponent(WeaponRoot);
	
	WeaponMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(FName("WeaponMeshComponent"));
	//Use this instead of AttachToComponent when doing this in the constructor. Interestingly enough, AttachToComponent works as intended in Blueprint. 
	WeaponMeshComponent->SetupAttachment(WeaponRoot);

	MultipleTargetCollection = OI.CreateDefaultSubobject<UMultipleTargetCollection>(this, FName("MultipleTargetCollectionObject"));
}

// Called when the game starts or when spawned
void AUrathaWeapon::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AUrathaWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

UStaticMeshComponent* AUrathaWeapon::GetWeaponMesh()
{
	return WeaponMeshComponent;
}

UMultipleTargetCollection* AUrathaWeapon::GetTargetCollection()
{
	if (IsValid(MultipleTargetCollection))
		return MultipleTargetCollection;
	else
		return nullptr;
}

void AUrathaWeapon::FireWeapon(FVector VelocityVector)
{
	if (WeaponProjectileBlueprint)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FTransform ProjectileSpawnSocketTransform = WeaponMeshComponent->GetSocketTransform(FName("ProjectileSpawnSocket"));
			FActorSpawnParameters ProjectileSpawnParameters;
			ProjectileSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

			World->SpawnActor<AUrathaWeaponProjectile>(
				WeaponProjectileBlueprint,
				ProjectileSpawnSocketTransform.GetLocation(),
				ProjectileSpawnSocketTransform.GetRotation().Rotator(),
				ProjectileSpawnParameters
				);
		}
	}
}

bool AUrathaWeapon::WeaponAttack(class AUrathaCharacter* character, int bulletsPerAttack, float weaponFiringDistance, float firingDeviationAngle, float firingAimDeviationAngle)
{
	bool targetHit = false;

	for (size_t bullet = 0; bullet < bulletsPerAttack; bullet++)
	{
		FVector shootingOriginVector = GetWeaponMesh()->GetSocketTransform(FName("ProjectileSpawnSocket")).GetLocation();

		float firingDeviationRadians = 0.f;
		if (character->IsAiming())
		{
			//Function takes a cone half-angle (from center to edge)
			firingDeviationRadians = FMath::DegreesToRadians(firingAimDeviationAngle * 0.5f);
		}
		else
		{
			//Function takes a cone half-angle (from center to edge)
			firingDeviationRadians = FMath::DegreesToRadians(firingDeviationAngle * 0.5f);
		}
		FVector shootingSpreadVector = FMath::VRandCone(character->GetFollowCamera()->GetForwardVector(), firingDeviationRadians, firingDeviationRadians);

		FVector shootingTargetVector = character->GetFollowCamera()->GetComponentLocation() + shootingSpreadVector * (weaponFiringDistance + 100.f);
		

		FHitResult hit;
		FCollisionQueryParams collisionParams;
		collisionParams.AddIgnoredActor(character);		

		if (GetWorld()->LineTraceSingleByChannel(hit, shootingOriginVector, shootingTargetVector, ECollisionChannel::ECC_GameTraceChannel11, collisionParams))
		{
			DrawDebugLine(GetWorld(), shootingOriginVector, shootingTargetVector, FColor::Green, false, 3.f);

			AActor* hitActor = hit.GetActor();
			if (IsValid(hitActor))
			{
				MultipleTargetCollection->AddTarget(hitActor);
				targetHit = true;
			}
			else
			{
				UE_LOG(LogTemp, Warning, TEXT("HIT ACTOR NOT VALID!"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Miss!"));
			DrawDebugLine(GetWorld(), shootingOriginVector, shootingTargetVector, FColor::Red, false, 3.f);
		}
	}

	return targetHit;
}

