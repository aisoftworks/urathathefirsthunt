// Copyright 1988-20XX Igor Trkulja. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "../Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "UrathaWeaponProjectile.generated.h"

UCLASS()
class URATHATHEFIRSTHUNT_API AUrathaWeaponProjectile : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AUrathaWeaponProjectile(const FObjectInitializer& OI);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Projectile")
	UCapsuleComponent* ProjectileRoot;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
	UStaticMeshComponent* ProjectileMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovementComponent;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	class UProjectileMovementComponent* GetProjectileMovementComponent();

};
